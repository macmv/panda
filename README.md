# Panda

 [![pipeline status](https://gitlab.com/macmv/panda/badges/main/pipeline.svg)](https://gitlab.com/macmv/panda/-/pipelines)
 [![coverage report](https://gitlab.com/macmv/panda/badges/main/coverage.svg)](https://macmv.gitlab.io/panda/index.html)

```rust
struct Point {
  x: int,
  y: int,
  data: any,
}

fn main() {
  println("Hello world!")

  if 5 != 6 {
    println("foo")
  }

  m = map {
    "i": 5,
    "am": 6,
    "map": 7,
  }

  println(m.len()) // Prints 3
  println(m["am"]) // Prints 6
}
```

This is a scripting language written for Minecraft plugins. Specifically, it is written for my server,
[Bamboo](https://gitlab.com/macmv/bamboo). Since it is written for that specific purpose, it does a few things
very well:

- Simple Rust API
  - The Rust API for this language is designed for additions. You can easily declare builtin functions on any type,
    including types that are only defined within Rust code.
- Sandboxed
  - The core builtin functions keep the language entirely sandboxed. It doesn't even have access to stdin/stdout. This
    makes it ideal for writing plugins, as you get complete control over what the code has access to.
- Multithreading
  - The API makes it very easy to pass around a reference to a running environment, and execute code from multiple
    threads.

You can see more examples of the language in-use in the [examples](https://gitlab.com/macmv/panda/-/tree/main/examples)
and [testing](https://gitlab.com/macmv/panda/-/tree/main/src/runtime/tests) directories. The examples one will
probably fall out of date, as I don't use it at all. The testing directory will always be up-to-date, as everything
there is executed as part of `cargo test`.

### Compiling

`cargo build`. There are no complicated dependencies for this project. `cargo run` will give you an interactive shell,
where you can mess with the language. `cargo run <path>` will execute a Panda file.

### Contributing

If you'd like to contribute, PRs are welcome! I don't have any stability guarantees at the moment, so most things can
be changed. I like the current syntax, but that doesn't mean it can't be changed. This isn't a requirement, but I would
like to keep the dependencies to a minimum. It makes running `cargo tarpaulin` much faster, and it keeps binaries very small.
