use super::Panda;
use panda_docs::page::{Arg, Field, Func, Path, PathKind, Ty};
use panda_parse::{Path as SlPath, VarType};
use panda_runtime::Item;

pub use panda_docs::{markdown, Docs, MarkdownSection};

fn convert_path(p: &SlPath) -> Path {
  Path::new(p.inner().to_vec(), PathKind::Struct)
}

fn convert_ty(v: &VarType) -> Ty {
  match v {
    VarType::String => Ty::Str,
    VarType::Int => Ty::Int,
    VarType::Float => Ty::Float,
    VarType::Bool => Ty::Bool,
    VarType::Struct(p) => Ty::Struct(convert_path(p)),
    VarType::Map => Ty::Map,
    VarType::Array => Ty::Array,
    VarType::Range => Ty::Range,
    VarType::None => Ty::Any,
    VarType::Builtin(p) => Ty::Builtin(convert_path(p)),
    VarType::Callback => Ty::Callback,
    VarType::Closure => Ty::Closure,
  }
}

impl Panda {
  /// Generates docs. The extra list must have higher level modules first. For
  /// any type that is declared in a module, an item in extra is required for
  /// every module in the path.
  pub fn generate_docs(
    &self,
    modules: &[(SlPath, MarkdownSection)],
    funcs: &[(SlPath, MarkdownSection)],
  ) -> Docs {
    let mut docs = Docs::new(MarkdownSection::empty());
    let mut list: Vec<(_, _)> = self.env.names().iter().collect();
    list.sort_unstable_by(|(a, _), (b, _)| a.cmp(b));
    for (path, item) in list {
      match item {
        Item::Func(f) => {
          docs.add_struct_func(
            Path::new(path.inner().to_vec(), PathKind::Func),
            Func::new(
              path.last().to_string(),
              f.docs(),
              f.arg_docs()
                .into_iter()
                .map(|(name, ty)| Arg::new(name.into(), ty.map(|v| convert_ty(&v))))
                .collect(),
              None,
            ),
          );
        }
        Item::Struct(s) => {
          docs.add_struct(
            Path::new(path.inner().to_vec(), PathKind::Struct),
            MarkdownSection::from_lines(s.docs()),
            vec![],
            s.fields()
              .into_iter()
              .map(|(name, doc, ty)| Field::new(name, doc, convert_ty(&ty)))
              .collect(),
          );
        }
        Item::Trait(_) => {
          todo!("document traits")
        }
        Item::Module => {
          docs.add_module(convert_path(path), MarkdownSection::empty());
        }
      }
    }
    // The extra section needs to override the above docs
    for (path, section) in modules {
      docs.add_module(convert_path(path), section.clone());
    }
    for (path, section) in funcs {
      docs.update_func_docs(convert_path(path), section.clone());
    }
    let mut list: Vec<(_, _)> = self.env.lock(&self.files).predefined.iter().collect();
    list.sort_unstable_by(|(a, _), (b, _)| a.cmp(b));
    for (name, predefined) in list {
      docs.add_predefined(Path::new(vec![name.clone()], PathKind::Struct), predefined.docs.clone());
    }
    let mut list: Vec<(_, _)> = self.env.lock(&self.files).callback_impls.iter().collect();
    list.sort_unstable_by(|(a, _), (b, _)| a.cmp(b));
    for (name, cb) in list {
      docs.add_callback(
        Path::new(vec![name.clone()], PathKind::Struct),
        cb.args.iter().map(|ty| convert_ty(ty)).collect(),
        cb.docs.clone(),
      );
    }
    docs
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::path;

  #[derive(Debug, Clone)]
  struct MyBuiltin {}
  #[derive(Debug, Clone)]
  struct MyData {}

  /// My Builtin docs here!
  /// This should be on the same line.
  /// This is a really long line to test max_width for summaries.
  ///
  /// Here is another line of text.
  #[panda_derive::define_ty(crate = "panda_runtime", path = "sugarcane::MyType")]
  impl MyBuiltin {
    /// Custom docs here escaped: " \"
    ///
    /// Make sure these: ' work (should be a single quote)
    ///
    /// This should have correct formatting: `hello`
    ///
    /// This should not have a space: (`hello`)
    ///
    /// # Example
    ///
    /// ```
    /// MyBuiltin::hello()
    ///
    /// // These should be in gray
    ///
    /// // Strings and ints should be colored
    /// my_func("string", 5, 6)
    ///
    /// // Keywords and primitive types should also have colors
    /// fn my_func(a: str, b: MyType) {
    ///   var = 5
    ///   other_func(a, b)
    /// }
    ///
    /// // Here is some math:
    /// a * 5
    /// b + 2
    /// a - 1
    ///
    /// c-2
    /// b/4
    /// a//5
    ///
    /// /* long
    /// block
    /// comment */
    ///
    /// block comment end
    ///
    /// /* I am block */
    /// ```
    pub fn hello(&self, _a: &str, _v: i32, _arr: Vec<i32>, _mydata: &MyData) -> MyData {
      MyData {}
    }
  }

  /// My Builtin data docs here!
  #[panda_derive::define_ty(crate = "panda_runtime", path = "sugarcane::OtherNameForMyData")]
  impl MyData {}

  #[test]
  fn generate_docs() {
    let mut sl = Panda::new();
    sl.add_builtin_ty::<MyBuiltin>();
    sl.add_builtin_ty::<MyData>();
    let docs = sl.generate_docs(
      &[(
        path!(sugarcane),
        markdown!(
          /// A sample module
          ///
          /// Here is a really long line to test the max width property.
          /// This line needs to be very long, and it should span multiple
          /// lines.
          ///
          /// ```
          /// // for a width comparison
          /// ```
        ),
      )],
      &[],
    );
    docs.save("target/sl_docs");
  }
}
