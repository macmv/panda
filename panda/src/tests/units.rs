use crate::{parse::PdError, path, Panda};
use panda_docs::markdown;
use panda_parse::ansi_term::Colour;
use panda_runtime::PandaType;
use std::{
  fs,
  path::{Path, PathBuf},
};

struct ErrorExpectation {
  start:   (u32, u32),
  end:     (u32, u32),
  message: String,
}

enum Expectation {
  PassRuntime,
  PassCallback,
  PassPredefined,
  FailRuntime(ErrorExpectation),
  FailCompile(ErrorExpectation),
}

struct Test {
  expectation: Expectation,
  src:         String,
  path:        PathBuf,
}

enum TestResult {
  Pass,
  Fail(String),
}

use TestResult::{Fail, Pass};

#[test]
fn all_units() {
  let env_name = std::env::args().nth(2).unwrap_or_default();
  let (total, passed) = run_units(Path::new("src/tests/units"), "".into(), &env_name);
  if passed < total {
    println!(">>> passed {passed}/{total} tests");
    panic!("at least one test failed");
  }
}

fn run_units(path: &Path, name: String, env_name: &str) -> (usize, usize) {
  let dir = fs::read_dir(path).unwrap();
  let mut total = 0;
  let mut passed = 0;
  for ent in dir {
    let ent = ent.unwrap();
    if ent.metadata().unwrap().is_dir() {
      let (subtotal, subpassed) = run_units(
        &ent.path(),
        name.clone() + "/" + ent.path().file_name().unwrap().to_str().unwrap(),
        env_name,
      );
      total += subtotal;
      passed += subpassed;
    } else {
      if run_unit_test(&ent.path(), &name, env_name) {
        passed += 1;
      }
      total += 1;
    }
  }
  (total, passed)
}

fn run_unit_test(path: &Path, name: &str, env_name: &str) -> bool {
  let name = name.to_string() + "/" + path.file_name().unwrap().to_str().unwrap();
  print!(">> running unit test {name}... ");
  if !env_name.is_empty() && !name.contains(env_name) {
    println!("{}", Colour::Purple.paint("skipped"));
    true
  } else {
    match Test::new(path).run() {
      Pass => {
        println!("{}", Colour::Green.paint("passed"));
        true
      }
      Fail(msg) => {
        println!("{}", Colour::Red.paint("failed"));
        println!("{msg}");
        false
      }
    }
  }
}

impl Test {
  pub fn new(path: &Path) -> Test {
    let src = fs::read_to_string(path).unwrap();
    let expectation = Expectation::parse(&src, path);
    Test { expectation, path: path.into(), src }
  }

  pub fn run(self) -> TestResult {
    let mut pd = Panda::new();
    pd.env().add_test_builtins();
    pd.env().add_std_builtins();
    if matches!(self.expectation, Expectation::PassCallback) {
      pd.def_callback("sample_event", vec![i32::var_type()], markdown!());
    }
    if matches!(self.expectation, Expectation::PassPredefined) {
      pd.predefine("my_predefined", markdown!(), || 15.into());
    }
    match self.expectation {
      Expectation::FailCompile(exp) => match pd.parse_file(&path!(), &self.path, self.src) {
        Ok(_) => Fail("test compiled, but it was not supposed to".into()),
        Err(e) => exp.check_err(&pd, e),
      },
      _ => {
        match pd.parse_file(&path!(), &self.path, self.src) {
          Ok(_) => {}
          Err(e) => return Fail(pd.gen_err(e).trim().into()),
        }
        match self.expectation {
          Expectation::FailRuntime(exp) => match pd.call(&path!(test)) {
            Ok(_) => Fail("test executed, but it was not supposed to".into()),
            Err(e) => exp.check_err(&pd, e),
          },
          Expectation::PassRuntime | Expectation::PassPredefined => match pd.call(&path!(test)) {
            Ok(_) => Pass,
            Err(e) => Fail(pd.gen_err(e).trim().into()),
          },
          Expectation::PassCallback => match pd.run_callback("sample_event", vec![3.into()]) {
            Ok(_) => Pass,
            Err(e) => Fail(pd.gen_err(e).trim().into()),
          },
          _ => unreachable!(),
        }
      }
    }
  }
}

impl Expectation {
  pub fn parse(src: &str, path: &Path) -> Expectation {
    let mut lines = src.lines();
    let line = lines.next();
    match line {
      Some("// PASS RUNTIME") => Expectation::PassRuntime,
      Some("// CHECK CALLBACK") => Expectation::PassCallback,
      Some("// CHECK PREDEFINED") => Expectation::PassPredefined,
      Some("// FAIL RUNTIME") => Expectation::FailRuntime(ErrorExpectation::parse(lines, path)),
      Some("// FAIL COMPILE") => Expectation::FailCompile(ErrorExpectation::parse(lines, path)),
      _ => panic!("invalid test file {}", path.display()),
    }
  }
}

impl ErrorExpectation {
  pub fn parse<'a>(lines: impl Iterator<Item = &'a str>, path: &Path) -> ErrorExpectation {
    match ErrorExpectation::parse_inner(lines, path) {
      Some(e) => e,
      None => {
        panic!("invalid error spec in {}", path.display())
      }
    }
  }

  pub fn check_err(&self, pd: &Panda, err: impl PdError) -> TestResult {
    let msg = err.to_string();
    if msg != self.message {
      let mut fail = String::new();
      fail.push_str("errors did not match:\n");
      fail.push_str("expected: ");
      fail.push_str(&self.message);
      fail.push('\n');
      fail.push_str("actual  : ");
      fail.push_str(&msg);
      fail.push('\n');
      fail.push_str(&pd.gen_err(err));
      return Fail(fail);
    }

    let span = err.pos();
    let start = span.start().line_col(pd.files[0].src());
    let end = span.end().line_col(pd.files[0].src());

    if start != self.start || end != self.end {
      let mut fail = String::new();
      fail.push_str("position did not match:\n");
      fail.push_str("expected: ");
      fail.push_str(&format!(
        "from {}:{} to {}:{}",
        self.start.0, self.start.1, self.end.0, self.end.1
      ));
      fail.push('\n');
      fail.push_str("actual  : ");
      fail.push_str(&format!("from {}:{} to {}:{}", start.0, start.1, end.0, end.1));
      fail.push('\n');
      fail.push_str(&pd.gen_err(err));
      return Fail(fail);
    }

    Pass
  }

  fn parse_inner<'a>(
    mut lines: impl Iterator<Item = &'a str>,
    _path: &Path,
  ) -> Option<ErrorExpectation> {
    let line = lines.next()?;
    let line = line.strip_prefix("// > from ")?;
    let (line, start) = parse_pos(line)?;
    let line = line.strip_prefix(" to ")?;
    let (line, end) = parse_pos(line)?;
    if line.is_empty() {
      let message = lines.next()?.strip_prefix("// > ")?;
      Some(ErrorExpectation { start, end, message: message.into() })
    } else {
      None
    }
  }
}

fn parse_pos(text: &str) -> Option<(&str, (u32, u32))> {
  let split = text.find(':')?;
  let end = text.find(' ').unwrap_or(text.len());
  let line = text[..split].parse().ok()?;
  let col = text[split + 1..end].parse().ok()?;
  Some((&text[end..], (line, col)))
}
