mod callback;
mod derive_fields;
mod derive_in_module;
mod second_pass_errors;
mod units;

use panda_docs::markdown;

use crate::{parse::path, runtime::Callback, Panda, RuntimeError, Var};
use std::{
  path::Path,
  sync::{Arc, Mutex},
  time::Duration,
};

macro_rules! pand_test {
  ($name:ident, $path:expr) => {
    #[test]
    fn $name() {
      println!(concat!("running test `", stringify!($name), "`"));
      let mut sl = Panda::new();
      sl.env().add_test_builtins();
      let src = include_str!($path);
      match sl.parse_file(&path!(), Path::new($path), src.into()) {
        Ok(_) => {}
        Err(e) => {
          sl.print_err(e);
          panic!(concat!("failed to parse ", $path))
        }
      }
      match sl.call(&path!(test)) {
        Ok(_) => {}
        Err(e) => {
          sl.print_err(e);
          panic!(concat!("failed to execute test in ", $path))
        }
      }
    }
  };
}

macro_rules! pand_test_dir {
  ($name:ident, $path:expr) => {
    #[test]
    fn $name() {
      let mut sl = Panda::new();
      sl.env().add_test_builtins();
      match sl.parse_dir(&Path::new("src/tests").join($path), &path!()) {
        Ok(_) => {}
        Err(e) => {
          sl.print_err(e);
          panic!(concat!("failed to parse ", $path))
        }
      }
      match sl.call(&path!(main::test)) {
        Ok(_) => {}
        Err(e) => {
          sl.print_err(e);
          panic!(concat!("failed to execute test in ", $path))
        }
      }
    }
  };
}

pand_test!(builtins, "builtins.pand");
pand_test!(types, "types.pand");
pand_test_dir!(imports, "imports");

#[test]
fn infinite_loop() {
  let mut sl = Panda::new();
  sl.set_time_limit(Some(Duration::from_millis(100)));
  sl.env().add_test_builtins();
  let src = include_str!("infinite.pand");
  match sl.parse_file(&path!(), Path::new("src/tests/infinite.pand"), src.into()) {
    Ok(_) => {}
    Err(e) => {
      sl.print_err(e);
      panic!("failed to parse infinite.pand")
    }
  }
  if sl.call(&path!(test)).is_ok() {
    panic!("execution passed, when time limit should have been reached");
  }
}
#[test]
fn fail_imports_compile_time() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();
  match sl.parse_dir(Path::new("src/tests/imports_fail_compile_time"), &path!()) {
    Ok(_) => {
      panic!("failed to parse imports_fail_compile_time");
    }
    Err(e) => {
      // Make sure this doesn't panic
      sl.print_err(e);
    }
  }
}

#[test]
fn fail_imports_run_time() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();
  match sl.parse_dir(Path::new("src/tests/imports_fail_run_time"), &path!()) {
    Ok(_) => {}
    Err(e) => {
      sl.print_err(e);
      panic!("failed to parse imports_fail_compile_time");
    }
  }
  match sl.call(&path!(main::test)) {
    Ok(_) => {
      panic!("execution passed, when time limit should have been reached");
    }
    Err(e) => {
      // Make sure this doesn't panic
      sl.print_err(e);
    }
  }
}

#[derive(Debug, Clone)]
struct MyCallbackHandler {
  callback: Arc<Mutex<Option<Callback>>>,
}

#[panda_derive::define_ty(crate = "panda_runtime", path = "callback")]
impl MyCallbackHandler {
  fn save_callback(&mut self, c: Callback) {
    *self.callback.lock().unwrap() = Some(c);
  }
}

#[test]
fn callback() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();
  sl.env().add_builtin_ty::<MyCallbackHandler>();
  sl.env().add_builtin_fn(path!(run_callback), markdown!(), false, |env, _slf, mut args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos).unwrap();
    let path = match args.pop().unwrap() {
      Var::Callback(path) => path,
      _ => unreachable!(),
    };
    env.call(&path, pos, None, vec![])?;
    Ok(Var::None)
  });
  let src = include_str!("callback.pand");
  match sl.parse_file(&path!(main), Path::new("src/tests/callback.pand"), src.into()) {
    Ok(_) => {}
    Err(e) => {
      sl.print_err(e);
      panic!("failed to parse callback.pand")
    }
  }
  let h = MyCallbackHandler { callback: Arc::new(Mutex::new(None)) };
  match sl.call_args(&path!(main::test), vec![h.clone().into()]) {
    Ok(_) => {
      let mut callback = h.callback.lock().unwrap();
      let c = callback.as_mut().unwrap();
      let (env, files) = sl.env_files();
      let mut lock = env.lock(files);
      match c.call(&mut lock, vec![]) {
        Ok(_) => {}
        Err(_) => panic!("failed to run callback"),
      }
    }
    Err(e) => {
      sl.print_err(e);
      panic!("could not run test function");
    }
  }
}

#[derive(Debug, Clone)]
struct MyNestedStruct {}

#[panda_derive::define_ty(crate = "panda_runtime", path = "nested::foo::bar::Nested")]
impl MyNestedStruct {
  fn it_worked() {
    println!("hello!");
  }
}

#[test]
fn nested_builtin() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();
  sl.env().add_builtin_ty::<MyNestedStruct>();
  dbg!(&sl.env());
  let src = include_str!("nested_builtin.pand");
  match sl.parse_file(&path!(main), Path::new("src/tests/nested_builtin.pand"), src.into()) {
    Ok(_) => {}
    Err(e) => {
      sl.print_err(e);
      panic!("failed to parse nested_builtin.pand")
    }
  }
  match sl.call(&path!(main::test)) {
    Ok(_) => {}
    Err(e) => {
      sl.print_err(e);
      panic!("failed to execute test nested_builtin.pand")
    }
  }
}

#[test]
fn nested_builtin_fail() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();
  sl.env().add_builtin_ty::<MyNestedStruct>();
  dbg!(&sl.env());
  let src = include_str!("nested_builtin_fail.pand");
  match sl.parse_file(&path!(main), Path::new("src/tests/nested_builtin_fail.pand"), src.into()) {
    Ok(_) => {
      panic!("should have failed to parse");
    }
    Err(e) => {
      sl.print_err(e);
    }
  }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct MyMapKey {
  x: i32,
  y: i32,
}

#[panda_derive::define_ty(crate = "panda_runtime", path = "MapKey", map_key = true)]
impl MyMapKey {}
