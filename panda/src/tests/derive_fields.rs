use crate::{Panda, PdError};
use panda_runtime::{tree::Closure, Var};
use std::path::Path;

#[derive(Debug, Clone)]
struct AllFieldGetter;

#[panda_derive::define_ty(crate = "panda_runtime")]
impl AllFieldGetter {
  pub fn new() -> Self {
    AllFieldGetter
  }
  #[all_fields]
  pub fn field(&self, field: &str) -> Option<Var> {
    Some(match field {
      "my_field" => 3.into(),
      _ => return None,
    })
  }
}

#[test]
fn derive_all_fields() {
  let mut sl = Panda::new();
  sl.add_builtin_ty::<AllFieldGetter>();
  sl.env().add_test_builtins();

  let src = "AllFieldGetter::new().my_field";
  let out = sl.parse_statement(src.as_bytes());
  match out {
    Ok(stat) => match sl.run(stat) {
      Ok(_) => {}
      Err(e) => {
        e.print(src, Path::new("test"), sl.use_color());
        panic!();
      }
    },
    Err(e) => {
      e.print(src, Path::new("test"), sl.use_color());
      panic!();
    }
  }
}

#[derive(Debug, Clone)]
struct FieldGetter;

#[panda_derive::define_ty(crate = "panda_runtime")]
impl FieldGetter {
  pub fn new() -> Self {
    FieldGetter
  }
  #[field]
  pub fn my_field(&self) -> i32 {
    3
  }
}

#[test]
fn derive_fields() {
  let mut sl = Panda::new();
  sl.add_builtin_ty::<FieldGetter>();
  sl.env().add_test_builtins();

  let src = "FieldGetter::new().my_field";
  let out = sl.parse_statement(src.as_bytes());
  match out {
    Ok(stat) => match sl.run(stat) {
      Ok(_) => {}
      Err(e) => {
        e.print(src, Path::new("test"), sl.use_color());
        panic!();
      }
    },
    Err(e) => {
      e.print(src, Path::new("test"), sl.use_color());
      panic!();
    }
  }
}

#[derive(Debug, Clone)]
struct ClosureArg {
  closure: Option<Closure>,
}

#[panda_derive::define_ty(crate = "panda_runtime")]
impl ClosureArg {
  pub fn new() -> Self {
    ClosureArg { closure: None }
  }
  pub fn call_closure(&mut self, closure: Closure) {
    self.closure = Some(closure);
  }
}

#[test]
fn closure_args() {
  let mut sl = Panda::new();
  sl.add_builtin_ty::<ClosureArg>();
  sl.env().add_test_builtins();

  let src = "let c = ClosureArg::new()   c.call_closure(|a| assert_eq(a, 3))   c";
  let out = sl.parse_statement(src.as_bytes());
  match out {
    Ok(stat) => match sl.run(stat) {
      Ok(res) => {
        let strct = res.strct(panda_parse::token::Span::default()).unwrap();
        let builtin = strct.as_builtin(panda_parse::token::Span::default()).unwrap();
        let c = builtin.as_any().downcast_ref::<ClosureArg>().unwrap();
        let (env, files) = sl.env_files();
        c.closure.as_ref().unwrap().call(&mut env.lock(files), vec![3.into()]).unwrap();
      }
      Err(e) => {
        e.print(src, Path::new("test"), sl.use_color());
        panic!();
      }
    },
    Err(e) => {
      e.print(src, Path::new("test"), sl.use_color());
      panic!();
    }
  }
}
