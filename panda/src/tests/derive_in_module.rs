use crate::{Panda, PdError};
use std::path::Path;
#[derive(Debug, Clone)]
struct MyType;

#[panda_derive::define_ty(crate = "panda_runtime", path = "hello::world")]
impl MyType {
  pub fn static_func() {
    // If we got here, it worked
  }

  // TODO: Variadics
  // pub fn varidic_self(&self, _args: Variadic<&Var>) {}
}

#[test]
fn derive_in_module() {
  let mut sl = Panda::new();
  sl.add_builtin_ty::<MyType>();
  sl.env().add_test_builtins();

  let src = "hello::world::static_func()";
  let out = sl.parse_statement(src.as_bytes());
  match out {
    Ok(stat) => match sl.run(stat) {
      Ok(_) => {}
      Err(e) => {
        e.print(src, Path::new("test"), sl.use_color());
        panic!();
      }
    },
    Err(e) => {
      e.print(src, Path::new("test"), sl.use_color());
      panic!();
    }
  }
}
