use panda_parse::token::Span;

use crate::{path, runtime::Var, Panda};
use std::path::Path;

#[test]
fn derive_in_module() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();

  let src = "fn my_func(a, b, c) {}";
  sl.parse_file(&path!(test), Path::new("test.pand"), src.into()).unwrap();
  if let Ok(v) = sl.call(&path!(test::my_func)) {
    println!("{}", v);
    panic!("should have failed with arg number");
  }
  if let Ok(v) = sl.call_args(&path!(test::my_func), vec![Var::Int(5)]) {
    println!("{}", v);
    panic!("should have failed with arg number");
  }
  if let Err(e) = sl.call_args(&path!(test::my_func), vec![Var::Int(1), Var::Int(2), Var::Int(3)]) {
    sl.print_err(e);
    panic!("should have worked");
  }
}

#[test]
fn call_capture_closure() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();

  let src = "fn test() { let a = 3 let closure = || a closure }";
  sl.parse_file(&path!(test), Path::new("test.pand"), src.into()).unwrap();
  let v = sl.call(&path!(test::test)).unwrap();
  let closure = v.closure(Span::default()).unwrap();
  let mut lock = sl.env.lock(&sl.files);
  closure.call(&mut lock, vec![]).unwrap();
}

#[test]
fn call_capture_call_closure() {
  let mut sl = Panda::new();
  sl.env().add_test_builtins();

  let src = r#"
    fn test() {
      let a = 3
      let b = 4
      let c = || a + b
      c
    }"#;
  sl.parse_file(&path!(test), Path::new("test.pand"), src.into()).unwrap();
  let v = sl.call(&path!(test::test)).unwrap();
  let closure = v.closure(Span::default()).unwrap();
  dbg!(&closure);
  let mut lock = sl.env.lock(&sl.files);
  closure.call(&mut lock, vec![]).unwrap();
}
