//! The tokenizer. This handles individual tokens. Specifically, the
//! [`Tokenizer`] makes it easy to [`read`](Tokenizer::read),
//! [`peek`](Tokenizer::peek), and [`unread`](Tokenizer::unread) tokens.
//!
//! Example of the tokenizer:
//! ```
//! use panda_parse::token::{Keyword, Token, Tokenizer};
//!
//! let mut tok = Tokenizer::new("if 5 == 6 {".as_bytes(), 0);
//! assert!(tok.peek(0).unwrap().is_keyword(Keyword::If));
//!
//! let t = tok.read().unwrap();
//! assert!(t.is_keyword(Keyword::If));
//! assert!(matches!(tok.peek(0).unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
//!
//! // After unreading a token, the next peek or read will find that token again.
//! tok.unread(t);
//! assert!(tok.peek(0).unwrap().is_keyword(Keyword::If));
//! assert!(tok.read().unwrap().is_keyword(Keyword::If));
//!
//! assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
//! assert!(tok.read().unwrap().is_keyword(Keyword::Eq));
//! assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 6));
//!
//! // This is the same thing as read() then is_keyword(). This will return a parse error
//! // if the token is not the right keyword.
//! tok.expect(Keyword::OpenBlock).unwrap();
//!
//! // Now that we are out of tokens, `read` will return EOF, and `read_opt` will return None.
//! // EOF is a token so that errors on the cli are easier to work with. For example, we
//! // can expected a `)` token, but get an EOF token instead. It just makes things simpler.
//! assert!(tok.read_opt().unwrap().is_none());
//! assert_eq!(tok.read(), Ok(Token::EOF(0)));
//! ```
mod err;
pub use err::{ParseError, Result};

use crate::Path;
use std::{
  borrow::Borrow,
  collections::VecDeque,
  fmt, io,
  io::Read,
  ops::{Deref, DerefMut},
  str::FromStr,
};

mod group;
mod keyword;
mod pos;
mod reader;
#[cfg(test)]
mod tests;

pub use group::{Group, GroupKind};
pub use keyword::Keyword;
pub use pos::{Pos, Span};
use reader::PeekReader;

/// The tokenizer. See the [`module`](crate::parse::token) level docs for more.
pub struct Tokenizer<R> {
  file:   u16,
  reader: PeekReader<R>,
  peeked: VecDeque<Token>,
}

/// A single token. This is the result of [`peek`](Tokenizer::peek) or
/// [`read`](Tokenizer::peek) on the tokenizer. This contains a token kind, and
/// a span. It can be used to generate errors (for unexpected tokens) or to
/// validate that a keyword is present.
#[derive(Debug, Clone, PartialEq)]
pub enum Token {
  Keyword(KeywordPos),
  Ident(Ident),
  Lit(Lit),
  Special(Special),
  /// A special token. This is only emitted once the tokenizer has read all the
  /// bytes in the file. This is used so that error handling is simpler for
  /// the cli.
  ///
  /// The usize is the file index of the given token.
  EOF(u16),
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Ident {
  inner: String,
  pos:   Span,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct PathLit {
  pub(crate) inner: Path,
  pos:              Span,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct KeywordPos {
  inner: Keyword,
  pos:   Span,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Lit {
  Int(IntLit),
  Float(FloatLit),
  String(StringLit),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct IntLit {
  pub pos: Span,
  pub val: i64,
}
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct FloatLit {
  pub pos: Span,
  pub val: f64,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StringLit {
  pub pos: Span,
  pub val: String,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Special {
  Predefined(Predefined),
  Tag(Tag),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Predefined {
  pub pos: Span,
  pub val: String,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Tag {
  pub pos: Span,
  pub val: String,
}

impl fmt::Display for Token {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::Keyword(v) => write!(f, "`{}`", v.inner),
      Self::Ident(v) => write!(f, "`{}`", v.inner),
      Self::Lit(v) => write!(f, "{}", v),
      Self::Special(v) => write!(f, "{}", v),
      Self::EOF(_) => write!(f, "EOF"),
    }
  }
}

impl fmt::Display for Lit {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::Int(v) => write!(f, "`{}`", v.val),
      Self::Float(v) => write!(f, "`{}`", v.val),
      Self::String(v) => write!(f, "\"{}\"", v.val),
    }
  }
}

impl fmt::Display for Special {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::Predefined(v) => write!(f, "@{}", v.val),
      Self::Tag(v) => write!(f, "#{}", v.val),
    }
  }
}

impl fmt::Display for Predefined {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "@{}", self.val)
  }
}

impl AsRef<str> for Ident {
  fn as_ref(&self) -> &str {
    &self.inner
  }
}
impl Borrow<String> for Ident {
  fn borrow(&self) -> &String {
    &self.inner
  }
}
impl From<Ident> for String {
  fn from(v: Ident) -> String {
    v.inner
  }
}
impl Deref for Ident {
  type Target = str;

  fn deref(&self) -> &str {
    &self.inner
  }
}
impl Ident {
  pub fn pos(&self) -> Span {
    self.pos
  }
  /// Converts this Ident into a path with a single segment.
  pub fn into_path(self) -> PathLit {
    PathLit { inner: Path::new(vec![self.inner]), pos: self.pos }
  }
}

impl AsRef<Keyword> for KeywordPos {
  fn as_ref(&self) -> &Keyword {
    &self.inner
  }
}
impl Borrow<Keyword> for KeywordPos {
  fn borrow(&self) -> &Keyword {
    &self.inner
  }
}
impl KeywordPos {
  pub fn pos(&self) -> Span {
    self.pos
  }
  pub fn value(&self) -> Keyword {
    self.inner
  }
}

impl PartialEq<i64> for IntLit {
  fn eq(&self, other: &i64) -> bool {
    self.val == *other
  }
}
impl PartialEq<f64> for FloatLit {
  fn eq(&self, other: &f64) -> bool {
    self.val == *other
  }
}
impl<'a> PartialEq<&'a str> for StringLit {
  fn eq(&self, other: &&str) -> bool {
    self.val == *other
  }
}

impl PathLit {
  pub fn new(inner: Path, pos: Span) -> PathLit {
    PathLit { inner, pos }
  }
  pub fn replace(&mut self, new: Path) {
    self.inner = new
  }
  pub fn path(&self) -> &Path {
    &self.inner
  }
  pub fn into_path(self) -> Path {
    self.inner
  }
  pub fn pos(&self) -> Span {
    self.pos
  }
  pub fn first(&self) -> Ident {
    Ident::new(self.inner.first(), self.pos)
  }
  pub fn last(&self) -> Ident {
    Ident::new(self.inner.last(), self.pos)
  }
  pub fn push_prefix(&mut self, prefix: Ident) {
    self.pos = self.pos.join(prefix.pos());
    self.inner.push_prefix(prefix.into());
  }
  pub fn push_all_prefix(&mut self, prefix: PathLit) {
    self.pos = self.pos.join(prefix.pos());
    self.inner.push_all_prefix(prefix.into());
  }
  /// Returns an ident that represents this path. Panics if the
  /// path does not have exactly one element.
  pub fn as_ident(&self) -> Ident {
    if self.inner.len() != 1 {
      panic!("cannot convert path to ident, as it does not have 1 element {:?}", self);
    }
    self.first()
  }

  pub fn set_path(&mut self, path: Path) {
    self.inner = path;
  }
}

impl From<PathLit> for Path {
  fn from(lit: PathLit) -> Path {
    lit.inner
  }
}

impl From<Ident> for PathLit {
  fn from(ident: Ident) -> PathLit {
    PathLit { pos: ident.pos(), inner: ident.inner.into() }
  }
}
/// Converts the vec to a path lit. If the vec is empty, this will panic.
impl From<Vec<Ident>> for PathLit {
  fn from(segments: Vec<Ident>) -> PathLit {
    debug_assert_eq!(segments.first().unwrap().pos().file(), segments.last().unwrap().pos().file());
    PathLit {
      pos:   Span::new(
        segments.first().unwrap().pos().start(),
        segments.last().unwrap().pos().end(),
        segments.first().unwrap().pos().file(),
      ),
      inner: Path::new(segments.into_iter().map(|ident| ident.inner).collect()),
    }
  }
}
impl Deref for PathLit {
  type Target = Path;

  fn deref(&self) -> &Path {
    &self.inner
  }
}
impl DerefMut for PathLit {
  fn deref_mut(&mut self) -> &mut Path {
    &mut self.inner
  }
}

impl Lit {
  pub fn pos(&self) -> Span {
    match self {
      Lit::Int(v) => v.pos,
      Lit::Float(v) => v.pos,
      Lit::String(v) => v.pos,
    }
  }
  pub fn is_int(&self) -> bool {
    matches!(self, Lit::Int(_))
  }
  pub fn is_float(&self) -> bool {
    matches!(self, Lit::Float(_))
  }
  pub fn is_str(&self) -> bool {
    matches!(self, Lit::String(_))
  }

  pub fn unwrap_int(&self) -> IntLit {
    match self {
      Lit::Int(v) => *v,
      _ => panic!("unwrapped on non-int {:?}", self),
    }
  }
  pub fn unwrap_float(&self) -> FloatLit {
    match self {
      Lit::Float(v) => *v,
      _ => panic!("unwrapped on non-float {:?}", self),
    }
  }
  pub fn unwrap_str(&self) -> StringLit {
    match self {
      Lit::String(v) => v.clone(),
      _ => panic!("unwrapped on non-string {:?}", self),
    }
  }
}
impl Ident {
  pub fn new<N: Into<String>>(name: N, pos: Span) -> Self {
    Ident { inner: name.into(), pos }
  }

  pub fn as_str(&self) -> &str {
    &self.inner
  }
}
macro_rules! unwrap_tok {
  ( $name:ident, $val:ident, $ret:ty ) => {
    pub fn $name<M: Into<String>>(self, msg: M) -> Result<$ret> {
      match self {
        Token::$val(v) => Ok(v),
        v => Err(ParseError::unexpected(v, msg)),
      }
    }
  };
}

impl Special {
  pub fn pos(&self) -> Span {
    match self {
      Self::Predefined(v) => v.pos,
      Self::Tag(v) => v.pos,
    }
  }
}

impl Token {
  pub fn pos(&self) -> Span {
    match self {
      Token::Keyword(v) => v.pos,
      Token::Ident(v) => v.pos,
      Token::Lit(v) => v.pos(),
      Token::Special(v) => v.pos(),
      Token::EOF(file) => Span::eof(*file),
    }
  }
  // Unwraps the token. If it is not a keyword, this will produce a
  // ParseError::unepected, with the message being the message passed to this
  // function.
  unwrap_tok!(keyword, Keyword, KeywordPos);
  // Unwraps the token. If it is not an identifier, this will produce a
  // ParseError::unepected, with the message being the message passed to this
  // function.
  unwrap_tok!(ident, Ident, Ident);
  // Unwraps the token. If it is not an int, this will produce a
  // ParseError::unepected, with the message being the message passed to this
  // function.
  unwrap_tok!(lit, Lit, Lit);

  pub fn is_keyword(&self, k: Keyword) -> bool {
    matches!(self, Token::Keyword(v) if v.inner == k)
  }
  pub fn is_ident(&self) -> bool {
    matches!(self, Token::Ident(_))
  }
}

macro_rules! try_eof {
  ( $self:expr, $expr:expr ) => {
    match $expr {
      Some(v) => v,
      None => return Ok(Token::EOF($self.file)),
    }
  };
}

enum ParseIOError {
  ParseError(ParseError),
  IO(io::Error),
}

impl From<io::Error> for ParseIOError {
  fn from(e: io::Error) -> Self {
    ParseIOError::IO(e)
  }
}
impl From<ParseError> for ParseIOError {
  fn from(e: ParseError) -> Self {
    ParseIOError::ParseError(e)
  }
}

impl ParseIOError {
  fn into_parse(self, file: u16) -> ParseError {
    match self {
      Self::ParseError(e) => e,
      Self::IO(e) => ParseError::io(e, file),
    }
  }
}

impl<R: Read> Tokenizer<R> {
  pub fn new(reader: R, file: u16) -> Self {
    Tokenizer { reader: PeekReader::new(reader), peeked: VecDeque::new(), file }
  }
  /// Sets the position of the reader. This will not change any of the tokens,
  /// but it will change the line/column numbers they are on. This is used in
  /// the cli to generate errors for the right line number.
  pub fn set_pos(&mut self, pos: Pos) {
    self.reader.set_pos(pos);
  }
  pub fn file(&self) -> u16 {
    self.file
  }
  /// Returns the current position of the tokenizer.
  pub fn pos(&self) -> Pos {
    match self.peeked.get(0) {
      Some(t) => t.pos().start(),
      None => self.reader.pos(),
    }
  }
  /// Reads a token. If the kind does not match the given keyword, then an
  /// unexpected error is returned.
  pub fn expect(&mut self, keyword: Keyword) -> Result<KeywordPos> {
    let t = self.read()?;
    match t {
      Token::Keyword(k) if k.inner == keyword => Ok(k),
      _ => Err(ParseError::unexpected(t, format!("{}", keyword))),
    }
  }
  /// If needed, this will parse a token ahead of the current reader. This will
  /// not affect the current reading position.
  pub fn peek(&mut self, amount: usize) -> Result<&Token> {
    // Prevents reading extra if we find an EOF token
    if self.peeked.get(amount) == Some(&Token::EOF(self.file())) {
      return Ok(&self.peeked[amount]);
    }
    while self.peeked.len() <= amount {
      let t = match self.read_token() {
        // We only want to push to peeked if it is not EOF. This is because if read_token gives
        // use one EOF, all future reads will give us EOF. However, because of borrowing rules,
        // we need to push a single EOF token onto the peeked stack, so that we can borrow into
        // that.
        Ok(Token::EOF(file)) => {
          self.peeked.push_back(Token::EOF(file));
          return Ok(&self.peeked[self.peeked.len() - 1]);
        }
        Ok(t) => t,
        Err(e) => return Err(e.into_parse(self.file)),
      };
      self.peeked.push_back(t);
    }
    Ok(&self.peeked[amount])
  }
  /// This does the same thing as [`peek`](Self::peek), but returns an Ok(None)
  /// when an EOF error whould have occured.
  pub fn peek_opt(&mut self, amount: usize) -> Result<Option<&Token>> {
    // Prevents reading extra if we find an EOF token
    if self.peeked.get(amount) == Some(&Token::EOF(self.file())) {
      return Ok(None);
    }
    while self.peeked.len() <= amount {
      let t = match self.read_token() {
        Ok(Token::EOF(_)) => {
          self.peeked.push_back(Token::EOF(self.file()));
          return Ok(None);
        }
        Ok(t) => t,
        Err(e) => return Err(e.into_parse(self.file)),
      };
      self.peeked.push_back(t);
    }
    Ok(Some(&self.peeked[amount]))
  }
  /// Parses a token from the internal stream, or returns a peeked token.
  pub fn read(&mut self) -> Result<Token> {
    if let Some(c) = self.peeked.pop_front() {
      Ok(c)
    } else {
      self.read_token().map_err(|e| e.into_parse(self.file))
    }
  }
  /// Inserts this into the peek stack. This will cause the next call to read
  /// to return the given token.
  pub fn unread(&mut self, tok: Token) {
    self.peeked.push_front(tok)
  }
  /// This does the same thing as [`read`](Self::read), but returns Ok(None) if
  /// an EOF error would have occured.
  pub fn read_opt(&mut self) -> Result<Option<Token>> {
    if let Some(c) = self.peeked.pop_front() {
      Ok(Some(c))
    } else {
      match self.read_token() {
        Ok(Token::EOF(_)) => Ok(None),
        Ok(t) => Ok(Some(t)),
        Err(e) => Err(e.into_parse(self.file)),
      }
    }
  }

  /// Parses a token from the internal stream.
  fn read_token(&mut self) -> std::result::Result<Token, ParseIOError> {
    let mut s = String::new();
    loop {
      let c = try_eof!(self, self.reader.peek(0)?);
      if c == '/' {
        if self.reader.peek(1)? == Some('/') {
          // Line comment
          while self.reader.peek(0)? != Some('\n') {
            if self.reader.read().unwrap() == None {
              return Ok(Token::EOF(self.file));
            }
          }
          // Consume the newline
          self.reader.read().unwrap().unwrap();
          continue;
        } else if self.reader.peek(1)? == Some('*') {
          // Block comment
          while !(self.reader.peek(0)? == Some('*') && self.reader.peek(1)? == Some('/')) {
            if self.reader.read().unwrap() == None {
              return Err(
                ParseError::unexpected(Token::EOF(self.file), "a closing block comment").into(),
              );
            }
          }
          // Consume the `*` and `/`
          self.reader.read().unwrap().unwrap();
          self.reader.read().unwrap().unwrap();
          continue;
        }
      } else if c.is_whitespace() {
        self.reader.read().unwrap().unwrap();
        continue;
      }
      break;
    }
    let start = self.reader.pos();
    let c = try_eof!(self, self.reader.read()?);
    if c == '"' {
      loop {
        let c = try_eof!(self, self.reader.read()?);
        if c == '"' {
          break;
        }
        s.push(c);
      }
      Ok(Token::Lit(Lit::String(StringLit {
        val: s,
        pos: Span::new(start, self.reader.pos(), self.file),
      })))
    } else if matches!(c, '0'..='9') {
      s.push(c);
      self.parse_number(start, &mut s)
    } else if c == '@' {
      let is_valid = |c| matches!(c, Some('a'..='z' | 'A'..='Z' | '_'));

      loop {
        let c = self.reader.peek(0)?;
        if is_valid(c) {
          s.push(self.reader.read().unwrap().unwrap());
        } else {
          return Ok(Token::Special(Special::Predefined(Predefined {
            val: s,
            pos: Span::new(start, self.reader.pos(), self.file),
          })));
        }
      }
    } else if c == '#' {
      todo!()
    } else {
      // It is not a number of or a string literal, so it will be a keyword or
      // identifier.
      s.push(c);
      // Keywords are greedy. If we find a *, we want to look for another *, so that
      // ** will be parsed as an Exp token, and not two Mul tokens.
      let is_valid_ident = matches!(c, 'a'..='z' | 'A'..='Z');

      loop {
        let c = self.reader.peek(0)?;
        if is_valid_ident {
          if !matches!(c, Some('a'..='z' | 'A'..='Z' | '0'..='9' | '_')) {
            // We have reached the end of an identifier, so we don't consume this token
            if let Some(kw) = Keyword::from_token(&s) {
              return Ok(Token::Keyword(KeywordPos {
                inner: kw,
                pos:   Span::new(start, self.reader.pos(), self.file),
              }));
            }
            return Ok(Token::Ident(Ident {
              inner: s,
              pos:   Span::new(start, self.reader.pos(), self.file),
            }));
          } else {
            self.reader.read().unwrap();
            if let Some(c) = c {
              // We got another valid identifier character, so push it and keep iterating.
              s.push(c);
            } else {
              // End of the stream, and we have a valid ident
              if let Some(kw) = Keyword::from_token(&s) {
                return Ok(Token::Keyword(KeywordPos {
                  inner: kw,
                  pos:   Span::new(start, self.reader.pos(), self.file),
                }));
              }
              return Ok(Token::Ident(Ident {
                inner: s,
                pos:   Span::new(start, self.reader.pos(), self.file),
              }));
            }
          }
        } else {
          // We are trying to match something like >= or ||. Note that we have not pushed
          // c to s yet. This makes sure that single char keywords work as expected.
          if let Some(mut kw) = Keyword::from_token(&s) {
            loop {
              // s could be `=`, in which case we want to look ahead to see if we have
              // something like `==` or `=>`. Since we haven't pushed c to s,
              // the first iteration of this loop makes sense.
              //
              // This relies in the fact that we will always have a valid keyword in these
              // steps. So the keyword =:= would not parse, because =: is not a
              // valid keyword. This is fine, because this is much simpler than
              // the recursive solution. I am aware that this is still
              // recursive, because it does string equality checks every iteration,
              // but checking the =:= case would be much more recursion than what is already
              // here.
              s.push(match self.reader.peek(0)? {
                Some(c) => c,
                None => break,
              });
              if let Some(new_kw) = Keyword::from_token(&s) {
                self.reader.read().unwrap();
                kw = new_kw;
              } else {
                break;
              }
            }
            return Ok(Token::Keyword(KeywordPos {
              inner: kw,
              pos:   Span::new(start, self.reader.pos(), self.file),
            }));
          } else {
            // This is because all our non-ident tokens are valid with only one character
            // known. The main reason we don't just push and try again is so
            // that we don't get weird errors with a much of spaces in the
            // token.
            return Err(
              ParseError::invalid(Token::Ident(Ident {
                inner: s,
                pos:   Span::new(start, self.reader.pos(), self.file),
              }))
              .into(),
            );
            // If we need to check multi character keywords, replace the above
            // with this: ```
            // if let Some(c) = c {
            //   // Invalid keyword, so we consume and try again
            //   self.reader.read().unwrap();
            //   s.push(c);
            // } else {
            //   // End of the stream, and we have an invalid token and invalid
            // ident   return Err(ParseError::invalid(Token::
            // Ident(Ident {     inner: s,
            //     pos:   Span::new(start, self.reader.pos()),
            //   })));
            // }
            // ```
          }
        }
      }
    }
  }

  fn parse_number(
    &mut self,
    start: Pos,
    s: &mut String,
  ) -> std::result::Result<Token, ParseIOError> {
    #[derive(Clone, Copy, Debug, PartialEq)]
    enum NumKind {
      Int,
      Float,
      Hex,
      Octal,
      Bin,
    }
    impl NumKind {
      pub fn is_valid(&self, c: char) -> bool {
        match self {
          Self::Int => matches!(c, '0'..='9'),
          Self::Float => matches!(c, '0'..='9'),
          Self::Hex => matches!(c, '0'..='9' | 'a'..='f' | 'A'..='F'),
          Self::Octal => matches!(c, '0'..='7'),
          Self::Bin => matches!(c, '0'..='1'),
        }
      }
      pub fn base(&self) -> u32 {
        match self {
          Self::Int => 10,
          Self::Float => 10,
          Self::Hex => 16,
          Self::Octal => 8,
          Self::Bin => 2,
        }
      }
    }
    let mut kind = NumKind::Int;
    let mut prev = None;
    while let Some(c) = self.reader.peek(0)? {
      //     0xff
      // Find ^
      if s == "0" {
        let new_kind = match c {
          'x' => Some(NumKind::Hex),
          'o' => Some(NumKind::Octal),
          'b' => Some(NumKind::Bin),
          _ => None,
        };
        if let Some(k) = new_kind {
          kind = k;
          prev = Some(self.pos());
          self.reader.read().unwrap();
          s.push(c);
          continue;
        }
      }
      if kind.is_valid(c) {
        prev = Some(self.pos());
        self.reader.read().unwrap();
      } else if c.is_ascii_hexdigit() {
        s.push(c);
        let start = self.pos();
        self.reader.read().unwrap();
        return Err(
          ParseError::custom(
            format!("Invalid digit `{}`", c),
            Span::new(start, self.pos(), self.file()),
          )
          .into(),
        );
      } else if c == '.' && kind == NumKind::Int {
        if let Some(c2) = self.reader.peek(1)? {
          if NumKind::Float.is_valid(c2) {
            // Consume the dot token, we will consome the number on the next loop.
            self.reader.read().unwrap();
            kind = NumKind::Float;
          } else {
            // 5. is not a float, so we don't set kind to NumKind::Float here. This is so we
            // can parse things like `5.pow(2)` or `0x123.pow(4)`
            break;
          }
        } else {
          break;
        }
      } else {
        break;
      }
      s.push(c);
    }
    Ok(if kind == NumKind::Float {
      Token::Lit(Lit::Float(FloatLit {
        val: f64::from_str(s).unwrap(),
        pos: Span::new(start, self.reader.pos(), self.file),
      }))
    } else if kind == NumKind::Int {
      Token::Lit(Lit::Int(IntLit {
        val: i64::from_str(s).unwrap(),
        pos: Span::new(start, self.reader.pos(), self.file),
      }))
    } else if s.len() == 2 {
      // If we only have `0x` in our string, it's an invalid number
      return Err(
        ParseError::custom(
          "expected digit in literal".into(),
          Span::new(prev.unwrap(), self.pos(), self.file()),
        )
        .into(),
      );
    } else {
      Token::Lit(Lit::Int(IntLit {
        // Remove the `0x` from our string
        val: i64::from_str_radix(&s[2..], kind.base()).unwrap(),
        pos: Span::new(start, self.reader.pos(), self.file),
      }))
    })
  }
}
