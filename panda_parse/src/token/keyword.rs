use std::fmt;

macro_rules! keywords {
  [ $($name:ident => $val:expr,)* ] => {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum Keyword {
      $(
        $name,
      )*
    }

    impl Keyword {
      pub fn from_token(s: &str) -> Option<Self> {
        match s {
          $(
            $val => Some(Keyword::$name),
          )*
          _ => None,
        }
      }
    }

    impl fmt::Display for Keyword {
      fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
          $(
            Keyword::$name => f.write_str($val),
          )*
        }
      }
    }
  }
}

#[rustfmt::skip]
keywords![
  Function => "fn",
  On       => "on",
  When     => "when",

  Impl   => "impl",
  Struct => "struct",
  Trait  => "trait",
  Map    => "map",
  Let    => "let",

  // Arr      => "arr",
  If       => "if",
  Else     => "else",
  For      => "for",
  In       => "in",
  While    => "while",
  Loop     => "loop",
  Break    => "break",
  Continue => "continue",

  True  => "true",
  False => "false",

  Str   => "str",
  Int   => "int",
  Float => "float",
  Bool  => "bool",
  Any   => "any",

  // Imports
  Use => "use",

  OpenBlock  => "{",
  CloseBlock => "}",
  OpenParen  => "(",
  CloseParen => ")",
  OpenArr    => "[",
  CloseArr   => "]",

  Assign  => "=",
  Comma   => ",",
  Dot     => ".",
  Range   => "..",
  Colon   => ":",
  PathSep => "::",

  Add => "+",
  Sub => "-",
  Mul => "*",
  Div => "/",
  Mod => "%",
  Exp => "**",

  AddAssign => "+=",
  SubAssign => "-=",
  MulAssign => "*=",
  DivAssign => "/=",
  ModAssign => "%=",
  ExpAssign => "**=",

  Inc => "++",
  Dec => "--",

  Not => "!",
  Eq => "==",
  Neq => "!=",
  Less => "<",
  Greater => ">",
  GTE => ">=",
  LTE => "<=",

  BitXor => "^",
  BitOr => "|",
  BitAnd => "&",

  Or => "||",
  And => "&&",

  Shl => "<<",
  Shr => ">>",
  Shrz => ">>>",
];
