use ansi_term::{ANSIString, Colour};
use std::{cell::RefCell, cmp::Ordering};

/// A token span. This contains a start and end position. It represents a region
/// of text. Once errors are fully implemented, it will be used to generate
/// messages like this:
///
/// ```text
/// 22 if a == b {
/// 23   println(words)
///              ^^^^^ Unknown variable `words`
/// 24 }
/// ```
///
/// Or
///
/// ```text
/// 22 if a == b {
/// 23   println(,)
///              ^ Unexpected token `,`, expected an expression
/// 24 }
/// ```
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Span {
  file:  u16,
  start: Pos,
  len:   u8,
}
#[macro_export]
macro_rules! span {
  ( $from:literal..$to:literal ) => {
    $crate::token::Span::new(
      $crate::token::Pos::new($from - 1),
      $crate::token::Pos::new($to - 1),
      0,
    )
  };
}
/// A position within a source file. When an error is generated, the index will
/// be used to search both forwards and backwards for a newline. If found, the
/// line will be printed in the error message. If not found, a subsection of the
/// line will be printed. See [`new`](Self::new) for more.
///
/// The index will be used to choose which character the position is
/// referencing. The column will be printed in the error message, but that is
/// not validated with the newlines that may have been found.
///
/// Note that this uses a `u32` for indexing. This enforces a 4GB file size
/// limit. I cannot imagine a situation where this is a problem, but this can be
/// changed if needed.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Pos {
  pub(super) index: u32,
}

impl Default for Span {
  /// Returns the default span, which ranges from line 1, column 1 to
  /// line 1, column 2. This will use the file at index 0. If this is
  /// ever used to underline something, it will not make any sense.
  ///
  /// This should only ever be used when a placeholder is needed, for
  /// example building a node in the tree before you've parsed everything
  /// needed.
  fn default() -> Self {
    Span::new(Pos::new(0), Pos::new(1), 0)
  }
}
impl Default for Pos {
  /// Returns the default position, which is line 1, column 1.
  fn default() -> Self {
    Pos::new(0)
  }
}

struct Color {}
struct NoColor {}

trait Colorizer {
  fn paint(c: Colour, text: String) -> ANSIString<'static>
  where
    Self: Sized;
}

impl Colorizer for Color {
  fn paint(color: Colour, text: String) -> ANSIString<'static> {
    color.paint(text)
  }
}

impl Colorizer for NoColor {
  fn paint(_: Colour, text: String) -> ANSIString<'static> {
    text.into()
  }
}

thread_local! {
  static CALL_SITE: RefCell<Span> = RefCell::new(Span::default())
}

impl Span {
  /// Creates a new span with the given start and end positions. The end is
  /// not inclusive. So for example the span from column 2 to column 4 would
  /// result in this range:
  /// ```text
  /// 1234
  ///  ^^ <- The span from column 2 to 4
  /// ```
  ///
  /// The file is a unique id for each file. This should be obtained from
  /// a tokenizer or the Panda type.
  pub fn new(start: Pos, end: Pos, file: u16) -> Self {
    Span { start, len: (end.index - start.index).try_into().unwrap_or(u8::MAX), file }
  }
  /// Returns the call site of the current function.
  pub fn call_site() -> Self {
    CALL_SITE.with(|v| *v.borrow())
  }
  /// Set's the call site for this thread. Note that this should not be used!
  /// This is public because `panda_runtime` needs to use it, but it should not
  /// be used outside of Panda.
  #[doc(hidden)]
  pub fn set_call_site(span: Span) {
    CALL_SITE.with(|v| *v.borrow_mut() = span);
  }
  /// Creates a new span at the end of the file. This should only be used in
  /// rare situations. Specifically, this is the position that Token::EOF
  /// returns when generating error messages.
  ///
  /// Generally, things like unmatched parenthesis should not produce errors
  /// at the eof, as that is just annoying. This is only used so that cli
  /// errors are easy to generate.
  pub fn eof(file: u16) -> Self {
    Span { start: Pos::new(!0), len: !0, file }
  }
  /// Returns the start of the range. This is inclusive, so this character
  /// should be counted as the first character in the range.
  pub fn start(&self) -> Pos {
    self.start
  }
  /// Returns the end of the range. This is exclusive, so everything before
  /// this character should be counted.
  pub fn end(&self) -> Pos {
    Pos::new(self.start.index + self.len as u32)
  }
  /// Returns the file that this Span is in. This is an index into a list of
  /// files in the Panda type.
  pub fn file(&self) -> u16 {
    self.file
  }
  /// Returns the joined span of `self` and `other`.
  pub fn join(&self, other: Span) -> Span {
    Span::new(self.start.min(other.start), self.end().max(other.end()), self.file)
  }
  /// Produces an underline for the given source code, with the given message
  /// displayed next to that message. This will only attempt to look for a
  /// newline 100 characters ahead and backwards from `self.index` within `src`,
  /// in order to avoid issues where the whole file is on one line.
  pub fn underline(&self, src: &str, path: &str, msg: &str, color: bool) -> String {
    if self == &Span::eof(self.file) {
      // Line and column numbers start at 1
      let actual = Span::new(Pos::new(src.len() as u32 - 1), Pos::new(src.len() as u32), self.file);
      return actual.underline(src, path, msg, color);
    }
    if color {
      self.underline_inner::<Color>(src, path, msg)
    } else {
      self.underline_inner::<NoColor>(src, path, msg)
    }
  }
  // Clippy tries to remove the `to_string()` call on the red painted text, which
  // breaks the output.
  #[allow(clippy::unnecessary_to_owned)]
  fn underline_inner<C: Colorizer>(&self, src: &str, path: &str, msg: &str) -> String {
    let mut out = String::new();

    out += &format!(" in `{}`:\n", path,);

    let (start_line, start_col, prev, next) = Span::find_newlines(src, self.start.index, 100);

    let lines_away = 2;
    let num_width = (start_line + lines_away).to_string().len();

    for (i, pos) in prev.iter().enumerate().rev().take(lines_away + 1).rev() {
      if i == prev.len() - 1 {
        break;
      }
      let pos = *pos;
      let line = start_line - (prev.len() - i - 1);
      out += &format!(
        " {}{} {}\n",
        " ".repeat(num_width - line.to_string().len()),
        C::paint(Colour::Blue, line.to_string()),
        &src[pos..prev[i + 1] - 1]
      );
    }
    if let Some(start) = prev.last() {
      if let Some(end) = next.first() {
        out += &format!(
          " {}{} {}\n",
          " ".repeat(num_width - start_line.to_string().len()),
          C::paint(Colour::Blue, start_line.to_string()),
          &src[*start..*end]
        );
      }
    } else {
    }
    // TODO: Multiline
    out += &C::paint(
      Colour::Red,
      format!(" {}{} {}\n", " ".repeat(start_col + num_width), "^".repeat(self.len as usize), msg),
    )
    .to_string();
    for (i, pos) in next.iter().enumerate().take(lines_away) {
      if i == next.len() - 1 {
        break;
      }
      let line = start_line + i + 1;
      out += &format!(
        " {}{} {}\n",
        " ".repeat(num_width - line.to_string().len()),
        C::paint(Colour::Blue, line.to_string()),
        &src[*pos + 1..next[i + 1]]
      );
    }
    out
  }

  /// Finds the newlines in the source string. This will look at most `dist`
  /// characters away from index.
  ///
  /// The first returned array is a list of newlines found before the index. All
  /// of these are offset by one, so that a value of zero means we found the
  /// start of the source string.
  ///
  /// The second returned array is a list of newlines after index. These are not
  /// offset, so that `src.len()` can be used to signify the end of the source
  /// string.
  fn find_newlines(src: &str, index: u32, dist: u32) -> (usize, usize, Vec<usize>, Vec<usize>) {
    let mut iter = src.chars().enumerate();
    let mut prev_newlines = vec![];
    let mut next_newlines = vec![];
    let mut line = 1;
    let mut col = 1;
    let mut prev_was_newline = false;
    if index <= dist {
      prev_newlines.push(0);
    } else {
      // TODO: Cache this somewhere
      for (pos, c) in iter.by_ref() {
        if pos >= (index - dist) as usize {
          break;
        }
        if c == '\n' {
          line += 1;
          col = 1;
        } else {
          col += 1;
        }
      }
    }
    for _ in 0..dist * 2 {
      let (pos, c) = match iter.next() {
        Some(v) => v,
        None => {
          // This handles a newline at the end of a file correctly.
          if !prev_was_newline {
            next_newlines.push(src.len());
          }
          break;
        }
      };
      if pos < index as usize {
        if c == '\n' {
          line += 1;
          col = 1;
        } else {
          col += 1;
        }
      }
      prev_was_newline = c == '\n';
      if c == '\n' {
        if pos > index as usize {
          next_newlines.push(pos)
        } else {
          prev_newlines.push(pos + 1)
        }
      }
    }
    (line, col, prev_newlines, next_newlines)
  }
}
impl Pos {
  /// Creates a new position. The line and column are for human readability,
  /// and the index is how this fetches the source code when generating errors.
  /// So an invalid combination of these values would result in some source
  /// code being displayed for the wrong line.
  ///
  /// Generally, you shouldn't need to call this. The tokenizer will generate
  /// spans for every token it parses, so you should just re-use the positions
  /// it returns instead of building your own.
  pub fn new(index: u32) -> Self {
    Pos { index }
  }
  /// Returns the index of this position. This is the byte index into the
  /// source, not the char index.
  #[inline(always)]
  pub fn index(&self) -> u32 {
    self.index
  }

  /// Finds the line and column of the span in the given source.
  pub fn line_col(&self, src: &str) -> (u32, u32) {
    let mut line = 1;
    let mut col = 1;
    for (i, c) in src.chars().enumerate() {
      if i >= self.index as usize {
        break;
      }
      if c == '\n' {
        line += 1;
        col = 1;
      } else {
        col += 1;
      }
    }
    (line, col)
  }
}

impl PartialOrd for Pos {
  fn partial_cmp(&self, other: &Pos) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

impl Ord for Pos {
  fn cmp(&self, other: &Pos) -> Ordering {
    self.index.cmp(&other.index)
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn underline() {
    let src = "words\nhere\nlots\nof\nwords";
    let res =
      Span::new(Pos::new(12), Pos::new(14), 0).underline(src, "lots of words", "big gaming", true);
    let expected = format!(
      " in `lots of words`:\n {} words\n {} here\n {} lots\n{} {} of\n {} words\n",
      Colour::Blue.paint(1.to_string()),
      Colour::Blue.paint(2.to_string()),
      Colour::Blue.paint(3.to_string()),
      Colour::Red.paint("    ^^ big gaming\n"),
      Colour::Blue.paint(4.to_string()),
      Colour::Blue.paint(5.to_string())
    );
    println!("res:\n{}", res);
    println!("expected:\n{}", expected);
    assert_eq!(res, expected,);

    let src = "words\nhere\nlots\nof\nwords";
    let res =
      Span::new(Pos::new(12), Pos::new(14), 0).underline(src, "lots of words", "big gaming", false);
    println!("{}", res);
    assert_eq!(
      res,
      format!(
        " in `lots of words`:\n 1 words\n 2 here\n 3 lots\n    ^^ big gaming\n 4 of\n 5 words\n",
      )
    );
  }
}
