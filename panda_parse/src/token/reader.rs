use super::Pos;
use std::{
  collections::VecDeque,
  io::{self, BufReader, Read},
};
use utf8_chars::BufReadCharsExt;

pub struct PeekReader<R> {
  reader: BufReader<R>,
  // The items at the end of this list are further along the list. The items at the start
  // are at the start of the list.
  peeked: VecDeque<char>,
  pos:    Pos,
}

impl<R: Read> PeekReader<R> {
  pub fn new(reader: R) -> Self {
    PeekReader { reader: BufReader::new(reader), peeked: VecDeque::new(), pos: Pos::new(0) }
  }
  /// Sets the current position of the reader.
  pub fn set_pos(&mut self, pos: Pos) {
    self.pos = pos;
  }
  pub fn peek(&mut self, amount: usize) -> io::Result<Option<char>> {
    while self.peeked.len() <= amount {
      match self.reader.read_char()? {
        Some(c) => self.peeked.push_back(c),
        None => break,
      }
    }
    if self.peeked.len() <= amount {
      Ok(None)
    } else {
      Ok(Some(self.peeked[amount]))
    }
  }
  pub fn read(&mut self) -> io::Result<Option<char>> {
    if let Some(c) = self.peeked.pop_front() {
      self.pos.index += 1;
      /*
      if c == '\n' {
        self.pos.line += 1;
        self.pos.col = 1;
      } else {
        self.pos.col += 1;
      }
      */
      Ok(Some(c))
    } else {
      match self.reader.read_char() {
        Ok(Some(c)) => {
          self.pos.index += 1;
          /*
          if c == '\n' {
            self.pos.line += 1;
            self.pos.col = 1;
          } else {
            self.pos.col += 1;
          }
          */
          Ok(Some(c))
        }
        v => v,
      }
    }
  }
  pub fn pos(&self) -> Pos {
    self.pos
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn peek() {
    let mut r = PeekReader::new("hello".as_bytes());
    assert_eq!(r.read().unwrap().unwrap(), 'h');
    assert_eq!(r.read().unwrap().unwrap(), 'e');
    assert_eq!(r.read().unwrap().unwrap(), 'l');
    assert_eq!(r.read().unwrap().unwrap(), 'l');
    assert_eq!(r.read().unwrap().unwrap(), 'o');

    let mut r = PeekReader::new("hello".as_bytes());
    assert_eq!(r.peek(0).unwrap().unwrap(), 'h');
    assert_eq!(r.peek(0).unwrap().unwrap(), 'h');
    assert_eq!(r.peek(0).unwrap().unwrap(), 'h');
    assert_eq!(r.read().unwrap().unwrap(), 'h');
    assert_eq!(r.peek(0).unwrap().unwrap(), 'e');
    assert_eq!(r.read().unwrap().unwrap(), 'e');
    assert_eq!(r.read().unwrap().unwrap(), 'l');
    assert_eq!(r.read().unwrap().unwrap(), 'l');
    assert_eq!(r.peek(0).unwrap().unwrap(), 'o');
    assert_eq!(r.read().unwrap().unwrap(), 'o');
    assert_eq!(r.peek(0).unwrap(), None);
    assert_eq!(r.peek(0).unwrap(), None);
    assert_eq!(r.peek(0).unwrap(), None);

    let mut r = PeekReader::new("hello".as_bytes());
    assert_eq!(r.peek(2).unwrap().unwrap(), 'l');
    assert_eq!(r.peek(2).unwrap().unwrap(), 'l');
    assert_eq!(r.peek(1).unwrap().unwrap(), 'e');
    assert_eq!(r.peek(0).unwrap().unwrap(), 'h');
    assert_eq!(r.read().unwrap().unwrap(), 'h');
    assert_eq!(r.read().unwrap().unwrap(), 'e');
    assert_eq!(r.read().unwrap().unwrap(), 'l');
    assert_eq!(r.read().unwrap().unwrap(), 'l');
    assert_eq!(r.read().unwrap().unwrap(), 'o');
    assert_eq!(r.peek(0).unwrap(), None);
    assert_eq!(r.peek(1).unwrap(), None);
    assert_eq!(r.peek(2).unwrap(), None);
  }
}
