use super::*;

#[test]
fn read_ident() -> Result<()> {
  let mut tok = Tokenizer::new("hello world".as_bytes(), 0);
  assert_eq!(tok.pos(), Pos::new(0));
  assert_eq!(
    tok.peek(0),
    Ok(&Token::Ident(Ident::new("hello", Span::new(Pos::new(0), Pos::new(5), 0))))
  );
  assert_eq!(tok.pos(), Pos::new(0));
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("hello", Span::new(Pos::new(0), Pos::new(5), 0))))
  );
  assert_eq!(tok.pos(), Pos::new(5));
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("world", Span::new(Pos::new(6), Pos::new(11), 0))))
  );
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_keyword() -> Result<()> {
  let mut tok = Tokenizer::new("{ (   )   }world fn main()".as_bytes(), 0);
  assert_eq!(
    tok.read(),
    Ok(Token::Keyword(KeywordPos {
      inner: Keyword::OpenBlock,
      pos:   Span::new(Pos::new(0), Pos::new(1), 0),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Keyword(KeywordPos {
      inner: Keyword::OpenParen,
      pos:   Span::new(Pos::new(2), Pos::new(3), 0),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Keyword(KeywordPos {
      inner: Keyword::CloseParen,
      pos:   Span::new(Pos::new(6), Pos::new(7), 0),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Keyword(KeywordPos {
      inner: Keyword::CloseBlock,
      pos:   Span::new(Pos::new(10), Pos::new(11), 0),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("world", Span::new(Pos::new(11), Pos::new(16), 0))))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Keyword(KeywordPos {
      inner: Keyword::Function,
      pos:   Span::new(Pos::new(17), Pos::new(19), 0),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Ident(Ident::new("main", Span::new(Pos::new(20), Pos::new(24), 0))))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Keyword(KeywordPos {
      inner: Keyword::OpenParen,
      pos:   Span::new(Pos::new(24), Pos::new(25), 0),
    }))
  );
  assert_eq!(
    tok.read(),
    Ok(Token::Keyword(KeywordPos {
      inner: Keyword::CloseParen,
      pos:   Span::new(Pos::new(25), Pos::new(26), 0),
    }))
  );
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_int() -> Result<()> {
  let mut tok = Tokenizer::new("0 5 -10".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 0));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 10));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_float() -> Result<()> {
  let mut tok = Tokenizer::new("0.0 5.0 -10.0".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 0.0));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 5.0));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 10.0));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_str() -> Result<()> {
  let mut tok = Tokenizer::new(r#""Hello" "world" "big""gaming""#.as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_str() == "Hello"));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_str() == "world"));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_str() == "big"));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_str() == "gaming"));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_minus() -> Result<()> {
  let mut tok = Tokenizer::new("-".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("-5".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("- 5".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("-5.0".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 5.0));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("- 5.0".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 5.0));

  let mut tok = Tokenizer::new("3 - -5".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("3 - - 5".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Sub));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("3 -- 5".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Dec));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_eof() -> Result<()> {
  let mut tok = Tokenizer::new("".as_bytes(), 0);
  assert_eq!(tok.peek(0), Ok(&Token::EOF(0)));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("(3)".as_bytes(), 0);
  assert_eq!(tok.peek(3), Ok(&Token::EOF(0)));
  assert!(matches!(tok.peek(2).unwrap(), t if t.is_keyword(Keyword::CloseParen)));
  assert!(matches!(tok.peek(1).unwrap(), Token::Lit(l) if l.unwrap_int() == 3));
  assert!(matches!(tok.peek(0).unwrap(), t if t.is_keyword(Keyword::OpenParen)));

  let mut tok = Tokenizer::new("(3)".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), t if t.is_keyword(Keyword::OpenParen)));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 3));
  assert_eq!(tok.peek_opt(1), Ok(None));
  assert!(matches!(tok.read().unwrap(), t if t.is_keyword(Keyword::CloseParen)));
  assert_eq!(tok.peek_opt(0), Ok(None));

  let mut tok = Tokenizer::new("(3)".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), t if t.is_keyword(Keyword::OpenParen)));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 3));
  assert_eq!(tok.peek(1), Ok(&Token::EOF(0)));
  assert!(matches!(tok.read().unwrap(), t if t.is_keyword(Keyword::CloseParen)));
  assert_eq!(tok.peek(0), Ok(&Token::EOF(0)));

  let mut tok = Tokenizer::new("(3)".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), t if t.is_keyword(Keyword::OpenParen)));
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 3));
  assert_eq!(tok.peek(1), Ok(&Token::EOF(0)));
  assert!(matches!(tok.read().unwrap(), t if t.is_keyword(Keyword::CloseParen)));
  // There was a bug where `read` would insert an EOF into the peeked stack,
  // and peek_opt would not check anything in the stack, and it would return
  // Ok(EOF) here, which is invalid.
  assert_eq!(tok.peek_opt(0), Ok(None));
  Ok(())
}

#[test]
fn read_exp() -> Result<()> {
  let mut tok = Tokenizer::new("*".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Mul));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("**".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Exp));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("* *".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Mul));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Mul));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("*a*".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Mul));
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "a"));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Mul));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_dot() -> Result<()> {
  let mut tok = Tokenizer::new(".".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Dot));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("5.".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert!(matches!(tok.read().unwrap(), Token::Keyword(k) if k.value() == Keyword::Dot));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("5.0".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_float() == 5.0));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn read_comment() -> Result<()> {
  let mut tok = Tokenizer::new("// hello".as_bytes(), 0);
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("5 // hello".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("//hello\n5".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Lit(l) if l.unwrap_int() == 5));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("//hello\nhumans".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "humans"));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("words\n//hello".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "words"));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("words\n  //hello".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "words"));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));

  let mut tok = Tokenizer::new("  //hello\n  words".as_bytes(), 0);
  assert!(matches!(tok.read().unwrap(), Token::Ident(i) if i.as_ref() == "words"));
  assert_eq!(tok.read(), Ok(Token::EOF(0)));
  Ok(())
}

#[test]
fn numbers() {
  fn assert_int(text: &str, num: i64) {
    let mut tok = Tokenizer::new(text.as_bytes(), 0);
    match tok.read().unwrap() {
      Token::Lit(l) => match l {
        Lit::Int(v) => assert_eq!(v.val, num),
        l => panic!("not an int: {:?}", l),
      },
      t if t.is_keyword(Keyword::Sub) => match tok.read().unwrap() {
        Token::Lit(l) => match l {
          Lit::Int(v) => assert_eq!(-v.val, num),
          l => panic!("not an int: {:?}", l),
        },
        t => panic!("not a number: {:?}", t),
      },
      t => panic!("not a number: {:?}", t),
    }
  }
  fn assert_invalid(text: &str) {
    let mut tok = Tokenizer::new(text.as_bytes(), 0);
    if let Ok(v) = tok.read() {
      panic!("expected invalid, got {:?}", v);
    }
  }
  assert_int("5", 5);
  assert_int("00055", 55);
  assert_int("0o55", 0o55);
  assert_int("0x55", 0x55);
  assert_int("-0x55", -0x55);
  assert_int("0b0101", 5);
  assert_invalid("0a");
  assert_invalid("0b");
  assert_invalid("0c");
  assert_invalid("0o");
  assert_invalid("0x");
  assert_invalid("0b3");
  assert_invalid("0xz");
  assert_invalid("0o9");
}
