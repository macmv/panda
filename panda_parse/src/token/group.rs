use super::{super::syntax::Parse, Keyword, ParseError, Result, Span, Tokenizer};
use std::io::Read;

/// Stores a comma delimited list. Trailing comma is optional.
///
/// This stores `T` in a `Box` or `Vec`, in order to allow this to be stored in
/// things like an `Expr` without recursion.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Group<T> {
  items: Vec<(T, Span)>,
  last:  Option<Box<T>>,
  pos:   Span,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum GroupKind {
  Paren,
  Arr,
  Block,
}

impl GroupKind {
  pub fn start(&self) -> Keyword {
    match self {
      Self::Paren => Keyword::OpenParen,
      Self::Arr => Keyword::OpenArr,
      Self::Block => Keyword::OpenBlock,
    }
  }
  pub fn end(&self) -> Keyword {
    match self {
      Self::Paren => Keyword::CloseParen,
      Self::Arr => Keyword::CloseArr,
      Self::Block => Keyword::CloseBlock,
    }
  }
}

impl<T: Parse<Output = T>> Group<T> {
  pub fn parse<R: Read>(tok: &mut Tokenizer<R>, kind: GroupKind) -> Result<Self> {
    let mut items = vec![];
    let mut last = None;
    let start = tok.pos();
    tok.expect(kind.start())?;
    let end_kw = kind.end();
    loop {
      if tok.peek(0)?.is_keyword(end_kw) {
        tok.read().unwrap();
        break;
      }
      let val = T::parse(tok)?;
      match tok.read()? {
        t if t.is_keyword(end_kw) => {
          last = Some(Box::new(val));
          break;
        }
        t if t.is_keyword(Keyword::Comma) => items.push((val, t.pos())),
        t => return Err(ParseError::unexpected(t, format!("a `,` or `{end_kw}`"))),
      }
    }
    let end = tok.pos();
    Ok(Group { items, last, pos: Span::new(start, end, tok.file()) })
  }
}

impl<T> Group<T> {
  pub fn empty(pos: Span) -> Self {
    Group { items: vec![], last: None, pos }
  }
  pub fn pos(&self) -> Span {
    self.pos
  }
  pub fn into_values(self) -> Vec<T> {
    let mut items = self.items.into_iter().map(|(v, _)| v).collect::<Vec<_>>();
    if let Some(last) = self.last {
      items.push(*last);
    }
    items
  }
}
