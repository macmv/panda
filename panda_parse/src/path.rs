use crate::token::Ident;
use std::{fmt, mem};

#[macro_export]
macro_rules! path {
  ( $( $name:ident )::* ) => {
    $crate::Path::new(vec![ $( stringify!($name).into() ),*])
  };
}

/// A type path. This can be either an absolute path (when used within a
/// VarType), or a partial path, for `use` statements. This can be mixed with
/// path literals in source code, so for example all of these snippets resolve
/// to the same type:
///
/// ```ignore
/// my::custom::ty::hello();
/// ```
///
/// ```ignore
/// // Parser error, because modules are imported by default.
/// use my;
/// my::custom::ty::hello();
/// ```
///
/// ```ignore
/// use my::custom;
/// custom::ty::hello();
/// ```
///
/// ```ignore
/// use my::custom::ty;
/// ty::hello();
/// ```
///
/// ```ignore
/// use my::custom::ty::hello;
/// hello();
/// ```
#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Path {
  segments: Vec<String>,
}

impl fmt::Display for Path {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", self.segments.join("::"))
  }
}

impl From<String> for Path {
  fn from(s: String) -> Path {
    Path { segments: vec![s] }
  }
}
impl From<&str> for Path {
  fn from(s: &str) -> Path {
    Path { segments: vec![s.into()] }
  }
}
impl From<Ident> for Path {
  fn from(s: Ident) -> Path {
    Path { segments: vec![s.into()] }
  }
}

impl Path {
  /// Creates a new path. If the given vec is empty, an empty string will
  /// be inserted, so that [`last`](Self::last) will never panic.
  ///
  /// See the macro [`path`](crate::path) for an cleaner alternative.
  pub fn new(mut segments: Vec<String>) -> Self {
    for (i, seg) in segments.iter().enumerate() {
      if i != 0 && seg.is_empty() {
        panic!("cannot have empty path segment that is not the first element {:?}", segments);
      }
    }
    if segments.is_empty() {
      segments.push("".into());
    }
    Path { segments }
  }
  /// The empty path. This can be used to reference to top level module.
  /// The internal list still has one element, which is an empty string.
  /// So [`last`](Self::last) will still return something.
  pub fn empty() -> Self {
    Path { segments: vec!["".into()] }
  }
  /// Joins the segments of self and other, returning them in a new path.
  /// This needs to clone all of self and other, so this should be changed
  /// in the future.
  pub fn join(&self, other: &Path) -> Self {
    let mut segments = Vec::with_capacity(self.segments.len() + other.segments.len());
    for seg in self.segments.iter() {
      if !seg.is_empty() {
        segments.push(seg.clone());
      }
    }
    for seg in other.segments.iter() {
      if !seg.is_empty() {
        segments.push(seg.clone());
      }
    }
    if segments.is_empty() {
      segments.push("".into());
    }
    Path { segments }
  }
  /// Returns the last segment in this path. If this is an empty path, it
  /// will return ""
  pub fn last(&self) -> &str {
    self.segments.last().unwrap()
  }
  /// Returns the first segment in this path. If this is an empty path, it
  /// will return ""
  pub fn first(&self) -> &str {
    self.segments.first().unwrap()
  }
  /// Returns the number if segments in this path. If the path is empty, then
  /// this will return 0, even though there is one internal element.
  pub fn len(&self) -> usize {
    if self.segments[0].is_empty() {
      0
    } else {
      self.segments.len()
    }
  }
  /// Returns true if the path is empty.
  pub fn is_empty(&self) -> bool {
    self.len() == 0
  }
  /// Pops the last element of this path. If there are no elements after the
  /// pop, an empty string is added as the sole path element.
  pub fn pop(&mut self) -> String {
    let ret = self.segments.pop().unwrap();
    if self.segments.is_empty() {
      self.segments.push("".into());
    }
    ret
  }
  /// Pushes an element onto the path. If the current path is empty, then that
  /// element will be replaced with the given one.
  pub fn push(&mut self, val: String) {
    if self.segments.len() == 1 && self.segments[0].is_empty() {
      self.segments[0] = val
    } else {
      self.segments.push(val)
    }
  }
  pub fn push_prefix(&mut self, val: String) {
    if self.segments.len() == 1 && self.segments[0].is_empty() {
      self.segments[0] = val
    } else {
      self.segments.insert(0, val)
    }
  }
  pub fn push_all_prefix(&mut self, mut prefix: Path) {
    match (self.is_empty(), prefix.is_empty()) {
      (_, true) => {}
      (true, false) => *self = prefix,
      (false, false) => {
        mem::swap(&mut self.segments, &mut prefix.segments);
        self.segments.extend(prefix.segments);
      }
    }
  }
  pub fn inner(&self) -> &[String] {
    &self.segments
  }
}
