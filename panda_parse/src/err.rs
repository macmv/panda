use crate::token::Span;
use std::{fmt::Display, path::Path};

pub trait PdError: Display {
  /// Returns the positition of this error.
  fn pos(&self) -> Span;
  /// Prints the error message to stdout.
  fn print(&self, src: &str, path: &Path, color: bool);
  /// Generates the error message as a string.
  fn gen(&self, src: &str, path: &Path, color: bool) -> String;
}
