use crate::{
  syntax::Parse,
  token::{Keyword, Result, Span, Tokenizer},
  tree::{Expr, Loop, StatementList},
};
use std::io::Read;

impl Loop {
  pub fn parse_for<R: Read>(tok: &mut Tokenizer<R>, keyword_pos: Span) -> Result<Self> {
    let first = tok.read()?.ident("a variable")?;
    let t = tok.read()?;
    let mut second = None;
    if t.is_keyword(Keyword::Comma) {
      second = Some(tok.read()?.ident("a variable")?);
      tok.expect(Keyword::In)?;
    } else if !t.is_keyword(Keyword::In) {
      t.ident("`,` or `in`")?;
    }
    let iter = Expr::parse(tok)?;

    tok.expect(Keyword::OpenBlock)?;
    let block = StatementList::parse(tok)?;
    tok.expect(Keyword::CloseBlock)?;

    Ok(Loop::For { first, second, iter, block, keyword_pos })
  }

  pub fn parse_while<R: Read>(tok: &mut Tokenizer<R>, keyword_pos: Span) -> Result<Self> {
    let cond = Expr::parse(tok)?;

    tok.expect(Keyword::OpenBlock)?;
    let block = StatementList::parse(tok)?;
    tok.expect(Keyword::CloseBlock)?;

    Ok(Loop::While { cond, block, keyword_pos })
  }
  pub fn parse_loop<R: Read>(tok: &mut Tokenizer<R>, keyword_pos: Span) -> Result<Self> {
    tok.expect(Keyword::OpenBlock)?;
    let block = StatementList::parse(tok)?;
    tok.expect(Keyword::CloseBlock)?;

    Ok(Loop::Loop { block, keyword_pos })
  }
}
