use crate::{
  syntax::Parse,
  token::{
    FloatLit, Group, GroupKind, Ident, IntLit, Keyword, Lit as TokenLit, ParseError, Result, Span,
    Special, Token, Tokenizer,
  },
  tree::{BoolLit, Closure, Expr, Lit, MapItem, Op, StatementList, StructItem, StructLit},
};
use std::io::Read;

impl Parse for MapItem {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    let key = Expr::parse(tok)?;
    tok.expect(Keyword::Colon)?;
    let val = Expr::parse(tok)?;
    Ok(MapItem { key, val })
  }
}

impl Parse for StructItem {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    let name = tok.read()?.ident("a struct key")?;
    tok.expect(Keyword::Colon)?;
    let val = Expr::parse(tok)?;
    Ok(StructItem { name, val })
  }
}

impl Parse for Expr {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    let _start = tok.peek(0)?.pos().start();
    let lhs = Expr::parse_bp(tok, 0)?;
    /*
    let is_not = tok.peek(0)?.is_keyword(Keyword::Not);
    if is_not {
      tok.read().unwrap();
    }
    let mut val = Expr::parse_val(tok)?;
    if is_not {
      // !a + 5 is the same as (!a) + 5, so we apply the not op before math op
      val = Expr::Bin(BinOp::not(start, val));
    }
    let mut lhs = Expr::parse_math(tok, val)?;
    loop {
      match tok.peek(0)? {
        t if t.is_keyword(Keyword::Dot) || t.is_keyword(Keyword::OpenArr) => {
          lhs = Expr::parse_math(tok, lhs)?
        }
        t @ Token::Keyword(k) if MathOp::from_keyword(k, None, None, t.pos()).is_some() => {
          lhs = Expr::parse_math(tok, lhs)?
        }
        _ => break,
      }
    }
    match tok.peek(0)? {
      t if t.is_keyword(Keyword::Eq)
        || t.is_keyword(Keyword::Neq)
        || t.is_keyword(Keyword::LogicOr)
        || t.is_keyword(Keyword::LogicAnd)
        || t.is_keyword(Keyword::Neq)
        || t.is_keyword(Keyword::Less)
        || t.is_keyword(Keyword::Greater)
        || t.is_keyword(Keyword::LTE)
        || t.is_keyword(Keyword::GTE) =>
      {
        let t = tok.read().unwrap();
        let k = t.keyword("").unwrap();
        let rhs = Expr::parse(tok)?;
        Ok(Expr::Bin(BinOp::new(k, lhs, rhs)))
      }
      _ => Ok(lhs),
    }
    */
    Ok(lhs)
  }
}

impl Expr {
  /// Parses an expression recursively. This is a list of values, with things
  /// like + or - between them.
  ///
  /// This is a Pratt parser. Read this article:
  /// https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html
  fn parse_bp<R: Read>(tok: &mut Tokenizer<R>, min_bp: u8) -> Result<Self> {
    let t = tok.peek(0)?;
    let mut lhs = if let Some(op) = Op::from_tok(t) {
      if let Some(r_bp) = op.prefix_bp() {
        let _t = tok.read().unwrap();
        let rhs = Self::parse_bp(tok, r_bp)?;
        Expr::Op(None, Some(Box::new(rhs)), op)
      } else {
        Self::parse_val(tok)?
      }
    } else {
      Self::parse_val(tok)?
    };

    loop {
      let t = match tok.peek_opt(0)? {
        Some(t) => t,
        None => return Ok(lhs),
      };
      let op = match Op::from_tok(t) {
        Some(op) => op,
        None => return Ok(lhs),
      };
      if let Some(l_bp) = op.postfix_bp() {
        if l_bp < min_bp {
          break;
        }
        let _t = tok.read().unwrap();

        lhs = if op == Op::Arr {
          let rhs = Self::parse_bp(tok, 0)?;
          tok.expect(Keyword::CloseArr)?;
          Expr::Op(Some(Box::new(lhs)), Some(Box::new(rhs)), op)
        } else {
          Expr::Op(Some(Box::new(lhs)), None, op)
        };
        continue;
      }
      if let Some((l_bp, r_bp)) = op.infix_bp() {
        if l_bp < min_bp {
          break;
        }
        let _t = tok.read().unwrap();

        let rhs = Self::parse_bp(tok, r_bp)?;
        lhs = Expr::Op(Some(Box::new(lhs)), Some(Box::new(rhs)), op);
        continue;
      }
      break;
    }
    Ok(lhs)
  }

  /// Parses a single value. This could be a string literal, a function call, a
  /// variable reference, or a number.
  fn parse_val<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    match tok.read()? {
      t if t.is_keyword(Keyword::Sub) => match tok.read()? {
        Token::Lit(l) => match l {
          TokenLit::Int(v) => {
            Ok(Expr::Lit(IntLit { val: -v.val, pos: t.pos().join(v.pos) }.into()))
          }
          TokenLit::Float(v) => {
            Ok(Expr::Lit(FloatLit { val: -v.val, pos: t.pos().join(v.pos) }.into()))
          }
          t2 => Err(ParseError::unexpected(Token::Lit(t2), "a number")),
        },
        t2 => Err(ParseError::unexpected(t2, "a number")),
      },
      Token::Lit(l) => match l {
        TokenLit::Int(v) => Ok(Expr::Lit(v.into())),
        TokenLit::Float(v) => Ok(Expr::Lit(v.into())),
        TokenLit::String(v) => Ok(Expr::Lit(v.into())),
      },
      Token::Ident(name) => Self::parse_ident(tok, name),
      Token::Special(Special::Predefined(p)) => Ok(Expr::Predefined(p)),
      t if t.is_keyword(Keyword::Or) || t.is_keyword(Keyword::BitOr) => {
        tok.unread(t);
        Ok(Expr::Closure(Closure::parse(tok)?))
      }
      t if t.is_keyword(Keyword::OpenParen) => {
        let expr = Expr::parse(tok)?;
        tok.expect(Keyword::CloseParen)?;
        Ok(Expr::Paren(Box::new(expr)))
      }
      t if t.is_keyword(Keyword::OpenArr) => {
        tok.unread(t);
        Ok(Expr::Lit(Lit::Arr(Group::parse(tok, GroupKind::Arr)?)))
      }
      t if t.is_keyword(Keyword::OpenBlock) => {
        let start = t.pos().start();
        let block = StatementList::parse(tok)?;
        tok.expect(Keyword::CloseBlock).unwrap();
        Ok(Expr::Block(Span::new(start, tok.pos(), tok.file()), block))
      }
      t if t.is_keyword(Keyword::True) => {
        Ok(Expr::Lit(Lit::Bool(BoolLit { val: true, pos: t.pos() })))
      }
      t if t.is_keyword(Keyword::False) => {
        Ok(Expr::Lit(Lit::Bool(BoolLit { val: false, pos: t.pos() })))
      }
      t if t.is_keyword(Keyword::Map) => {
        Ok(Expr::Lit(Lit::Map(Group::parse(tok, GroupKind::Block)?)))
      }
      /*
      Token::Ident(name) => {
        match tok.peek(0)? {
          // A function call
          t if t.is_keyword(Keyword::OpenParen) => {
            Ok(Expr::FuncCall(FuncCall::parse(tok, None, name.into())?))
          }
          t if t.is_keyword(Keyword::OpenBlock) => {
            // We could have one of three things:
            //
            // a = name { field: value, ...
            //
            // if name { ...
            //
            // name {}
            //
            // For the last option, we assume a struct literal, simply because
            // an empty conditional block doesn't make that much sense.
            //
            if tok.peek(1)?.is_keyword(Keyword::CloseBlock)
              || (tok.peek(1)?.is_ident() && tok.peek(2)?.is_keyword(Keyword::Colon))
            {
              // This is enough to ensure a struct literal.
              Ok(Expr::StructLit(StructLit::parse(tok, name.into())?))
            } else {
              // In this situation, the `{` we found was probably an if or for block,
              // so we can ignore it.
              Ok(Expr::VarRef(name.into_path()))
            }
          }
          t if t.is_keyword(Keyword::PathSep) => {
            // Variables can also be paths, so we are at a struct lit, a function call, or a
            // var ref.
            //
            // name::something { field: value, ...
            //
            // if name::something {
            //
            // name::something(args, ...
            //
            let path = PathLit::parse(tok, name)?;
            if tok.peek(0)?.is_keyword(Keyword::OpenBlock) {
              if tok.peek(1)?.is_keyword(Keyword::CloseBlock)
                || (tok.peek(1)?.is_ident() && tok.peek(2)?.is_keyword(Keyword::Colon))
              {
                // This is enough to ensure a struct literal.
                Ok(Expr::StructLit(StructLit::parse(tok, path)?))
              } else {
                // In this situation, the `{` we found was probably an if or for block,
                // so we can ignore it.
                Ok(Expr::VarRef(path))
              }
            } else if tok.peek(0)?.is_keyword(Keyword::OpenParen) {
              Ok(Expr::FuncCall(FuncCall::parse(tok, None, path)?))
            } else {
              // Anything else -> var ref
              Ok(Expr::VarRef(path))
            }
          }
          // Anything else means a variable reference
          _ => Ok(Expr::VarRef(name.into_path())),
        }
      }
      */
      t => Err(ParseError::unexpected(t, "an expression")),
    }
  }

  fn parse_ident<R: Read>(tok: &mut Tokenizer<R>, name: Ident) -> Result<Expr> {
    match tok.peek(0)? {
      // A function call
      t if t.is_keyword(Keyword::OpenParen) => {
        Ok(Expr::Call(name, Group::parse(tok, GroupKind::Paren)?))
      }
      // A struct lit or if statement.
      // A struct literal would look like this:
      //
      // MyStruct { foo : bar }
      // ^^^^^^^  ^ ^^^ ^
      //          0  1  2
      // or this:
      //
      // MyStruct { field_1 , field_2 }
      // ^^^^^^^  ^ ^^^^^^^ ^
      //          0    1    2
      //
      // Edge case:
      //
      // MyStruct { }
      // ^^^^^^^^ ^ ^
      //          0 1
      //
      // Colons are not valid there for blocks.
      t if t.is_keyword(Keyword::OpenBlock) => {
        if tok.peek(1)?.is_keyword(Keyword::CloseBlock) {
          Ok(Expr::Lit(Lit::Struct(StructLit {
            name,
            fields: Group::parse(tok, GroupKind::Block)?,
          })))
        } else {
          let next = tok.peek(2)?;
          if next.is_keyword(Keyword::Colon) || next.is_keyword(Keyword::Comma) {
            Ok(Expr::Lit(Lit::Struct(StructLit {
              name,
              fields: Group::parse(tok, GroupKind::Block)?,
            })))
          } else {
            Ok(Expr::Ident(name))
          }
        }
      }
      _ => Ok(Expr::Ident(name)),
    }
  }
}

impl Op {
  fn prefix_bp(&self) -> Option<u8> {
    Some(match self {
      Op::Not => 13,
      _ => return None,
    })
  }
  fn postfix_bp(&self) -> Option<u8> {
    Some(match self {
      Op::Arr => 11,
      _ => return None,
    })
  }
  fn infix_bp(&self) -> Option<(u8, u8)> {
    Some(match self {
      Op::Or | Op::And => (2, 1),
      Op::Eq | Op::Neq => (4, 3),
      Op::Less | Op::Greater | Op::LTE | Op::GTE => (5, 6),
      Op::Add | Op::Sub => (7, 8),
      Op::Mul | Op::Div | Op::Mod => (9, 10),
      Op::Exp => (12, 11),
      Op::Range => (14, 13),
      Op::Dot => (15, 16),
      Op::Path => (18, 17),
      _ => return None,
    })
  }
}

/*
impl MathOp {
  fn from_keyword(k: &KeywordPos, lhs: Option<Expr>, rhs: Option<Expr>, pos: Span) -> Self {
    let kind = match k.value() {
      Keyword::Add => MathKind::Add,
      Keyword::Sub => MathKind::Sub,
      Keyword::Mul => MathKind::Mul,
      Keyword::Div => MathKind::Div,
      Keyword::Mod => MathKind::Mod,
      Keyword::Exp => MathKind::Exp,
      _ => panic!("unexpected token: {:?}", k),
    };
    Self { kind, lhs: lhs.map(Box::new), rhs: rhs.map(Box::new), pos }
  }
  fn presidence(&self) -> u32 {
    match self.kind {
      MathKind::Add => 1,
      MathKind::Sub => 1,
      MathKind::Mul => 2,
      MathKind::Div => 2,
      MathKind::Mod => 2,
      MathKind::Exp => 3,
      _ => unreachable!("cannot call presidence on a number!"),
    }
  }
}
*/

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{span, syntax::Parse, token::Ident};

  #[test]
  fn str_addition() {
    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("\"hello\" + \" world\"".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Add);
    assert_eq!(lhs, Some(Expr::lit("hello", span!(1..8))));
    assert_eq!(rhs, Some(Expr::lit(" world", span!(11..19))));
  }

  #[test]
  fn simple_math() -> Result<()> {
    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 * 5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Mul);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(5, span!(5..6))));

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 + 5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Add);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(5, span!(5..6))));

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 - 5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Sub);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(5, span!(5..6))));

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 - -5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Sub);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(-5, span!(5..7))));

    // This looks terrible, but is correct (it should parse into "3 - (-5)").
    // Note that -- parses into a decrement, so 3 -- 5 is invalid.
    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 - - 5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Sub);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(-5, span!(5..8))));

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 / 5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Div);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(5, span!(5..6))));

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 / -5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Div);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(-5, span!(5..7))));

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 % 5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Mod);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(5, span!(5..6))));

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("3 ** 5".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Exp);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::lit(5, span!(6..7))));
    Ok(())
  }

  #[test]
  // Skip because of tree comments
  #[rustfmt::skip]
  fn multi_math() -> Result<()> {
    // Produces the tree
    //       -
    //      / \
    //     -   2
    //    / \
    //   3   5
    let (lhs, rhs, kind) = Expr::parse(&mut Tokenizer::new("3 - 5 - 2".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Sub);
    assert_eq!(lhs, Some(Expr::op(Some(Expr::lit(3, span!(1..2))), Some(Expr::lit(5, span!(5..6))), Op::Sub)));
    assert_eq!(rhs, Some(Expr::lit(2, span!(9..10))));

    // Produces the tree
    //       -
    //      / \
    //     *   2
    //    / \
    //   3   5
    let (lhs, rhs, kind) = Expr::parse(&mut Tokenizer::new("3 * 5 - 2".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Sub);
    assert_eq!(lhs, Some(Expr::op(Some(Expr::lit(3, span!(1..2))), Some(Expr::lit(5, span!(5..6))), Op::Mul)));
    assert_eq!(rhs, Some(Expr::lit(2, span!(9..10))));

    // Produces the tree
    //       -
    //      / \
    //     3   *
    //        / \
    //       5   2
    let (lhs, rhs, kind) = Expr::parse(&mut Tokenizer::new("3 - 5 * 2".as_bytes(), 0)).unwrap().unwrap_op();
    assert_eq!(kind, Op::Sub);
    assert_eq!(lhs, Some(Expr::lit(3, span!(1..2))));
    assert_eq!(rhs, Some(Expr::op(Some(Expr::lit(5, span!(5..6))), Some(Expr::lit(2, span!(9..10))), Op::Mul)));

    // Produces the tree
    //       -
    //      / \
    //     -   6
    //    / \
    //   3   *
    //      / \
    //     5   2
    let (lhs, rhs, kind) = Expr::parse(&mut Tokenizer::new("3 - 5 * 2 - 6".as_bytes(), 0)).unwrap().unwrap_op();
    dbg!(&lhs, &rhs, &kind);
    assert_eq!(kind, Op::Sub);
    assert_eq!(lhs, Some(Expr::op(Some(Expr::lit(3, span!(1..2))), Some(Expr::op(Some(Expr::lit(5, span!(5..6))), Some(Expr::lit(2, span!(9..10))), Op::Mul)), Op::Sub)));
    assert_eq!(rhs, Some(Expr::lit(6, span!(13..14))));

    Ok(())
  }

  #[test]
  fn variables() -> Result<()> {
    /*
    let res = math_op!(Expr::parse(&mut Tokenizer::new("i * j".as_bytes(), 0)).unwrap());
    assert_eq!(res.kind(), &MathKind::Mul);
    assert_eq!(
      *res.lhs.unwrap(),
      Expr::Ident(Ident::new("i", Span::new(Pos::new(1, 1, 0), Pos::new(1, 2, 1), 0)).into())
    );
    assert_eq!(
      *res.rhs.unwrap(),
      Expr::Ident(Ident::new("j", Span::new(Pos::new(1, 5, 4), Pos::new(1, 6, 5), 0)).into())
    );
    */
    Ok(())
  }

  #[test]
  fn paths() {
    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("foo::bar::baz".as_bytes(), 0)).unwrap().unwrap_op();
    dbg!(&lhs, &rhs, &kind);
    assert_eq!(kind, Op::Path);
    assert_eq!(lhs, Some(Expr::Ident(Ident::new("foo", span!(1..4)))));
    assert_eq!(
      rhs,
      Some(Expr::op(
        Some(Expr::Ident(Ident::new("bar", span!(6..9)))),
        Some(Expr::Ident(Ident::new("baz", span!(11..14)))),
        Op::Path
      ))
    );

    let (lhs, rhs, kind) =
      Expr::parse(&mut Tokenizer::new("foo::bar().baz".as_bytes(), 0)).unwrap().unwrap_op();
    dbg!(&lhs, &rhs, &kind);
    assert_eq!(kind, Op::Dot);
    assert_eq!(
      lhs,
      Some(Expr::op(
        Some(Expr::Ident(Ident::new("foo", span!(1..4)))),
        Some(Expr::Call(Ident::new("bar", span!(6..9)), Group::empty(span!(9..11)))),
        Op::Path,
      ))
    );
    assert_eq!(rhs, Some(Expr::Ident(Ident::new("baz", span!(12..15)))));
  }
}
