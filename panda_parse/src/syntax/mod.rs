//! This is the syntactic analyzer of Panda. Example:
//! ```
//! use panda_parse::{
//!   tree::{Statement, Expr, Op},
//!   syntax::Parser,
//!   token::{Ident, Keyword, Pos, Span, Token, Tokenizer},
//!   span
//! };
//!
//! // Create a new parser, and parse the text as a statement.
//! let mut parser = Parser::new(Tokenizer::new("i * j".as_bytes(), 0));
//! // If the syntax was invalid at all, this will error out. Things
//! // like adding strings and ints are not checked at this stage.
//! let statements = parser.parse_statement().unwrap();
//!
//! // This will make all the nonsense below easier to visualize:
//! dbg!(&statements);
//!
//! assert_eq!(statements.items().len(), 1);
//!
//! // Get the expression from the statement.
//! let expr = match &statements.items()[0] {
//!   Statement::Expr(expr) => expr,
//!   v => panic!("expecting a statement, got {:?}", v),
//! };
//! // Get the left hand side, right hand side, and op kind from the expression.
//! let (lhs, rhs, op) = match expr {
//!   Expr::Op(lhs, rhs, op) => (lhs, rhs, op),
//!   v => panic!("expecting an op, got {:?}", v),
//! };
//! // Now we validate the math op is correct.
//! assert_eq!(*op, Op::Mul);
//! assert_eq!(**lhs.as_ref().unwrap(), Expr::Ident(Ident::new("i", span!(1..2))));
//! assert_eq!(**rhs.as_ref().unwrap(), Expr::Ident(Ident::new("j", span!(5..6))));
//! ```

use super::{
  token::{Ident, Keyword, ParseError, PathLit, Result, Token, Tokenizer},
  tree::{Expr, FuncDef, Impl, Import, OnEvent, StatementList, Struct, Trait},
};
use crate::Path;
use std::io::Read;

mod closure;
mod event;
mod expr;
mod func;
mod if_stmt;
mod imp;
mod import;
mod loop_stmt;
mod statement;
mod strct;

#[derive(Debug)]
pub struct ParsedFile {
  pub path:      Path,
  pub imports:   Vec<Import>,
  pub funcs:     Vec<FuncDef>,
  pub structs:   Vec<Struct>,
  pub traits:    Vec<Trait>,
  pub impls:     Vec<Impl>,
  pub callbacks: Vec<OnEvent>,
  pub globals:   Vec<Global>,
}

#[derive(Debug, PartialEq)]
pub struct Global {
  pub name: Ident,
  pub init: Option<Expr>,
}

/// This is the parser. It converts a token stream into [`StatementList`]s,
/// [`Expr`]s, and [`FuncDef`]. See the [`module`](crate::parse::syntax) level
/// docs for more.
pub struct Parser<R> {
  tok: Tokenizer<R>,
}

impl<R: Read> Parser<R> {
  pub fn new(tok: Tokenizer<R>) -> Self {
    Parser { tok }
  }
  /// Parses the internal tokenizer as a source file. This will consume the
  /// entire file, and produce an error if there were any remaining tokens. So
  /// `self` is essentially unusable after this is called. This does not take
  /// self by value, as there might be reasons in user code to hold onto
  /// this reference.
  pub fn parse_file(&mut self, path: Path) -> Result<ParsedFile> {
    let mut imports = vec![];
    let mut funcs = vec![];
    let mut structs = vec![];
    let mut traits = vec![];
    let mut impls = vec![];
    let mut callbacks = vec![];
    let mut globals = vec![];
    while let Some(t) = self.tok.peek_opt(0)? {
      match t {
        t if t.is_keyword(Keyword::Struct) => structs.push(Struct::parse(&mut self.tok)?),
        t if t.is_keyword(Keyword::Trait) => traits.push(Trait::parse(&mut self.tok)?),
        t if t.is_keyword(Keyword::Function) => funcs.push(FuncDef::parse(&mut self.tok)?),
        t if t.is_keyword(Keyword::Impl) => impls.push(Impl::parse(&mut self.tok)?),
        t if t.is_keyword(Keyword::Use) => imports.push(Import::parse(&mut self.tok)?),
        t if t.is_keyword(Keyword::On) => callbacks.push(OnEvent::parse(&mut self.tok)?),
        t if t.is_keyword(Keyword::Let) => {
          self.tok.read().unwrap();
          let name = self.tok.read()?.ident("variable")?;
          if self.tok.peek_opt(0)?.map(|t| t.is_keyword(Keyword::Assign)).unwrap_or(false) {
            self.tok.expect(Keyword::Assign)?;
            globals.push(Global { name, init: Some(Expr::parse(&mut self.tok)?) })
          } else {
            globals.push(Global { name, init: None })
          }
        }
        _ => return Err(ParseError::unexpected(t.clone(), "a function or struct")),
      }
    }
    Ok(ParsedFile { path, imports, funcs, structs, traits, impls, callbacks, globals })
  }
  /// Parses a statement list from the tokenizer. This will read as much as
  /// possible from the inner tokenizer, making the parser unusable after this
  /// is called. This does not take self by value, as there might be reasons
  /// in user code to hold onto this reference.
  pub fn parse_statement(&mut self) -> Result<StatementList> {
    StatementList::parse(&mut self.tok)
  }
  /// Parses a single expression from the tokenizer. Since all expressions are
  /// also statements, there isn't much reason to call this function. It will
  /// only consume the tokens for one expression, so you can call this until
  /// you get an EOF error.
  pub fn parse_expr(&mut self) -> Result<Expr> {
    Expr::parse(&mut self.tok)
  }
  /// Returns a token if there are any remaining. This is what should be used to
  /// check if something like [`parse_expr`](Self::parse_expr) has parsed
  /// everything.
  pub fn peek_opt(&mut self) -> Result<Option<&Token>> {
    self.tok.peek_opt(0)
  }
}

/// The parse trait. This is defined for all AST elements that can be parsed
/// from a token stream. Example:
/// ```
/// use panda_parse::{tree::Expr, syntax::Parse, token::Tokenizer};
///
/// // Use the parse function on Expr.
/// let expr = Expr::parse(&mut Tokenizer::new("i * j".as_bytes(), 0));
/// dbg!(expr);
/// ```
pub trait Parse {
  type Output;

  /// Parses an AST node from the tokenizer. Most of the time, the first token
  /// (say, a `fn`) will be peeked before this is called. So this function
  /// usually starts with [`tok.expect`](Tokenizer::expect). However, in some
  /// expression and statement parsing, it is faster to call
  /// [`tok.read`](Tokenizer::read) while getting the first token. So some AST
  /// nodes have a different `parse` function, which will take arguments like
  /// what the identifer was that was just read.
  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self::Output>;
}

impl PathLit {
  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<PathLit> {
    let first = tok.read()?.ident("a path")?;
    PathLit::parse_tail(tok, first)
  }

  fn parse_tail<R: Read>(tok: &mut Tokenizer<R>, first: Ident) -> Result<PathLit> {
    let mut segments = vec![first];
    loop {
      if tok.peek(0)?.is_keyword(Keyword::PathSep) {
        tok.read().unwrap();
        segments.push(tok.read()?.ident("a path segment")?);
      } else {
        break;
      }
    }
    Ok(segments.into())
  }
}

#[cfg(test)]
use crate::{err::PdError, path};

#[cfg(test)]
#[track_caller]
fn assert_valid(src: &str) {
  let mut parser = Parser::new(Tokenizer::new(src.as_bytes(), 0));
  if let Err(e) = parser.parse_file(path!(test)) {
    e.print(src, std::path::Path::new("test.pand"), true);
    panic!("parsed invalid text");
  }
}

#[cfg(test)]
#[track_caller]
fn assert_invalid(src: &str) {
  let mut parser = Parser::new(Tokenizer::new(src.as_bytes(), 0));
  if parser.parse_file(path!(test)).is_ok() {
    panic!("parsed valid text");
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{span, token::IntLit, tree::Op};

  #[test]
  fn file() {
    let text = r#"
    let foo = 3
    let bar = foo + 2
    "#;
    let mut p = Parser::new(Tokenizer::new(text.as_bytes(), 0));
    let f = p.parse_file(path!(abc)).unwrap();
    assert_eq!(
      f.globals,
      vec![
        Global {
          name: Ident::new("foo", span!(10..13)),
          init: Some(Expr::Lit(IntLit { pos: span!(16..17), val: 3 }.into())),
        },
        Global {
          name: Ident::new("bar", span!(26..29)),
          init: Some(Expr::op(
            Some(Expr::Ident(Ident::new("foo", span!(32..35)))),
            Some(Expr::Lit(IntLit { pos: span!(38..39), val: 2 }.into())),
            Op::Add,
          ),),
        }
      ]
    );
  }
}
