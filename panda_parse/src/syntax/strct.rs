use crate::{
  syntax::Parse,
  token::{Keyword, ParseError, PathLit, Result, Token, Tokenizer},
  tree::{Struct, Trait, TraitFuncDef},
  VarType,
};
use std::{
  collections::{hash_map::Entry, HashMap},
  io::Read,
};

impl Parse for Struct {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    tok.expect(Keyword::Struct)?;
    let name = tok.read()?.ident("struct name")?;
    tok.expect(Keyword::OpenBlock)?;

    let mut fields = HashMap::new();
    loop {
      if tok.peek(0)?.is_keyword(Keyword::CloseBlock) {
        break;
      }
      let name = tok.read()?.ident("field name")?;
      tok.expect(Keyword::Colon)?;
      let ty = VarType::parse(tok)?;
      match fields.entry(name) {
        Entry::Vacant(e) => {
          e.insert(ty);
        }
        Entry::Occupied(e) => {
          return Err(ParseError::redeclaration("struct field", e.key().clone()));
        }
      }

      let t = tok.peek(0)?;
      if t.is_keyword(Keyword::Comma) {
        tok.expect(Keyword::Comma).unwrap();
      } else if !t.is_keyword(Keyword::CloseBlock) {
        return Err(ParseError::unexpected(t.clone(), "a comma or closing bracket"));
      }
    }

    tok.expect(Keyword::CloseBlock)?;
    Ok(Struct {
      name,
      order: fields.keys().cloned().collect(),
      fields: fields.into_iter().map(|(k, v)| (k.into(), v)).collect(),
    })
  }
}

impl Parse for VarType {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    match tok.read()? {
      t if t.is_keyword(Keyword::Str) => Ok(VarType::String),
      t if t.is_keyword(Keyword::Int) => Ok(VarType::Int),
      t if t.is_keyword(Keyword::Float) => Ok(VarType::Float),
      t if t.is_keyword(Keyword::Bool) => Ok(VarType::Bool),
      // t if t.is_keyword(Keyword::Arr) => Ok(VarType::Array),
      t if t.is_keyword(Keyword::Map) => Ok(VarType::Map),
      t if t.is_keyword(Keyword::Any) => Ok(VarType::None),
      Token::Ident(first) => Ok(VarType::Struct(PathLit::parse_tail(tok, first)?.into_path())),
      t => Err(ParseError::unexpected(t, "a field type")),
    }
  }
}

impl Parse for Trait {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    tok.expect(Keyword::Trait)?;
    let name = tok.read()?.ident("struct name")?;
    tok.expect(Keyword::OpenBlock)?;

    let mut funcs = HashMap::new();
    loop {
      if tok.peek(0)?.is_keyword(Keyword::CloseBlock) {
        break;
      }
      let func = TraitFuncDef::parse(tok)?;
      match funcs.entry(func.name.clone()) {
        Entry::Vacant(e) => {
          e.insert(func);
        }
        Entry::Occupied(e) => {
          return Err(ParseError::redeclaration("struct field", e.key().clone()));
        }
      }
    }

    tok.expect(Keyword::CloseBlock)?;
    Ok(Trait {
      name,
      order: funcs.keys().cloned().collect(),
      funcs: funcs.into_iter().map(|(k, v)| (k.into(), v)).collect(),
    })
  }
}

#[cfg(test)]
mod tests {
  use super::super::{assert_invalid, assert_valid};

  #[test]
  fn test_fields() {
    assert_valid("struct Hello {}");
    assert_invalid("struct Hello {a}");
    assert_invalid("struct Hello {a:}");
    assert_valid("struct Hello {a: int}");
    assert_valid("struct Hello {a: int, b: str}");
    assert_valid("struct Hello {a: foo}");
    assert_valid("struct Hello {a: foo::bar}");
  }

  #[test]
  fn test_traits() {
    assert_valid("trait Hello {}");
    assert_invalid("trait Hello { fn }");
    assert_invalid("trait Hello { fn foo }");
    assert_valid("trait Hello { fn foo() }");
    assert_valid("trait Hello { fn foo() fn bar() }");
    assert_valid("trait Hello { fn foo() {} fn bar() }");
  }
}
