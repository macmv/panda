use crate::{
  syntax::Parse,
  token::{Ident, Keyword, ParseError, PathLit, Result, Span, Tokenizer},
  tree::{Args, Expr, FuncCall, FuncDef, StatementList, TraitFuncDef},
  VarType,
};
use std::io::Read;

impl Parse for FuncDef {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    tok.expect(Keyword::Function)?;
    let name = tok.read()?.ident("a function name")?;

    tok.expect(Keyword::OpenParen)?;
    let args = Args::parse(tok)?;
    tok.expect(Keyword::CloseParen)?;

    tok.expect(Keyword::OpenBlock)?;
    let body = StatementList::parse(tok)?;
    tok.expect(Keyword::CloseBlock)?;

    Ok(FuncDef { name, args, body })
  }
}

impl Parse for TraitFuncDef {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    tok.expect(Keyword::Function)?;
    let name = tok.read()?.ident("a function name")?;

    tok.expect(Keyword::OpenParen)?;
    let args = Args::parse(tok)?;
    tok.expect(Keyword::CloseParen)?;

    let t = tok.peek(0)?;
    let body = if t.is_keyword(Keyword::OpenBlock) {
      tok.expect(Keyword::OpenBlock)?;
      let body = StatementList::parse(tok)?;
      tok.expect(Keyword::CloseBlock)?;
      Some(body)
    } else {
      None
    };

    Ok(TraitFuncDef { name, args, body })
  }
}

impl Parse for Args {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    let t = tok.peek(0)?;
    if t.is_keyword(Keyword::CloseParen) {
      return Ok(Args { pos: t.pos(), names: vec![], variadic: None });
    }

    // TODO: Variadics

    let mut names = vec![];
    let variadic = None;

    let start = t.pos().start();
    names.push(Args::parse_arg(tok)?);

    loop {
      let t = tok.peek(0)?;
      if t.is_keyword(Keyword::CloseParen) {
        return Ok(Args { pos: Span::new(start, t.pos().start(), tok.file()), names, variadic });
      } else if t.is_keyword(Keyword::Comma) {
        tok.read().unwrap();
      } else {
        let t = tok.read().unwrap();
        return Err(ParseError::unexpected(t, ", or )"));
      }
      names.push(Args::parse_arg(tok)?);
    }
  }
}

impl Args {
  fn parse_arg<R: Read>(tok: &mut Tokenizer<R>) -> Result<(Ident, Option<VarType>)> {
    let name = tok.read()?.ident("an argument name")?;
    let path = if tok.peek(0)?.is_keyword(Keyword::Colon) {
      tok.expect(Keyword::Colon).unwrap();
      Some(VarType::parse(tok)?)
    } else {
      None
    };

    Ok((name, path))
  }
}

impl FuncCall {
  pub fn parse<R: Read>(tok: &mut Tokenizer<R>, slf: Option<Expr>, name: PathLit) -> Result<Self> {
    tok.expect(Keyword::OpenParen)?;
    let args = read_expr_args(tok)?;
    tok.expect(Keyword::CloseParen)?;

    Ok(FuncCall { name, args, slf: slf.map(Box::new) })
  }
}

fn read_expr_args<R: Read>(tok: &mut Tokenizer<R>) -> Result<Vec<Expr>> {
  if tok.peek(0)?.is_keyword(Keyword::CloseParen) {
    return Ok(vec![]);
  }
  let mut args = vec![];
  args.push(Expr::parse(tok)?);
  loop {
    let t = tok.peek(0)?;
    if t.is_keyword(Keyword::CloseParen) {
      return Ok(args);
    } else if t.is_keyword(Keyword::Comma) {
      tok.read().unwrap();
    } else {
      let t = tok.read().unwrap();
      return Err(ParseError::unexpected(t, ", or )"));
    }
    args.push(Expr::parse(tok)?);
  }
}

#[cfg(test)]
mod tests {
  use super::super::{assert_invalid, assert_valid};

  #[test]
  fn test_arg_types() {
    assert_valid("fn hello() {}");
    assert_invalid("fn hello(,) {}");
    assert_valid("fn hello(a) {}");
    assert_invalid("fn hello(a,) {}");
    assert_valid("fn hello(a, b) {}");
    assert_valid("fn hello(a: int, b) {}");
    assert_valid("fn hello(a: foo, b: bar) {}");
    assert_valid("fn hello(a: foo::bar, b: any) {}");
  }
}
