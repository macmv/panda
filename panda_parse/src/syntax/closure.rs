use super::Parse;
use crate::{
  token::{Ident, Keyword, ParseError, Result, Span, Tokenizer},
  tree::{Closure, ClosureArgs, Expr},
};
use std::io::Read;

impl Parse for Closure {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    Ok(Closure { args: ClosureArgs::parse(tok)?, body: Box::new(Expr::parse(tok)?) })
  }
}
impl Parse for ClosureArgs {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    let start = tok.pos();
    let names = match tok.read()? {
      // Special case for no arguments
      t if t.is_keyword(Keyword::Or) => vec![],
      t if t.is_keyword(Keyword::BitOr) => parse_args(tok)?,
      t => return Err(ParseError::unexpected(t, "a closure")),
    };
    Ok(ClosureArgs { pos: Span::new(start, tok.pos(), tok.file()), names })
  }
}

fn parse_args<R: Read>(tok: &mut Tokenizer<R>) -> Result<Vec<Ident>> {
  let mut args = vec![];
  match tok.read()? {
    t if t.is_ident() => args.push(t.ident("").unwrap()),
    t if t.is_keyword(Keyword::BitOr) => return Ok(args),
    t => return Err(ParseError::unexpected(t, "a closure argument")),
  }
  loop {
    match tok.read()? {
      t if t.is_keyword(Keyword::Comma) => args.push(tok.read()?.ident("an argument")?),
      t if t.is_keyword(Keyword::BitOr) => break,
      t => return Err(ParseError::unexpected(t, "a closure argument")),
    }
  }
  Ok(args)
}
