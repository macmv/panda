use crate::{
  syntax::Parse,
  token::{Keyword, Result, Tokenizer},
  tree::{Args, Expr, OnEvent, StatementList},
};
use std::io::Read;

impl Parse for OnEvent {
  type Output = Self;

  fn parse<R: Read>(tok: &mut Tokenizer<R>) -> Result<Self> {
    tok.expect(Keyword::On)?;
    let name = tok.read()?.ident("a function name")?;

    tok.expect(Keyword::OpenParen)?;
    let args = Args::parse(tok)?;
    tok.expect(Keyword::CloseParen)?;

    let when = if tok.peek(0)?.is_keyword(Keyword::When) {
      tok.expect(Keyword::When)?;
      Some(Expr::parse(tok)?)
    } else {
      None
    };

    tok.expect(Keyword::OpenBlock)?;
    let body = StatementList::parse(tok)?;
    tok.expect(Keyword::CloseBlock)?;

    Ok(OnEvent { name, args, when, body })
  }
}
