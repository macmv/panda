use super::{Args, Expr, StatementList};
use crate::token::Ident;

#[derive(Debug, Clone)]
pub struct OnEvent {
  /// The name of the function.
  pub name: Ident,
  /// Arguments for this callback.
  pub args: Args,
  /// When clause
  pub when: Option<Expr>,
  /// The actual body to execute when the function is called.
  pub body: StatementList,
}
