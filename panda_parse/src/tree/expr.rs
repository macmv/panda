use super::{Closure, StatementList, StructLit};
use crate::token::{FloatLit, Group, Ident, IntLit, Keyword, Predefined, Span, StringLit, Token};

#[derive(Debug, Clone, PartialEq)]
pub enum Lit {
  Int(IntLit),
  Float(FloatLit),
  String(StringLit),
  Bool(BoolLit),
  Arr(Group<Expr>),
  Map(Group<MapItem>),
  Struct(StructLit),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BoolLit {
  pub pos: Span,
  pub val: bool,
}
#[derive(Debug, Clone, PartialEq)]
pub struct MapItem {
  pub key: Expr,
  pub val: Expr,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
  Lit(Lit),
  Paren(Box<Expr>),
  Op(Option<Box<Expr>>, Option<Box<Expr>>, Op),
  Call(Ident, Group<Expr>),
  Block(Span, StatementList),

  // An identifier. At this point, we don't know if its a variable, a method reference, or a field
  // reference.
  Ident(Ident),

  Predefined(Predefined),
  Closure(Closure),
}

macro_rules! op {
  { $( $name:ident $(: $kw:ident)?, )* } => {
    macro_rules! last {
      ( $a:ident, ) => {
        Keyword::$a
      };
      ( $a:ident, $b:ident, ) => {
        Keyword::$b
      }
    }
    #[derive(Debug, Clone, PartialEq, Eq)]
    pub enum Op {
      $($name,)*
    }

    impl Op {
      pub fn from_tok(t: &Token) -> Option<Self> {
        match t {
          Token::Keyword(k) => match k.value() {
            $(last!($name, $($kw,)?) => Some(Op::$name),)*
            _ => None,
          },
          _ => None,
        }
      }
    }
  }
}

op! {
  // +=
  Add,
  // -
  Sub,
  // *
  Mul,
  // /
  Div,

  // %
  Mod,
  // **
  Exp,

  // &
  BitAnd,
  // |
  BitOr,
  // ^
  BitXor,

  // <<
  Shl,
  // >>
  Shr,
  // >>>
  Shrz,

  // ||
  Or,
  // &&
  And,
  // !
  Not,

  // ==
  Eq,
  // !=
  Neq,
  // <
  Less,
  // >
  Greater,
  // <=
  LTE,
  // >=
  GTE,

  // [ ]
  Arr: OpenArr,
  // .
  Dot,
  // ::
  Path: PathSep,
  // ..
  Range,
}

impl Lit {
  pub fn pos(&self) -> Span {
    match self {
      Lit::Int(v) => v.pos,
      Lit::Float(v) => v.pos,
      Lit::String(v) => v.pos,
      Lit::Bool(v) => v.pos,
      Lit::Arr(v) => v.pos(),
      // Lit::Map(v) => v.pos,
      // Lit::Struct(v) => v.pos,
      _ => Span::default(),
    }
  }
}

impl Expr {
  pub fn pos(&self) -> Span {
    match self {
      Self::Lit(l) => l.pos(),
      Self::Paren(e) => e.pos(),
      Self::Op(lhs, rhs, _) => match (lhs, rhs) {
        (Some(lhs), Some(rhs)) => lhs.pos().join(rhs.pos()),
        (Some(lhs), None) => lhs.pos(),
        (None, Some(rhs)) => rhs.pos(),
        (None, None) => unreachable!(),
      },
      Self::Call(i, args) => i.pos().join(args.pos()),
      Self::Block(pos, _) => *pos,
      Self::Ident(i) => i.pos(),
      Self::Predefined(p) => p.pos,
      Self::Closure(c) => c.args.pos,
    }
  }

  pub fn lit(v: impl IntoLit, pos: Span) -> Self {
    Expr::Lit(v.into(pos))
  }
  pub fn op(lhs: Option<impl Into<Expr>>, rhs: Option<impl Into<Expr>>, kind: Op) -> Self {
    Expr::Op(lhs.map(|v| Box::new(v.into())), rhs.map(|v| Box::new(v.into())), kind)
  }

  pub fn unwrap_op(self) -> (Option<Expr>, Option<Expr>, Op) {
    match self {
      Expr::Op(lhs, rhs, kind) => (lhs.map(|v| *v), rhs.map(|v| *v), kind),
      _ => panic!("not an op: {:?}", self),
    }
  }
  pub fn unwrap_ident(self) -> Ident {
    match self {
      Expr::Ident(v) => v,
      _ => panic!("not an ident: {:?}", self),
    }
  }
}

pub trait IntoLit {
  fn into(self, pos: Span) -> Lit;
}

impl IntoLit for i64 {
  fn into(self, pos: Span) -> Lit {
    IntLit { pos, val: self }.into()
  }
}
impl IntoLit for f64 {
  fn into(self, pos: Span) -> Lit {
    FloatLit { pos, val: self }.into()
  }
}
impl<'a> IntoLit for &'a str {
  fn into(self, pos: Span) -> Lit {
    StringLit { pos, val: Into::into(self) }.into()
  }
}

impl From<IntLit> for Lit {
  fn from(v: IntLit) -> Lit {
    Lit::Int(v)
  }
}
impl From<FloatLit> for Lit {
  fn from(v: FloatLit) -> Lit {
    Lit::Float(v)
  }
}
impl From<StringLit> for Lit {
  fn from(v: StringLit) -> Lit {
    Lit::String(v)
  }
}
