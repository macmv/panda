use super::Expr;
use crate::token::Ident;

/// Any assignment operation. Includes variations such as `+=` and `--`
#[derive(Debug, Clone, PartialEq)]
pub struct AssignOp {
  pub lhs: LHS,
  pub op:  AssignOpKind,
}

/// The left hand side of an assignment.
///
/// Examples:
///
/// ```ignore
/// // VALID
/// a = 3
/// a.b = 3
/// a[2] = 3
/// a[2][3] = 3
/// a[2].c = 3
/// bar().baz = 3
/// foo().bar().baz = 3
///
/// // INVALID
/// bar() = 4
/// bar().baz() = 4
/// ```
#[derive(Debug, Clone, PartialEq)]
pub struct LHS {
  pub prefix: Option<Expr>,
  pub last:   LHSKind,
}

/// The final element of an assignment.
#[derive(Debug, Clone, PartialEq)]
pub enum LHSKind {
  Name(Ident),
  // Only valid with a prefix
  Index(Expr),
}

#[derive(Debug, Clone, PartialEq)]
pub enum AssignOpKind {
  Set(Expr),
  Inc,
  Dec,
  Add(Expr),
  Sub(Expr),
  Mul(Expr),
  Div(Expr),
  Mod(Expr),
  Exp(Expr),
}

impl LHS {
  pub fn unwrap_ident(&self) -> Ident {
    match &self.last {
      LHSKind::Name(v) => v.clone(),
      _ => unreachable!(),
    }
  }
}
