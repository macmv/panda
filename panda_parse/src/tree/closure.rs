use super::Expr;
use crate::token::{Ident, Span};

#[derive(Debug, Clone, PartialEq)]
pub struct Closure {
  pub args: ClosureArgs,
  pub body: Box<Expr>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ClosureArgs {
  pub pos:   Span,
  pub names: Vec<Ident>,
}
