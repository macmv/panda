use super::FuncDef;
use crate::token::PathLit;

#[derive(Debug, Clone)]
pub struct Impl {
  // The type this impl block is for
  pub struct_path: PathLit,
  // If this is an `impl Trait for Struct` statement, this will be Some(Trait)
  pub trait_path:  Option<PathLit>,
  // The functions inside the impl. The `slf` type for these functions
  // is the same as the above type.
  pub funcs:       Vec<FuncDef>,
}
