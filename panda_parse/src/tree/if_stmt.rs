use super::{Expr, StatementList};

#[derive(Debug, Clone, PartialEq)]
pub struct If {
  pub cond:       Expr,
  pub block:      StatementList,
  pub else_if:    Vec<(Expr, StatementList)>,
  pub else_block: Option<StatementList>,
}
