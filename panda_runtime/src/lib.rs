mod builtin;
mod file;
mod scope;
pub mod tree;
mod ty;
mod var;

#[macro_use]
pub extern crate panda_parse;

pub use panda_docs as docs;
use panda_docs::{markdown, MarkdownSection};
pub use panda_parse as parse;

pub use file::SlFile;
pub use ty::PandaType;
pub use var::{
  convert::{Callback, TryFromSpan, TryFromSpanMut, TryFromSpanRef, TryIntoSpan},
  DynHasher, MapKeyBuiltin, MapKeyBuiltinImpl, MapKeyStruct, MapKeyStructImpl, MapKeyVar, Struct,
  StructImpl, StructSend, StructSendImpl, Var, VarID, VarSend,
};

use instant::Instant;
use panda_parse::{
  syntax::ParsedFile,
  token::{Ident, ParseError, PathLit, Span},
  Path, VarType,
};
use std::{
  any::Any,
  cell::RefCell,
  collections::{HashMap, HashSet},
  error::Error,
  fmt,
  io::Write,
  ops::Deref,
  path::Path as StdPath,
  rc::Rc,
  sync::{Arc, Mutex, MutexGuard},
  time::Duration,
};
use tree::{Expr as RtExpr, FuncDef, OnEvent};

pub trait WriteAny: Write {
  fn as_any(&self) -> &dyn Any;
}

impl<T: 'static> WriteAny for T
where
  T: Write,
{
  fn as_any(&self) -> &dyn Any {
    self
  }
}

#[derive(Debug)]
pub struct CallbackImpl {
  pub docs:  MarkdownSection,
  pub args:  Vec<VarType>,
  pub impls: Vec<OnEvent>,
}

/// The runtime environment for a program. This will store all global
/// variables, and declared types/functions.
pub struct RtEnv {
  names: HashMap<Path, Item>,

  // A list of files that have been parsed, but not compiled. This is so that we can parse a bunch
  // of files (which can depend on each other), and then resolve all of their names at the same
  // time.
  parsed_files: Vec<ParsedFile>,
  globals:      HashMap<String, Mutex<VarSend>>,
  predefined:   HashMap<String, VarGetter>,

  callback_impls: HashMap<String, CallbackImpl>,

  pub time_limit: Option<Duration>,

  /// Stdout for this environment. Any print calls will go here.
  pub out: Arc<Mutex<dyn WriteAny + Send + Sync>>,
  /// Stderr for this environment. Any errors or eprint calls will go here.
  pub err: Arc<Mutex<dyn WriteAny + Send + Sync>>,
}

impl fmt::Debug for RtEnv {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    f.debug_struct("RtEnv")
      .field("names", &self.names)
      .field("parsed_files", &self.parsed_files)
      .field("time_limit", &self.time_limit)
      .finish()
  }
}

#[derive(Debug)]
pub enum VarRef<'a> {
  Ref(&'a Var),
  Guard(MutexGuard<'a, VarSend>),
}
impl Deref for VarRef<'_> {
  type Target = Var;

  fn deref(&self) -> &Self::Target {
    match self {
      Self::Ref(r) => r,
      Self::Guard(_g) => unimplemented!(),
    }
  }
}
impl VarRef<'_> {
  pub fn into_var(self) -> Var {
    match self {
      Self::Ref(r) => r.clone(),
      Self::Guard(g) => Var::from(g.clone()),
    }
  }
}

pub enum Item {
  Func(FuncItem),
  Struct(StructItem),
  Trait(TraitItem),
  /// Used as a placeholder, so that imports can resolve correctly.
  Module,
}

#[derive(Clone)]
pub struct FuncItem {
  imp: Arc<dyn FuncCallable + Send + Sync>,
}

pub struct StructItem {
  imp:            StructCallable,
  pub trait_imps: Vec<TraitImpl>,
}

pub struct TraitItem {
  funcs: HashMap<String, TraitFunc>,
}

/// A FuncItem within a trait. This is the same as a function item, but the body
/// may not be defined.
pub struct TraitFunc {
  has_body: bool,
  item:     FuncItem,
}

pub struct TraitImpl {
  pub path:  Path,
  pub funcs: HashMap<String, FuncItem>,
}

impl FuncItem {
  /// Returns the docs for this function. This is the syn::Attribute value,
  /// directly from the proc macro.
  ///
  /// Returns an empty array be default.
  pub fn docs(&self) -> MarkdownSection {
    self.imp.docs()
  }
  pub fn arg_docs(&self) -> Vec<(&'static str, Option<VarType>)> {
    self.imp.arg_docs()
  }
}

impl StructItem {
  /// Returns the docs for this function. This is the syn::Attribute value,
  /// directly from the proc macro.
  ///
  /// Returns an empty array be default.
  pub fn docs(&self) -> &'static [&'static str] {
    self.imp.docs()
  }
  /// Returns all of the fields on this struct, for documentation.
  pub fn fields(&self) -> Vec<(String, MarkdownSection, VarType)> {
    self.imp.fields()
  }
}

/// Anything that is callable. This is usually going to be a `FuncDef` or a
/// `BuiltinFunc`.
pub trait FuncCallable: std::fmt::Debug {
  /// Returns the name of the function.
  fn name(&self) -> String;
  /// Checks if the given arguments are valid.
  fn check_args(&self, pos: Span, args: &[RtExpr]) -> std::result::Result<(), ParseError>;
  /// Checks if the given arguments are valid, at runtime. Used when a Rust
  /// funciton calls a Panda function.
  fn check_args_rt(
    &self,
    env: &mut LockedEnv,
    pos: Span,
    called_on_self: bool,
    args: &[Var],
  ) -> Result<()>;
  /// Executes the function. This can always modify self.
  fn exec(&self, env: &mut LockedEnv, slf: Option<Var>, args: Vec<Var>, pos: Span) -> Result<Var>;
  /// If `self` is a FuncDef, returns self. Otherwise, returns None.
  fn as_func_def(&self) -> Option<&FuncDef> {
    None
  }
  /// Returns the docs for this function. This is the syn::Attribute value,
  /// directly from the proc macro.
  ///
  /// Returns an empty array be default.
  fn docs(&self) -> MarkdownSection {
    markdown!()
  }
  fn arg_docs(&self) -> Vec<(&'static str, Option<VarType>)> {
    vec![]
  }
}

/// Any struct that is accessible. This is only for accessing fields. This will
/// usually be a `Struct` or a `BuiltinStruct`.
#[derive(Clone)]
pub enum StructCallable {
  Builtin(Arc<BuiltinStruct>),
  Native(tree::Struct),
}

impl StructCallable {
  pub fn has_field(&self, _env: &mut LockedEnv, _slf: &Var, _name: &str) -> bool {
    match self {
      StructCallable::Native(_) => todo!(),
      // We check this later
      StructCallable::Builtin(_) => true,
    }
  }
  pub fn field(&self, _env: &mut LockedEnv, _slf: &Var, _name: &str) -> Result<Var> {
    match self {
      StructCallable::Native(_) => todo!(),
      StructCallable::Builtin(_) => todo!(),
    }
  }
  pub fn fields(&self) -> Vec<(String, MarkdownSection, VarType)> {
    match self {
      StructCallable::Native(_) => vec![],
      StructCallable::Builtin(b) => b.fields.clone(),
    }
  }
  pub fn docs(&self) -> &'static [&'static str] {
    match self {
      StructCallable::Native(_) => &[],
      StructCallable::Builtin(b) => b.docs,
    }
  }
  pub fn validate_fields(
    &self,
    pos: Span,
    fields: &[(Ident, RtExpr)],
  ) -> std::result::Result<(), ParseError> {
    match self {
      StructCallable::Builtin(_) => todo!(),
      StructCallable::Native(s) => {
        let mut err = ParseError::Empty;
        let mut missing_fields: HashSet<String> = s.fields.keys().cloned().collect();
        for (name, _) in fields {
          if !s.fields.contains_key(name.as_ref()) {
            err.join(ParseError::undefined("field", name.clone()));
          }
          missing_fields.remove(name.as_ref());
        }
        err.into_result(())?;
        if missing_fields.is_empty() {
          Ok(())
        } else {
          let mut items: Vec<_> = missing_fields.into_iter().collect();
          items.sort_unstable();
          Err(ParseError::custom(
            format!(
              "missing field{} {}",
              if items.len() == 1 { "" } else { "s" },
              items.join(", ")
            ),
            pos,
          ))
        }
      }
    }
  }
}

/// An impl block that has a `defie_ty` macro. The struct this impl block is
/// implemeting will implement the `BuiltinImpl` trait. For example:
///
/// ```
/// // This must implement Debug and Clone
/// #[derive(Debug, Clone)]
/// pub struct MyStruct {
///   foo: i32,
/// }
///
/// #[panda_derive::define_ty(crate = "panda_runtime")]
/// impl MyStruct {
///   pub fn foo(&self) {}
/// }
/// ```
///
/// In this example, the type `MyStruct` would implement `BuiltinImpl`. The
/// implementation would include a `BuiltinFunc` for every function defined in
/// that impl block.
pub trait BuiltinImpl: fmt::Debug {
  /// Returns the full path of this struct. The path of each function will be
  /// `path()::func_name`.
  fn path() -> Path
  where
    Self: Sized;
  /// Returns the functions defined in this impl block.
  fn funcs() -> Vec<Arc<dyn FuncCallable + Send + Sync>>
  where
    Self: Sized;
  /// Returns the builtin struct for this type. This lists all the fields
  /// accessible to panda. For now, this is always empty.
  fn strct() -> BuiltinStruct
  where
    Self: Sized;
  /// Returns the field, if present.
  fn field(&self, name: &Ident) -> Option<Var>;
  /// Clones this object. This cannot be from `Clone`, as `Clone` requires that
  /// `Self` is `Sized`.
  fn box_clone(&self) -> Box<dyn BuiltinImpl + Send + Sync>;
  fn rc_clone(&self) -> Rc<RefCell<dyn BuiltinImpl + Send + Sync>>;
  /// Returns self as `Any`. This is used to type cast a `BuiltinImpl` into a
  /// concrete type.
  fn as_any(&self) -> &dyn Any;
  /// Returns self as `Any`. This is used to type cast a `BuiltinImpl` into a
  /// concrete type.
  fn as_any_mut(&mut self) -> &mut dyn Any;
  /// If this can be converted into a map key, this will return the map key. If
  /// not, this will return `None`.
  fn as_map_key(&self) -> Option<MapKeyBuiltin>;
}

pub struct VarGetter {
  pub docs: MarkdownSection,
  pub func: Box<dyn Fn() -> Var + Send + Sync>,
}

impl fmt::Debug for VarGetter {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    f.debug_struct("VarGetter").finish()
  }
}

impl VarGetter {
  pub fn get(&self) -> Var {
    (self.func)()
  }
}

#[derive(Debug)]
pub struct LockedEnv<'a> {
  // Variables local to this env. Each variable is assigned a unique
  // id by the second parsing pass. The id is the index into this array.
  locals: Vec<Var>,

  names:              &'a HashMap<Path, Item>,
  globals:            &'a HashMap<String, Mutex<VarSend>>,
  pub predefined:     &'a HashMap<String, VarGetter>,
  pub callback_impls: &'a HashMap<String, CallbackImpl>,

  time_limit:   Option<Duration>,
  time_started: Instant,

  files: &'a [SlFile],
}

/// Stores a variable, by value or by mutable reference.
#[derive(Debug)]
pub enum VarOrMut<'a> {
  Val(Var),
  Mut(&'a mut Var),
}

impl From<Var> for VarOrMut<'_> {
  fn from(v: Var) -> Self {
    Self::Val(v)
  }
}
impl<'a> From<&'a mut Var> for VarOrMut<'a> {
  fn from(v: &'a mut Var) -> Self {
    Self::Mut(v)
  }
}

impl<'a> VarOrMut<'a> {
  pub fn unwrap_val(self) -> Var {
    match self {
      Self::Val(v) => v,
      v => panic!("not a value: {v:?}"),
    }
  }
}

pub struct BuiltinFunc {
  // Any doc comments on the builtin func. This is still present even if the `docs` feature is
  // disabled, so that the proc macro can generate the same code.
  pub docs:            MarkdownSection,
  pub name:            String,
  pub member:          bool,
  pub args:            Vec<(&'static str, Option<VarType>)>,
  pub args_must_match: bool,
  pub ret:             VarType,
  pub exec:            BuiltinFn,
}

pub type BuiltinFn = Arc<dyn Fn(&mut LockedEnv, Var, Vec<Var>, Span) -> Result<Var> + Send + Sync>;

impl fmt::Debug for BuiltinFunc {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    f.debug_struct("BuiltinFunc").field("name", &self.name).finish()
  }
}

pub struct BuiltinStruct {
  // Any doc comments on the builtin struct impl
  #[cfg(feature = "docs")]
  pub docs:           &'static [&'static str],
  pub name:           String,
  pub fields:         Vec<(String, MarkdownSection, VarType)>,
  pub dynamic_fields: bool,
}

impl fmt::Debug for Item {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Item::Func(_) => write!(f, "Func()"),
      Item::Struct(_) => write!(f, "Struct()"),
      Item::Trait(_) => write!(f, "Trait()"),
      Item::Module => write!(f, "Module"),
    }
  }
}

impl FuncCallable for BuiltinFunc {
  fn name(&self) -> String {
    self.name.clone()
  }
  fn check_args(&self, pos: Span, args: &[RtExpr]) -> std::result::Result<(), ParseError> {
    // TODO: Parse time type checking for arguments here
    if !self.args_must_match {
      return Ok(());
    }
    if self.args.len() != args.len() {
      Err(ParseError::custom(
        format!("expected {} arguments, got {}", self.args.len(), args.len()),
        pos,
      ))
    } else {
      Ok(())
    }
  }
  fn check_args_rt(
    &self,
    _env: &mut LockedEnv,
    pos: Span,
    _called_on_self: bool,
    args: &[Var],
  ) -> Result<()> {
    // TODO: Runtime type checking for arguments here
    if !self.args_must_match {
      return Ok(());
    }
    if self.args.len() != args.len() {
      Err(RuntimeError::custom(
        format!(
          "expected {} argument{}, got {} argument{}",
          self.args.len(),
          if self.args.len() == 1 { "" } else { "s" },
          args.len(),
          if args.len() == 1 { "" } else { "s" },
        ),
        pos,
      ))
    } else {
      Ok(())
    }
  }
  fn exec(
    &self,
    env: &mut LockedEnv,
    mut slf: Option<Var>,
    mut args: Vec<Var>,
    pos: Span,
  ) -> Result<Var> {
    Span::set_call_site(pos);
    if self.member && slf.is_none() {
      slf = Some(args.remove(0));
    }
    (self.exec)(env, slf.unwrap_or(Var::None), args, pos)
  }
  fn docs(&self) -> MarkdownSection {
    self.docs.clone()
  }
  fn arg_docs(&self) -> Vec<(&'static str, Option<VarType>)> {
    self.args.clone()
  }
}

impl Default for RtEnv {
  fn default() -> Self {
    RtEnv::new()
  }
}

impl RtEnv {
  pub fn new() -> Self {
    let mut env = RtEnv {
      names:          HashMap::new(),
      parsed_files:   vec![],
      globals:        HashMap::new(),
      predefined:     HashMap::new(),
      callback_impls: HashMap::new(),
      time_limit:     None,
      out:            Arc::new(Mutex::new(std::io::stdout())),
      err:            Arc::new(Mutex::new(std::io::stderr())),
    };
    env.names.insert(path!(str), Item::Module);
    env.names.insert(path!(int), Item::Module);
    env.names.insert(path!(float), Item::Module);
    env.names.insert(path!(bool), Item::Module);
    env.names.insert(path!(arr), Item::Module);
    env.names.insert(path!(map), Item::Module);
    env.names.insert(Path::empty(), Item::Module);
    env.add_core_builtins();
    env
  }
  pub fn add_callback(&mut self, name: String, args: Vec<VarType>, docs: MarkdownSection) {
    self.callback_impls.insert(name, CallbackImpl { docs, args, impls: vec![] });
  }
  pub fn predefine(
    &mut self,
    name: String,
    docs: MarkdownSection,
    getter: Box<dyn Fn() -> Var + Send + Sync>,
  ) {
    self.predefined.insert(name, VarGetter { docs, func: getter });
  }
  /// Locks all the global data. This should happen any time a function is
  /// called from rust.
  pub fn lock<'a>(&'a self, files: &'a [SlFile]) -> LockedEnv {
    LockedEnv {
      locals: vec![],
      globals: &self.globals,
      predefined: &self.predefined,
      callback_impls: &self.callback_impls,
      names: &self.names,
      time_limit: self.time_limit,
      time_started: Instant::now(),
      files,
    }
  }
  /// Adds a builtin function. In Panda, this function will look just like a
  /// normal function. However, when the function is called it will invoke `f`,
  /// instead of a panda function.
  pub fn add_builtin_fn<
    F: Fn(&mut LockedEnv, Var, Vec<Var>, Span) -> Result<Var> + 'static + Send + Sync,
  >(
    &mut self,
    path: Path,
    docs: MarkdownSection,
    takes_self: bool,
    f: F,
  ) {
    let name: String = path.last().into();
    self.names.insert(
      path,
      Item::Func(FuncItem {
        imp: Arc::new(BuiltinFunc {
          docs,
          name,
          member: takes_self,
          args: vec![],
          args_must_match: false,
          ret: VarType::None,
          exec: Arc::new(f),
        }),
      }),
    );
  }
  /// Adds a builtin type. This should probably be created with the proc macro
  /// `define_ty`.
  pub fn add_builtin_ty<T: BuiltinImpl + 'static>(&mut self) {
    let p = T::path();
    self.declare_module(&p);
    self.names.insert(
      p.clone(),
      Item::Struct(StructItem {
        imp:        StructCallable::Builtin(Arc::new(T::strct())),
        // builtins cannot implement traits
        trait_imps: vec![],
      }),
    );
    for f in T::funcs() {
      let mut p = p.clone();
      p.push(f.name());
      self.names.insert(p, Item::Func(FuncItem { imp: f }));
    }
  }
  /// Calls the given function. This will call a builtin.
  ///
  /// # Panics
  /// If the function is not defined.
  ///
  /// This is mostly because the error message doesn't make much sense, and also
  /// because the caller should check if the function doesn't exist before
  /// calling this.
  pub fn call(
    &self,
    path: &Path,
    slf: Option<Var>,
    args: Vec<Var>,
    files: &[SlFile],
  ) -> Result<Var> {
    match self.names.get(path) {
      Some(Item::Func(f)) => {
        let mut env = self.lock(files);
        f.imp.check_args_rt(&mut env, Span::default(), slf.is_some(), &args)?;
        f.imp.exec(&mut env, slf, args, Span::default())
      }
      Some(v) => panic!("not a function: {:?} at {}", v, path),
      None => panic!("undefined function {}", path),
    }
  }
  /// Checks if there is a function at the given path. Note that this will
  /// return false if the path doesn't exist, or if the item at that path is not
  /// a function.
  pub fn has_func(&self, path: &Path) -> bool {
    matches!(self.names.get(path), Some(Item::Func(_)))
  }
  /// Gets a struct type.
  pub fn get_type(&mut self, name: &PathLit) -> Result<&StructItem> {
    match self.names.get(name.path()) {
      Some(Item::Struct(v)) => Ok(v),
      _ => Err(RuntimeError::Undefined {
        pos:   name.pos(),
        name:  "type",
        value: format!("{}", name.path()),
        ty:    None,
      }),
    }
  }
  /// Returns the entire import namespace. This means it will include any
  /// structs and functions declared in any file.
  ///
  /// If you have completed the first pass of a file, but not the second pass,
  /// this structure will be broken! Only call this when you need to do
  /// something like tab completion, and take care that everything has been
  /// parsed before calling this.
  pub fn names(&self) -> &HashMap<Path, Item> {
    &self.names
  }
  /// Adds the given file to the list of parsed files. Once [`second_pass`] is
  /// called, all of the added files will be compiled into a runtime tree.
  /// Until [`second_pass`] is called, the file `f` will not be accessible to
  /// the runtime at all.
  ///
  /// [`second_pass`]: Self::second_pass
  pub fn first_pass_file(&mut self, f: ParsedFile) {
    self.parsed_files.push(f);
  }
  /// Adds a module at the give path. This is a placeholder, and is used for
  /// import validation.
  pub fn declare_module(&mut self, path: &Path) {
    for i in 1..=path.len() {
      self.names.insert(Path::new(path.inner()[..i].into()), Item::Module);
    }
  }
}

impl<'a> LockedEnv<'a> {
  /// Runs a callback.
  ///
  /// # Panics
  ///
  /// - If the callback has not been defined yet.
  /// - If the number of arguments passed doesn't match the number of arguments
  ///   it was defined with.
  #[track_caller]
  pub fn run_callback(&mut self, name: &str, args: Vec<Var>) -> Result<()> {
    let cbs = match self.callback_impls.get(name) {
      Some(v) => v,
      None => panic!("invalid callback `{name}`"),
    };
    if args.len() != cbs.args.len() {
      panic!(
        "callback was defined with {} arguments, but was called with {} arguments",
        cbs.args.len(),
        args.len()
      );
    }
    let mut to_calls = vec![];
    for cb in &cbs.impls {
      // TODO: Validate at parse time
      if args.len() != cb.args.names.len() {
        return Err(RuntimeError::custom(
          format!(
            "this callback should take {} arguments, but was defined with {}",
            args.len(),
            cb.args.names.len()
          ),
          cb.args.pos,
        ));
      }
      // cb.check_args_rt(self, Span::default(), false, &args)?;
      let mut to_call = true;
      if let Some(when) = &cb.when {
        let res = when.exec(self)?;
        if !res.bool(when.pos())? {
          to_call = false;
        }
      }
      to_calls.push(to_call);
    }
    for (_, cb) in cbs.impls.iter().enumerate().filter(|(i, _)| to_calls[*i]) {
      cb.exec(self, args.clone(), Span::default())?;
    }
    Ok(())
  }
  /// Calls a function. If `slf` is not a `ty`, this is considered a logic
  /// error. The reason these are seperate is for standalone funcitons. A
  /// standalone function will pass in None as slf, but the name and path of
  /// that function must be known.
  ///
  /// It is considered a logic error for an undefined type to be passed in at
  /// this stage. Those should have been caught during the second pass.
  ///
  /// Since this is meant to be called by the runtime environment, this will
  /// return an error if the function is not defined. In the future, all of
  /// these errors will be caught at runtime, and this will panic
  /// if the function is not defined. However, because that is not implemented,
  /// this will just return an error if the function doesn't exist.
  pub fn call(&mut self, name: &Path, pos: Span, slf: Option<Var>, args: Vec<Var>) -> Result<Var> {
    match self.get_item(name) {
      Some(Item::Func(f)) => f.imp.clone().exec(self, slf, args, pos),
      Some(v) => panic!("not a function {:?}", v),
      None => panic!("no function at {:?}", name),
    }
  }
  /// Gets a struct type.
  pub fn get_type(&self, name: &PathLit) -> Result<&StructItem> {
    match self.get_item(name.path()) {
      Some(Item::Struct(v)) => Ok(v),
      _ => Err(RuntimeError::Undefined {
        pos:   name.pos(),
        name:  "type",
        value: format!("{}", name.path()),
        ty:    None,
      }),
    }
  }
  pub fn get_item(&self, name: &Path) -> Option<&Item> {
    self.names.get(name)
  }
  /// Gets a variable from a variable id.
  #[track_caller]
  fn lookup(&self, id: &VarID) -> VarRef {
    if let Some(id) = id.local_value() {
      match self.locals.get(id) {
        Some(var) => VarRef::Ref(var),
        None => unreachable!("tried to look up invalid {}", id),
      }
    } else if let Some(name) = id.global_value() {
      match self.globals.get(name) {
        Some(var) => VarRef::Guard(var.lock().unwrap()),
        None => unreachable!("tried to look up invalid {}", id),
      }
    } else {
      unreachable!()
    }
  }
  /// Sets a variable. This will handle globals/locals correctly.
  pub fn set_var(&mut self, id: &VarID, new: Var) {
    if let Some(id) = id.local_value() {
      if id == self.locals.len() {
        self.locals.push(new);
      } else {
        match self.locals.get_mut(id) {
          Some(var) => *var = new,
          None => unreachable!("tried to look up invalid variable {}", id),
        }
      }
    } else if let Some(name) = id.global_value() {
      *self.globals[name].lock().unwrap() = new.into();
    }
  }

  /// Checks if the time limit has passed. If it has, this will return a
  /// RuntimeError.
  pub fn check_time_left(&self, pos: Span) -> Result<()> {
    match self.time_limit {
      Some(limit) if self.time_started.elapsed() > limit => Err(RuntimeError::TimeLimit(pos)),
      _ => Ok(()),
    }
  }

  /// Creates a new inner scope. This is called whenever we enter a function
  /// call, and local variables need to be reset.
  pub fn new_scope(&mut self, vars: usize) -> LockedEnv<'a> {
    LockedEnv::<'a> {
      locals:         vec![Var::None; vars],
      globals:        self.globals,
      predefined:     self.predefined,
      callback_impls: self.callback_impls,
      names:          self.names,
      time_limit:     self.time_limit,
      time_started:   self.time_started,
      files:          self.files,
    }
  }

  /// Returns the file of the given span. If the file index stored in the span
  /// is invalid, this will return `None`.
  pub fn file_of(&self, pos: Span) -> Option<&SlFile> {
    self.files.get(pos.file() as usize)
  }
}

#[derive(Debug)]
pub enum RuntimeError {
  // Undefined function/type/field. The first string is the word "function", "type", etc.
  // The second string is the actual function or type name.
  Undefined { pos: Span, name: &'static str, value: String, ty: Option<VarType> },
  Custom(String, Span),
  // These aren't really errors. They are thrown any time a break or continue
  // is hit. Loops just catch these errors, as it is the easiest way to call
  // up the stack of executing nodes. If these do reach the top of the stack,
  // they should be parsed as a 'continue outside loop' error.
  Break(Span),
  Continue(Span),
  // This is when the time limit for the whole call was hit. The span is where
  // the execution stopped.
  TimeLimit(Span),
}

impl fmt::Display for RuntimeError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::Undefined { name, value, ty, .. } => {
        if let Some(ty) = ty {
          write!(f, "Undefined {} `{}` on type {}", name, value, ty)
        } else {
          write!(f, "Undefined {} `{}`", name, value)
        }
      }
      Self::Custom(msg, _) => {
        write!(f, "{}", msg)
      }
      Self::Break(_) => {
        write!(f, "Cannot call `break` outside a loop",)
      }
      Self::Continue(_) => {
        write!(f, "Cannot call `continue` outside a loop",)
      }
      Self::TimeLimit(_) => {
        write!(f, "Time limit reached",)
      }
    }
  }
}

impl Error for RuntimeError {}

impl panda_parse::PdError for RuntimeError {
  fn pos(&self) -> Span {
    match self {
      Self::Undefined { pos, .. } => *pos,
      Self::Custom(_, pos) => *pos,
      Self::Break(pos) => *pos,
      Self::Continue(pos) => *pos,
      Self::TimeLimit(pos) => *pos,
    }
  }
  fn print(&self, src: &str, path: &StdPath, color: bool) {
    print!("{}", self.pos().underline(src, path.to_str().unwrap(), &self.to_string(), color))
  }
  fn gen(&self, src: &str, path: &StdPath, color: bool) -> String {
    self.pos().underline(src, path.to_str().unwrap(), &self.to_string(), color)
  }
}

impl RuntimeError {
  pub fn custom<M: Into<String>>(msg: M, pos: Span) -> Self {
    Self::Custom(msg.into(), pos)
  }
  /// Creates an error if the argument length is not exactly the amount
  /// specified.
  pub fn check_arg_len(args: &[Var], len: usize, pos: Span) -> Result<()> {
    if args.len() != len {
      Err(Self::Custom(format!("expected {} arguments, got {} arguments", len, args.len()), pos))
    } else {
      Ok(())
    }
  }
  /// Creates an error if the argument length is below the given minimum.
  pub fn check_arg_min(args: &[Var], min: usize, pos: Span) -> Result<()> {
    if args.len() < min {
      Err(Self::Custom(
        format!("expected at least {} arguments, got {} arguments", min, args.len()),
        pos,
      ))
    } else {
      Ok(())
    }
  }
  /// Creates an error if the argument length is not within `min..=max`.
  pub fn check_arg_range(args: &[Var], min: usize, max: usize, pos: Span) -> Result<()> {
    if args.len() < min || args.len() > max {
      Err(Self::Custom(
        format!("expected {} to {} arguments, got {} arguments", min, max, args.len()),
        pos,
      ))
    } else {
      Ok(())
    }
  }
}

pub type Result<T> = std::result::Result<T, RuntimeError>;

#[test]
fn sends() {
  macro_rules! assert_impl {
    ($x:ty, $($t:path),+ $(,)*) => {
      const _: fn() -> () = || {
        fn assert_impl<T: ?Sized $(+ $t)*>() {}

        assert_impl::<$x>();
      };
    };
  }
  macro_rules! assert_not_impl {
    ($x:ty, $($t:path),+ $(,)*) => {
      const _: fn() -> () = || {
        struct Check<T: ?Sized>(T);
        trait AmbiguousIfImpl<A> { fn some_item() { } }

        impl<T: ?Sized> AmbiguousIfImpl<()> for Check<T> { }
        impl<T: ?Sized $(+ $t)*> AmbiguousIfImpl<u8> for Check<T> { }

        <Check::<$x> as AmbiguousIfImpl<_>>::some_item()
      };
    };
  }

  assert_impl!(RtEnv, Send, Sync);
  assert_impl!(crate::VarSend, Send, Sync);
  assert_not_impl!(Var, Send, Sync);
  assert_not_impl!(LockedEnv, Send, Sync);
}
