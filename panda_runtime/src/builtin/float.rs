use crate::{RtEnv, RuntimeError, Var};
use panda_docs::markdown;
use std::rc::Rc;

pub fn add_core_builtins(env: &mut RtEnv) {
  env.add_builtin_fn(path!(float::to_s), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let slf = slf.int(pos)?;
    Ok(Var::String(Rc::new(slf.to_string())))
  });
  env.add_builtin_fn(path!(float::to_i), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let slf = slf.int(pos)?;
    Ok(Var::Int(slf as i64))
  });

  env.add_builtin_fn(path!(float::sin), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let val = slf.float(pos)?;
    Ok(Var::Float(val.sin()))
  });
  env.add_builtin_fn(path!(float::cos), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let val = slf.float(pos)?;
    Ok(Var::Float(val.cos()))
  });
}
