use crate::{RtEnv, RuntimeError, Var};
use panda_docs::markdown;
use std::{cell::RefCell, rc::Rc};

pub fn add_core_builtins(env: &mut RtEnv) {
  env.add_builtin_fn(path!(map::len), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let slf = slf.map(pos)?;
    Ok(Var::Int(slf.len() as i64))
  });
  env.add_builtin_fn(path!(map::keys), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let slf = slf.map(pos)?;
    Ok(Var::Array(Rc::new(RefCell::new(slf.keys().map(|v| v.clone().into()).collect()))))
  });
  env.add_builtin_fn(path!(map::contains_key), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let slf = slf.map(pos)?;
    let key = args[0].as_map_key().ok_or_else(|| {
      RuntimeError::custom(&format!("The type `{}` cannot be used as a map key", args[0].ty()), pos)
    })?;
    Ok(Var::Bool(slf.contains_key(&key)))
  });
  env.add_builtin_fn(path!(map::index), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let slf = slf.map(pos)?;
    let key = args[0].as_map_key().ok_or_else(|| {
      RuntimeError::custom(&format!("The type `{}` cannot be used as a map key", args[0].ty()), pos)
    })?;
    match slf.get(&key) {
      Some(v) => Ok(v.clone()),
      None => {
        Err(RuntimeError::custom(&format!("The key `{}` does not exist for this map", key), pos))
      }
    }
  });
  env.add_builtin_fn(path!(map::insert), markdown!(), true, |_, slf, mut args, pos| {
    RuntimeError::check_arg_len(&args, 2, pos)?;
    let val = args.pop().unwrap();
    let key = args.pop().unwrap();
    let mut slf = slf.map_mut(pos)?;
    let key = key.as_map_key().ok_or_else(|| {
      RuntimeError::custom(format!("The key `{key}` is not a valid map key"), pos)
    })?;
    slf.insert(key, val);
    Ok(Var::None)
  });
  env.add_builtin_fn(path!(map::remove), markdown!(), true, |_, slf, mut args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let key = args.pop().unwrap();
    let mut slf = slf.map_mut(pos)?;
    let key = key.as_map_key().ok_or_else(|| {
      RuntimeError::custom(format!("The key `{key}` is not a valid map key"), pos)
    })?;
    Ok(slf.remove(&key).unwrap_or(Var::None))
  });
}
