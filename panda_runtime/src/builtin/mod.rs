use panda_docs::markdown;

use crate::{LockedEnv, RtEnv, RuntimeError, Var, WriteAny};
use std::sync::{Arc, Mutex};

mod arr;
mod float;
mod int;
mod map;
mod str;

#[derive(Debug, Clone, Hash, PartialEq)]
struct StdBuiltins;

impl<'a> crate::TryFromSpan<&'a crate::Var> for std::cell::RefMut<'a, StdBuiltins> {
  type Error = crate::RuntimeError;

  fn try_from_span(
    var: &'a crate::Var,
    pos: panda_parse::token::Span,
  ) -> Result<std::cell::RefMut<'a, StdBuiltins>, crate::RuntimeError> {
    let strct = var.strct(pos)?;
    let mut builtin = strct.as_builtin_mut(pos)?;
    if !builtin.as_any_mut().is::<StdBuiltins>() {
      return Err(crate::RuntimeError::custom(
        &format!("expected builtin type StdBuiltins, got {}", strct.path()),
        pos,
      ));
    }
    Ok(std::cell::RefMut::map(builtin, |v| v.as_any_mut().downcast_mut::<StdBuiltins>().unwrap()))
  }
}

impl crate::BuiltinImpl for StdBuiltins {
  fn path() -> crate::Path {
    path!(StdBuiltins)
  }
  fn funcs() -> Vec<std::sync::Arc<dyn crate::FuncCallable + Send + Sync>> {
    vec![]
  }
  fn strct() -> crate::BuiltinStruct {
    crate::BuiltinStruct {
      docs:           &[],
      name:           "".into(),
      fields:         vec![],
      dynamic_fields: false,
    }
  }
  fn field(&self, _: &crate::parse::token::Ident) -> Option<crate::Var> {
    None
  }
  fn box_clone(&self) -> Box<dyn crate::BuiltinImpl + Send + Sync> {
    Box::new(self.clone())
  }
  fn rc_clone(&self) -> ::std::rc::Rc<::std::cell::RefCell<dyn crate::BuiltinImpl + Send + Sync>> {
    ::std::rc::Rc::new(::std::cell::RefCell::new(self.clone()))
  }
  fn as_any(&self) -> &dyn std::any::Any {
    self
  }
  fn as_any_mut(&mut self) -> &mut dyn std::any::Any {
    self
  }
  fn as_map_key(&self) -> Option<crate::MapKeyBuiltin> {
    None
  }
}
impl crate::MapKeyBuiltinImpl for StdBuiltins {
  fn to_builtin(
    &self,
  ) -> ::std::rc::Rc<::std::cell::RefCell<dyn crate::BuiltinImpl + Send + Sync>> {
    ::std::rc::Rc::new(::std::cell::RefCell::new(self.clone()))
  }
  fn hash(&self, state: &mut crate::DynHasher) {
    <Self as ::std::hash::Hash>::hash(self, state);
  }
  fn box_clone(&self) -> Box<dyn crate::MapKeyBuiltinImpl + Send + Sync> {
    Box::new(self.clone())
  }
  fn as_any(&self) -> &dyn std::any::Any {
    self
  }
  fn eq(&self, other: &dyn crate::MapKeyBuiltinImpl) -> bool {
    if let Some(other) = other.as_any().downcast_ref::<Self>() {
      <Self as ::std::cmp::PartialEq>::eq(self, other)
    } else {
      false
    }
  }
}

/*
#[panda_derive::define_ty(crate = "crate", path = "")]
impl StdBuiltins {
  pub fn println(args: Variadic<&Var>) {
    for (i, v) in args.iter().enumerate() {
      if i == args.len() - 1 {
        print!("{}", v);
      } else {
        print!("{} ", v);
      }
    }
    println!();
  }
  pub fn print(args: Variadic<&Var>) {
    for (i, v) in args.iter().enumerate() {
      if i == args.len() - 1 {
        print!("{}", v);
      } else {
        print!("{} ", v);
      }
    }
  }
  pub fn exit(code: Option<i32>) {
    if let Some(code) = code {
      process::exit(code);
    } else {
      process::exit(0);
    }
  }
  pub fn test_res() -> Result<i32, RuntimeError> {
    Ok(5)
  }
  // Eval is for wrong people. This was written as a demostration of how easy it
  // would be to implemented. I am leaving this commented out because it is
  // basically the worst security vulnerability possible to execute unknown
  // text.
  //
  // If you really need this, add it on your own instance of `Panda`.
  //
  // pub fn eval(src: &str) -> Var {
  //   let mut sl = crate::Panda::new();
  //   sl.env().add_std_builtins();
  //   match sl.parse_statement(src.as_bytes()) {
  //     Ok(stat) => match sl.run(stat) {
  //       Ok(v) => v,
  //       Err(e) => {
  //         e.print(src);
  //         Var::None
  //       }
  //     },
  //     Err(e) => {
  //       e.print(src);
  //       Var::None
  //     }
  //   }
  // }
}
*/

impl RtEnv {
  /// These are the core functions needed for any panda file. This includes
  /// things like `push` on arrays, and other functions that are needed for
  /// basic language functionality.
  pub(super) fn add_core_builtins(&mut self) {
    arr::add_core_builtins(self);
    float::add_core_builtins(self);
    int::add_core_builtins(self);
    map::add_core_builtins(self);
    str::add_core_builtins(self);
  }
  /// This adds standard features to panda. This includes things like
  /// `println`, `exit`, and `readline`. These are not needed for language
  /// usage, and should not be added if you want a sandboxed environment.
  pub fn add_std_builtins(&mut self) {
    fn format(out: &Arc<Mutex<dyn WriteAny + Send + Sync>>, args: Vec<Var>, _: &mut LockedEnv) {
      let mut o = out.lock().unwrap();
      for (i, v) in args.iter().enumerate() {
        if i == args.len() - 1 {
          write!(o, "{}", v).unwrap();
        } else {
          write!(o, "{} ", v).unwrap();
        }
      }
      writeln!(o).unwrap();
    }
    // self.add_builtin_ty::<StdBuiltins>();
    let out = self.out.clone();
    self.add_builtin_fn(path!(println), markdown!(), false, move |env, _, args, _| {
      format(&out, args, env);
      Ok(Var::None)
    });
    let err = self.err.clone();
    self.add_builtin_fn(path!(eprintln), markdown!(), false, move |env, _, args, _| {
      format(&err, args, env);
      Ok(Var::None)
    });
  }
  /// This includes testing functions like `assert`. This is used internally for
  /// language feature testing, and shouldn't be included in normal user code.
  pub fn add_test_builtins(&mut self) {
    self.add_builtin_fn(path!(assert), markdown!(), false, |env, _, args, pos| {
      RuntimeError::check_arg_range(&args, 1, 2, pos)?;
      let val = args[0].bool(pos)?;
      if !val {
        if let Some(file) = env.file_of(pos) {
          println!("Assert false at {}:{}", file.path().display(), pos.start().index());
        } else {
          println!("Assert false at {}", pos.start().index());
        }
        if args.len() == 2 {
          println!("Reason: {}", args[1]);
        }
        Err(RuntimeError::custom("assert failed", pos))
      } else {
        Ok(Var::None)
      }
    });
    self.add_builtin_fn(path!(assert_eq), markdown!(), false, |env, _, args, pos| {
      RuntimeError::check_arg_range(&args, 2, 3, pos)?;
      let lhs = &args[0];
      let rhs = &args[1];
      if lhs != rhs {
        if let Some(file) = env.file_of(pos) {
          println!(
            "Assert eq got differing values in {}:{}",
            file.path().display(),
            pos.start().index(),
          );
        } else {
          println!("Assert eq got differing values at {}", pos.start().index(),);
        }
        println!("lhs: {} (type: {})", lhs, lhs.ty());
        println!("rhs: {} (type: {})", rhs, rhs.ty());
        if args.len() == 3 {
          println!("Reason: {}", args[2]);
        }
        Err(RuntimeError::custom("assert_eq failed", pos))
      } else {
        Ok(Var::None)
      }
    });
    self.add_builtin_fn(path!(assert_neq), markdown!(), false, |env, _, args, pos| {
      RuntimeError::check_arg_range(&args, 2, 3, pos)?;
      let lhs = &args[0];
      let rhs = &args[1];
      if lhs == rhs {
        if let Some(file) = env.file_of(pos) {
          println!(
            "Assert neq got the same value at {}:{}",
            file.path().display(),
            pos.start().index(),
          );
        } else {
          println!("Assert neq got the same value at {}", pos.start().index(),);
        }
        println!("lhs: {}", lhs);
        println!("rhs: {}", rhs);
        if args.len() == 3 {
          println!("Reason: {}", args[2]);
        }
        Err(RuntimeError::custom("assert_neq failed", pos))
      } else {
        Ok(Var::None)
      }
    });
  }
}
