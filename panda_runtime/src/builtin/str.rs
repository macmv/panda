use crate::{RtEnv, RuntimeError, Var};
use panda_docs::markdown;
use std::{cell::RefCell, rc::Rc};

/// This is an implementation of split very similar to ruby's. The output will
/// never contain any empty strings.
fn split_str(val: &str, sep: &str) -> Vec<String> {
  if sep.is_empty() {
    val.chars().map(|c| c.into()).collect::<Vec<String>>()
  } else {
    if val.len() < sep.len() {
      if val.is_empty() {
        return vec![];
      } else {
        return vec![val.into()];
      }
    } else if val == sep {
      return vec![];
    }
    let mut res = vec![];
    let mut prev_end = 0;
    for (i, _) in val.chars().enumerate().skip(sep.len()) {
      if val[i - sep.len()..i] == *sep {
        if prev_end != i - sep.len() {
          res.push(val[prev_end..i - sep.len()].into());
        }
        prev_end = i;
      }
    }
    if prev_end < val.len() - sep.len() {
      res.push(val[prev_end..val.len()].into());
    }
    res
  }
}

pub fn add_core_builtins(env: &mut RtEnv) {
  env.add_builtin_fn(path!(str::len), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 0, pos)?;
    let val = slf.string(pos)?;
    Ok(Var::Int(val.len() as i64))
  });
  env.add_builtin_fn(path!(str::split), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let val = slf.string(pos)?;
    let sep = args[0].string(pos)?;
    Ok(Var::Array(Rc::new(RefCell::new(
      split_str(val, sep).into_iter().map(|s| Var::String(Rc::new(s))).collect::<Vec<Var>>(),
    ))))
  });
  env.add_builtin_fn(path!(str::repeat), markdown!(), true, |_, slf, args, pos| {
    RuntimeError::check_arg_len(&args, 1, pos)?;
    let val = slf.string(pos)?;
    let num = args[0].int(pos)?;
    if num < 0 {
      Err(RuntimeError::custom("Cannot repeat a string a negative number of times", pos))
    } else {
      Ok(Var::String(Rc::new(val.repeat(num as usize))))
    }
  });
}
