//! Converts a parse tree into a runtime tree. This does everything except
//! convert function calls into their full paths.

use super::{CompileEnv, Initialized};
use crate::tree::{
  ArrLit as RtArrLit, AssignOp as RtAssignOp, AssignOpKind as RtAssignOpKind, CallArgs, ClosureLit,
  Expr as RtExpr, If as RtIf, LHSKind as RtLHSKind, Lit as RtLit, Loop as RtLoop,
  MapLit as RtMapLit, OpKind, Statement as RtStatement, StatementList as RtStatementList,
  StructLit as RtStructLit, LHS as RtLHS,
};
use panda_parse::{
  token::{Ident, ParseError, PathLit, Result},
  tree::{AssignOpKind, Expr, LHSKind, Lit, Loop, Op, Statement, StatementList, LHS},
};

impl CompileEnv<'_> {
  /// Runs the second parsing pass on a statement list.
  pub(super) fn compile_stat_list(&mut self, stat: StatementList) -> Result<RtStatementList> {
    let mut err = ParseError::Empty;
    let mut list = RtStatementList { items: vec![] };
    for s in stat.items {
      match self.compile_stat(s) {
        Ok(item) => list.items.push(item),
        Err(e) => err.join(e),
      }
    }
    err.into_result(list)
  }

  /// This expands paths into their full value. This will look at imports and
  /// expand the path accordingly. This should not be called for local
  /// variables.
  fn expand_path(&mut self, path: &mut PathLit, kind: &'static str) -> Result<()> {
    if !self.has_name(&path.first()) {
      return Err(ParseError::undefined(kind, Ident::new(path.to_string(), path.pos())));
    }
    let mut prefix = self.expand_name(path.first())?;
    prefix.pop();
    path.push_all_prefix(prefix);
    self.validate_path(path)?;
    Ok(())
  }

  fn compile_lhs(&mut self, lhs: LHS, set: bool) -> Result<RtLHS> {
    if lhs.prefix.is_none() {
      if let LHSKind::Name(name) = lhs.last {
        if set {
          self.mark_initialized(&name);
        }
        return Ok(RtLHS::Var(self.get_local(&name)?));
      }
    }
    let mut prefix = self.compile_expr(lhs.prefix.unwrap())?;
    prefix = match prefix {
      RtExpr::FuncRef(path) if path.len() == 1 => RtExpr::IdRef(self.get_local(&path.as_ident())?),
      v => v,
    };
    Ok(match lhs.last {
      LHSKind::Name(name) => RtLHS::Value(prefix, RtLHSKind::Name(name)),
      LHSKind::Index(expr) => RtLHS::Value(prefix, RtLHSKind::Index(self.compile_expr(expr)?)),
    })
  }

  fn compile_stat(&mut self, s: Statement) -> Result<RtStatement> {
    Ok(match s {
      Statement::Let(name, e) => RtStatement::Let(
        self.define_local(name, e.is_some())?,
        match e {
          Some(e) => Some(self.compile_expr(e)?),
          None => None,
        },
      ),
      Statement::Expr(e) => RtStatement::Expr(self.compile_expr(e)?),
      Statement::Assign(op) => {
        let is_set = matches!(op.op, AssignOpKind::Set(_));
        RtStatement::Assign(RtAssignOp {
          op:  match op.op {
            AssignOpKind::Set(e) => RtAssignOpKind::Set(self.compile_expr(e)?),
            AssignOpKind::Inc => RtAssignOpKind::Inc,
            AssignOpKind::Dec => RtAssignOpKind::Dec,
            AssignOpKind::Add(e) => RtAssignOpKind::Add(self.compile_expr(e)?),
            AssignOpKind::Sub(e) => RtAssignOpKind::Sub(self.compile_expr(e)?),
            AssignOpKind::Mul(e) => RtAssignOpKind::Mul(self.compile_expr(e)?),
            AssignOpKind::Div(e) => RtAssignOpKind::Div(self.compile_expr(e)?),
            AssignOpKind::Mod(e) => RtAssignOpKind::Mod(self.compile_expr(e)?),
            AssignOpKind::Exp(e) => RtAssignOpKind::Exp(self.compile_expr(e)?),
          },
          // Local variable validation happens in the above call, so this must be after that
          // happens (a = a + 2 is invalid, as `a` isn't defined yet)
          lhs: self.compile_lhs(op.lhs, is_set)?,
        })
      }
      Statement::If(e) => RtStatement::If(RtIf {
        cond:       self.compile_expr(e.cond)?,
        block:      {
          self.enter_if();
          self.compile_stat_list(e.block)?
        },
        else_if:    {
          let mut list = Vec::with_capacity(e.else_if.len());
          for (cond, block) in e.else_if {
            self.swap_else();
            list.push((self.compile_expr(cond)?, self.compile_stat_list(block)?));
          }
          list
        },
        else_block: match e.else_block {
          Some(block) => {
            self.swap_else();
            let block = self.compile_stat_list(block)?;
            self.leave_else();
            Some(block)
          }
          None => {
            self.leave_if();
            None
          }
        },
      }),
      Statement::Loop(stat) => RtStatement::Loop(match stat {
        Loop::For { first, second, iter, block, keyword_pos } => {
          self.enter_for_while();
          let l = RtLoop::For {
            first: self.define_local(first, true)?,
            second: match second {
              Some(s) => Some(self.define_local(s, true)?),
              None => None,
            },
            iter: self.compile_expr(iter)?,
            block: self.compile_stat_list(block)?,
            keyword_pos,
          };
          self.leave_for_while();
          l
        }
        Loop::While { cond, block, keyword_pos } => {
          self.enter_for_while();
          let l = RtLoop::While {
            cond: self.compile_expr(cond)?,
            block: self.compile_stat_list(block)?,
            keyword_pos,
          };
          self.leave_for_while();
          l
        }
        Loop::Loop { block, keyword_pos } => {
          self.enter_loop();
          let l = RtLoop::Loop { block: self.compile_stat_list(block)?, keyword_pos };
          self.leave_loop();
          l
        }
      }),
      Statement::Break(pos) => RtStatement::Break(pos),
      Statement::Continue(pos) => RtStatement::Continue(pos),
    })
  }

  pub fn compile_expr(&mut self, e: Expr) -> Result<RtExpr> {
    let mut res = self.compile_expr_no_expand(e)?;
    // Local variables get expanded here, so that we don't end up using a variable
    // before it's declared.
    match &mut res {
      RtExpr::Lit(RtLit::Struct(strct)) => self.expand_path(&mut strct.name, "struct")?,
      RtExpr::Call(slf, path, _) => {
        // TODO: We should know the type of `slf`, and be able to validate calls.
        if slf.is_none() {
          self.expand_path(path, "function")?;
        }
      }
      RtExpr::FuncRef(path) => {
        if path.len() == 1 {
          if let Some(var) = self.get_local_opt(&path.as_ident()) {
            res = RtExpr::IdRef(var?);
          } else {
            self.expand_path(path, "variable")?;
          }
        } else {
          // They are trying to create a function reference (callback), so we call it a
          // function. A struct sould be caught in the `Lit` check above.
          self.expand_path(path, "function")?;
        }
      }
      _ => {}
    }
    Ok(res)
  }
  fn compile_expr_no_expand(&mut self, e: Expr) -> Result<RtExpr> {
    Ok(match e {
      Expr::Lit(l) => RtExpr::Lit(self.compile_lit(l)?),
      Expr::Op(lhs, rhs, op) => self.compile_op(lhs.map(|v| *v), rhs.map(|v| *v), op)?,
      Expr::Ident(name) => RtExpr::FuncRef(name.into_path()),
      Expr::Paren(e) => RtExpr::Paren(Box::new(self.compile_expr(*e)?)),
      Expr::Call(name, args) => RtExpr::Call(
        None,
        name.into_path(),
        CallArgs {
          pos:  args.pos(),
          args: args
            .into_values()
            .into_iter()
            .map(|v| self.compile_expr(v))
            .collect::<Result<_>>()?,
        },
      ),
      Expr::Block(pos, block) => RtExpr::Block(pos, {
        self.enter_block();
        let b = self.compile_stat_list(block)?;
        self.leave_block();
        b
      }),
      Expr::Predefined(p) => {
        if self.predefined.contains_key(&p.val) {
          RtExpr::Predefined(p)
        } else {
          return Err(ParseError::custom(format!("invalid predefined `{p}`"), p.pos));
        }
      }
      Expr::Closure(c) => RtExpr::Lit(RtLit::Closure({
        let mut env = self.closure_env(c.args.names.len());
        for (i, name) in c.args.names.iter().enumerate() {
          env.levels[0].vars.insert(
            name.to_string(),
            super::LocalVar {
              id:          i,
              pos:         name.pos(),
              initialized: vec![Initialized::Yes],
            },
          );
        }
        let body = env.compile_expr(*c.body)?;
        ClosureLit {
          args:       c.args,
          body:       Box::new(body),
          to_capture: env
            .outer_variables
            .into_iter()
            .filter_map(|(_, (id, used))| if used { Some(id) } else { None })
            .collect(),
        }
      })),
    })
  }

  fn compile_op(&mut self, lhs: Option<Expr>, rhs: Option<Expr>, op: Op) -> Result<RtExpr> {
    macro_rules! ops {
      ( $($name:ident),* ) => {
        match op {
          $(
            Op::$name => RtExpr::Op(Box::new(OpKind::$name(
              self.compile_expr(lhs.unwrap())?,
              self.compile_expr(rhs.unwrap())?,
            ))),
          )*
          Op::Not => RtExpr::Op(Box::new(OpKind::Not(
            self.compile_expr(rhs.unwrap())?,
          ))),
          Op::Dot => {
            let l = self.compile_expr(lhs.unwrap())?;
            let mut r = self.compile_expr_no_expand(rhs.unwrap())?;
            match &mut r {
              RtExpr::Call(val, _, _) => *val = Some(Box::new(l)),
              RtExpr::FuncRef(path) => if path.len() != 1 {
                return Err(ParseError::custom("invalid field name".into(), path.pos()))
              } else {
                r = RtExpr::Field(Box::new(l), path.as_ident());
              },
              RtExpr::IdRef(_) => unreachable!(),
              _ => return Err(ParseError::custom("invalid expression".into(), r.pos())),
            }
            r
          },
          Op::Path => {
            let l = lhs.unwrap();
            let mut r = self.compile_expr_no_expand(rhs.unwrap())?;
            match l {
              Expr::Ident(prefix) => match &mut r {
                RtExpr::Lit(RtLit::Struct(strct)) => strct.name.push_prefix(prefix),
                RtExpr::Call(_, path, _) => path.push_prefix(prefix),
                RtExpr::FuncRef(path) => path.push_prefix(prefix),
                RtExpr::IdRef(_) => unreachable!(),
                _ => return Err(ParseError::custom("invalid path segment".into(), r.pos())),
              }
              _ => return Err(ParseError::custom("invalid path segment".into(), l.pos())),
            }
            r
          },
          _ => todo!("op {:?}", op),
        }
      };
    }
    Ok(ops![Add, Sub, Div, Mul, Mod, Exp, And, Or, Eq, Neq, Less, Greater, LTE, GTE, Arr, Range])
  }

  fn compile_lit(&mut self, lit: Lit) -> Result<RtLit> {
    Ok(match lit {
      Lit::Bool(v) => RtLit::Bool(v),
      Lit::Int(v) => RtLit::Int(v),
      Lit::Float(v) => RtLit::Float(v),
      Lit::String(v) => RtLit::String(v),
      Lit::Arr(v) => RtLit::Arr(RtArrLit {
        pos: v.pos(),
        val: v.into_values().into_iter().map(|v| self.compile_expr(v)).collect::<Result<_>>()?,
      }),
      Lit::Map(v) => RtLit::Map(RtMapLit {
        pos: v.pos(),
        val: {
          let mut items = vec![];
          for it in v.into_values() {
            items.push((self.compile_expr(it.key)?, self.compile_expr(it.val)?));
          }
          items
        },
      }),
      Lit::Struct(v) => RtLit::Struct(RtStructLit {
        fields: {
          let mut items = vec![];
          for it in v.fields.into_values() {
            items.push((it.name, self.compile_expr(it.val)?));
          }
          items
        },
        name:   v.name.into_path(),
      }),
    })
  }
}
