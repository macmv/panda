use crate::{tree::Closure, LockedEnv, MapKeyVar, Path, Result, RuntimeError, Var};
use panda_parse::token::Span;
use std::{
  cell::{Ref, RefMut},
  convert::TryInto,
};

pub trait TryFromSpan<T> {
  type Error;
  fn try_from_span(v: T, pos: Span) -> std::result::Result<Self, Self::Error>
  where
    Self: Sized;
}
pub trait TryFromSpanRef<'a> {
  type Error;

  fn try_from_span_ref(v: &'a Var, pos: Span) -> std::result::Result<Ref<'a, Self>, Self::Error>
  where
    Self: Sized;
}
pub trait TryFromSpanMut<'a> {
  type Error;

  fn try_from_span_mut(v: &'a Var, pos: Span) -> std::result::Result<RefMut<'a, Self>, Self::Error>
  where
    Self: Sized;
}

pub trait TryIntoSpan<T> {
  type Error;

  fn try_into_span(self, pos: Span) -> std::result::Result<T, Self::Error>;
}

impl<T, U> TryIntoSpan<U> for T
where
  U: TryFromSpan<T>,
{
  type Error = U::Error;

  fn try_into_span(self, pos: Span) -> std::result::Result<U, U::Error> {
    U::try_from_span(self, pos)
  }
}

/// TryFromSpan includes all From implementations
impl<T, U> TryFromSpan<T> for U
where
  U: From<T>,
{
  type Error = RuntimeError;

  fn try_from_span(t: T, _: Span) -> std::result::Result<U, RuntimeError> {
    Ok(U::from(t))
  }
}

macro_rules! number_from_span {
  ( $( $ty:ty ),* ) => {
    $(
    impl TryFromSpan<Var> for $ty {
      type Error = RuntimeError;

      fn try_from_span(var: Var, pos: Span) -> Result<$ty> {
        var.int(pos)?.try_into().map_err(|_| {
          RuntimeError::custom(&format!("cannot convert {} into a `{}`", var, stringify!($ty)), pos)
        })
      }
    }

    impl TryFromSpan<MapKeyVar> for $ty {
      type Error = RuntimeError;

      fn try_from_span(var: MapKeyVar, pos: Span) -> Result<$ty> {
        var.int(pos)?.try_into().map_err(|_| {
          RuntimeError::custom(&format!("cannot convert {} into a `{}`", var, stringify!($ty)), pos)
        })
      }
    }
    )*
  };
}

number_from_span!(usize, u8, u16, u32, u64, u128);
number_from_span!(isize, i8, i16, i32);

impl TryFromSpan<Var> for i64 {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<i64> {
    var.int(pos)
  }
}
impl TryFromSpan<Var> for i128 {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<i128> {
    Ok(var.int(pos)?.into())
  }
}

impl TryFromSpan<Var> for f32 {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<f32> {
    Ok(var.float(pos)? as f32)
  }
}
impl TryFromSpan<Var> for f64 {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<f64> {
    var.float(pos)
  }
}

impl TryFromSpan<Var> for String {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<String> {
    Ok(var.string(pos)?.clone())
  }
}
impl<'a> TryFromSpan<&'a Var> for &'a String {
  type Error = RuntimeError;

  fn try_from_span(var: &'a Var, pos: Span) -> Result<&'a String> {
    var.string(pos)
  }
}
impl<'a> TryFromSpan<&'a Var> for &'a str {
  type Error = RuntimeError;

  fn try_from_span(var: &'a Var, pos: Span) -> Result<&'a str> {
    Ok(var.string(pos)?.as_ref())
  }
}

impl TryFromSpan<Var> for bool {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<bool> {
    var.bool(pos)
  }
}

impl<T> TryFromSpan<Var> for Vec<T>
where
  T: TryFromSpan<Var, Error = RuntimeError> + Clone,
{
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<Vec<T>> {
    let arr = var.arr(pos)?;
    let mut out = Vec::with_capacity(arr.len());
    for elem in arr.iter() {
      out.push(<T as TryFromSpan<Var>>::try_from_span(elem.clone(), pos)?);
    }
    Ok(out)
  }
}

/*
impl<'a, K, V> TryFromSpan<&'a Var> for HashMap<K, V>
where
  K: TryFromSpan<&'a MapKeyVar, Error = RuntimeError> + Eq + Hash,
  V: TryFromSpan<&'a Var, Error = RuntimeError>,
{
  type Error = RuntimeError;

  fn try_from_span(var: &'a Var, pos: Span) -> Result<HashMap<K, V>> {
    let map = var.map(pos)?;
    let mut out = HashMap::with_capacity(map.len());
    for (k, v) in map {
      out.insert(TryFromSpan::try_from_span(k, pos)?, TryFromSpan::try_from_span(v, pos)?);
    }
    Ok(out)
  }
}
*/

#[derive(Debug, Clone)]
pub struct Callback {
  path: Path,
}

impl Callback {
  /// Calls this function, using the given arguments. The environment must be
  /// the same environment that the Callback was recieved from, but it doesn't
  /// need to be the same lock. So a Callback can be stored for any amount of
  /// time, and you can unlock/lock the environment to use this callback at a
  /// later point.
  pub fn call(&self, env: &mut LockedEnv, args: Vec<Var>) -> Result<Var> {
    env.call(&self.path, Span::default(), None, args)
  }
}

impl TryFromSpan<Var> for Callback {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<Callback> {
    let path = var.callback(pos)?.clone();
    Ok(Callback { path })
  }
}

impl TryFromSpan<Var> for Closure {
  type Error = RuntimeError;

  fn try_from_span(var: Var, pos: Span) -> Result<Closure> {
    let closure = var.closure(pos)?.clone();
    Ok(closure)
  }
}
