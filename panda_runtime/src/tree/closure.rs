use std::collections::HashMap;

use super::Expr;
use crate::{LockedEnv, Result, Var, VarID, VarSend};
use panda_parse::{token::Span, tree::ClosureArgs};

#[derive(Debug, Clone)]
pub struct Closure {
  pub args:     ClosureArgs,
  pub body:     Expr,
  pub captured: HashMap<VarID, VarSend>,
}

impl Closure {
  pub fn pos(&self) -> Span {
    self.args.pos
  }

  pub fn call(&self, env: &mut LockedEnv, args: Vec<Var>) -> Result<Var> {
    // Create a new scope with all the args and all the captured variables set. The
    // captured variable ids will all be larger than self.args.len(), so we can just
    // use the max variable there.
    let mut scope = env.new_scope(
      self.captured.keys().filter_map(|id| id.local_value()).max().unwrap_or_else(|| args.len()),
    );
    for (i, (name, arg)) in self.args.names.iter().zip(args.into_iter()).enumerate() {
      scope.set_var(&VarID::local(i, name.pos()), arg);
    }
    for (id, var) in &self.captured {
      scope.set_var(id, var.clone().into());
    }
    self.body.exec(&mut scope)
  }
}
