mod assign;
mod closure;
mod event;
mod expr;
mod func;
mod if_stmt;
mod imp;
mod import;
mod loop_stmt;
mod statement;
mod strct;

pub use assign::{AssignOp, AssignOpKind, LHSKind, LHS};
pub use closure::Closure;
pub use event::OnEvent;
pub use expr::{ArrLit, CallArgs, ClosureLit, Expr, Lit, MapLit, OpKind};
pub use func::{FuncCall, FuncDef};
pub use if_stmt::If;
pub use imp::Impl;
pub use import::{Import, ImportTree};
pub use loop_stmt::Loop;
pub use statement::{Statement, StatementList};
pub use strct::{Struct, StructLit};

#[cfg(test)]
use crate::{RuntimeError, Var};
#[cfg(test)]
use panda_parse::{syntax::Parser, token::Tokenizer};

#[cfg(test)]
#[track_caller]
pub fn exec(src: &str) -> Result<Var, RuntimeError> {
  let mut parser = Parser::new(Tokenizer::new(src.as_bytes(), 0));
  let stat = parser.parse_statement().unwrap();
  if let Some(tok) = parser.peek_opt().unwrap() {
    panic!("parser is not empty! got token: {:?}", tok);
  }

  let env = crate::RtEnv::new();
  let stat = env.compile_stat_list(stat).unwrap();
  let mut lock = env.lock(&[]);
  stat.exec(&mut lock)
}
