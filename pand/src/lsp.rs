use panda::{path, Panda, PdError};
use std::error::Error;

use lsp_server::{Connection, ExtractError, Message, Notification};
use lsp_types::{
  notification::DidChangeTextDocument, Diagnostic, InitializeParams, ServerCapabilities,
};

pub fn main() -> Result<(), Box<dyn Error + Sync + Send>> {
  // Note that  we must have our logging only write out to stderr.
  eprintln!("starting panda LSP server");

  // Create the transport. Includes the stdio (stdin and stdout) versions but this
  // could also be implemented to use sockets or HTTP.
  let (connection, io_threads) = Connection::stdio();

  // Run the server and wait for the two threads to end (typically by trigger LSP
  // Exit event).
  let server_capabilities = serde_json::to_value(&ServerCapabilities {
    text_document_sync: Some(lsp_types::TextDocumentSyncCapability::Kind(
      lsp_types::TextDocumentSyncKind::FULL,
    )),
    // definition_provider: Some(OneOf::Left(true)),
    ..Default::default()
  })
  .unwrap();
  let initialization_params = connection.initialize(server_capabilities)?;
  main_loop(connection, initialization_params)?;
  io_threads.join()?;

  // Shut down gracefully.
  eprintln!("shutting down server");
  Ok(())
}

fn to_diagnostic(error: &dyn PdError, source: &str) -> Diagnostic {
  let pos = error.pos();
  let (start_line, start_col) = pos.start().line_col(&source);
  let (end_line, end_col) = pos.end().line_col(&source);

  Diagnostic {
    range: lsp_types::Range {
      start: lsp_types::Position { line: start_line - 1, character: start_col - 1 },
      end:   lsp_types::Position { line: end_line - 1, character: end_col - 1 },
    },
    severity: Some(lsp_types::DiagnosticSeverity::ERROR),
    message: error.to_string(),
    // code: Option<NumberOrString>,
    // code_description: Option<CodeDescription>,
    // source: Option<String>,
    // related_information: Option<Vec<DiagnosticRelatedInformation>>,
    // tags: Option<Vec<DiagnosticTag>>,
    // data: Option<Value>,
    ..Default::default()
  }
}

fn main_loop(
  connection: Connection,
  params: serde_json::Value,
) -> Result<(), Box<dyn Error + Sync + Send>> {
  let _params: InitializeParams = serde_json::from_value(params).unwrap();
  for msg in &connection.receiver {
    match msg {
      Message::Request(req) => {
        if connection.handle_shutdown(&req)? {
          return Ok(());
        }
        /*
        eprintln!("got request: {:?}", req);
        match cast_req::<GotoDefinition>(req) {
          Ok((id, params)) => {
            eprintln!("got gotoDefinition request #{}: {:?}", id, params);
            let result = Some(GotoDefinitionResponse::Array(Vec::new()));
            let result = serde_json::to_value(&result).unwrap();
            let resp = Response { id, result: Some(result), error: None };
            connection.sender.send(Message::Response(resp))?;
            continue;
          }
          Err(err @ ExtractError::JsonError { .. }) => panic!("{:?}", err),
          Err(ExtractError::MethodMismatch(req)) => req,
        };
        */
        // ...
      }
      Message::Response(_resp) => {
        eprintln!("got response");
      }
      Message::Notification(not) => {
        match cast_not::<DidChangeTextDocument>(not) {
          Ok(mut change) => {
            let doc = change.text_document;
            let change = change.content_changes.pop().unwrap();
            let source = change.text;

            let mut sl = Panda::new();
            sl.set_color(false);
            sl.env().add_std_builtins();

            let diagnostics =
              match sl.parse_file(&path!(), std::path::Path::new("main.pand"), source.clone()) {
                Ok(_) => None,
                Err(e) => {
                  if let panda::parse::token::ParseError::Multiple(vec) = e {
                    Some(vec.iter().map(|e| to_diagnostic(e, &source)).collect())
                  } else {
                    Some(vec![to_diagnostic(&e, &source)])
                  }
                }
              };

            if let Some(diagnostics) = diagnostics {
              let notification = Notification {
                method: "textDocument/publishDiagnostics".into(),
                params: serde_json::to_value(lsp_types::PublishDiagnosticsParams {
                  uri: doc.uri,
                  diagnostics,
                  version: None,
                })
                .unwrap(),
              };

              connection.sender.send(Message::Notification(notification))?;
            }

            continue;
          }
          Err(err @ ExtractError::JsonError { .. }) => panic!("{:?}", err),
          Err(ExtractError::MethodMismatch(not)) => not,
        };
      }
    }
  }
  Ok(())
}

/*
fn cast_req<R>(req: Request) -> Result<(RequestId, R::Params), ExtractError<Request>>
where
  R: lsp_types::request::Request,
  R::Params: serde::de::DeserializeOwned,
{
  req.extract(R::METHOD)
}
*/

fn cast_not<N>(not: Notification) -> Result<N::Params, ExtractError<Notification>>
where
  N: lsp_types::notification::Notification,
  N::Params: serde::de::DeserializeOwned,
{
  not.extract(N::METHOD)
}
