use helper::SlHelper;
use panda::{
  parse::{
    syntax::Parser,
    token::{Pos, Tokenizer},
  },
  path, Panda, PdError,
};
use std::{
  collections::{HashMap, HashSet},
  fs::File,
  io::Read,
  path::{Path, PathBuf},
};
use structopt::StructOpt;

mod helper;
mod lsp;

#[derive(StructOpt)]
struct Cli {
  /// Disables colors in error messages
  #[structopt(short, long)]
  no_color: bool,
  /// Executes the given file, then exists
  exec:     Option<PathBuf>,
  /// Starts a language server
  #[structopt(long)]
  run_lsp:  bool,
}

fn main() {
  let args = Cli::from_args();

  if args.run_lsp {
    match lsp::main() {
      Ok(()) => {}
      Err(e) => eprintln!("error while running language server: {}", e),
    }
    return;
  }

  let mut sl = Panda::new();
  sl.set_color(!args.no_color);
  sl.env().add_std_builtins();

  if let Some(path) = args.exec {
    let mut src_file = match File::open(&path) {
      Ok(v) => v,
      Err(e) => {
        eprintln!("Error while reading from file: {}", e);
        return;
      }
    };
    let mut src = String::new();
    src_file.read_to_string(&mut src).unwrap();
    match sl.parse_file(&path!(), &path, src) {
      Ok(_) => match sl.call(&path!(main)) {
        Ok(v) => {
          dbg!(v);
        }
        Err(e) => {
          sl.print_err(e);
        }
      },
      Err(e) => {
        sl.print_err(e);
      }
    }
  } else {
    let mut line = 1;
    let mut rl = rustyline::Editor::with_config(
      rustyline::Config::builder()
        .completion_type(rustyline::CompletionType::List)
        .auto_add_history(true)
        .build(),
    );
    rl.set_helper(Some(SlHelper::new(&mut sl)));

    let color = sl.use_color();
    let (env, files) = sl.env_files();
    let globals = HashSet::new();
    let import_map = HashMap::new();
    let local_funcs = HashSet::new();
    let p = path!();
    let mut compile_env = env.create_compile_env(&p, &globals, &import_map, &local_funcs);
    // We need local variables to carry between lines, so we must manually call the
    // first and second parsing functions. This also means we need to hold into
    // the parsing environment for the duration of the program.
    let mut lock = env.lock(files);

    loop {
      let readline = rl.readline(&format!("[{}] > ", line));
      match readline {
        Ok(text) => {
          let mut tok = Tokenizer::new(text.as_bytes(), 0);
          tok.set_pos(Pos::new(0));
          // First pass
          match Parser::new(tok).parse_statement() {
            // Second pass
            // TODO: Imports with a command line flag or even just inline `use` being parsed.
            Ok(stat) => match compile_env.compile(stat) {
              Ok(stat) => match env.validate_stat_list(&stat) {
                Ok(_) => match stat.exec(&mut lock) {
                  Ok(v) => {
                    println!("> {v}");
                  }
                  Err(e) => {
                    e.print(&text, Path::new("statement"), color);
                  }
                },
                Err(e) => {
                  e.print(&text, Path::new("statement"), color);
                }
              },
              Err(e) => {
                e.print(&text, Path::new("statement"), color);
              }
            },
            Err(e) => {
              e.print(&text, Path::new("statement"), color);
            }
          }
        }
        Err(_) => break,
      }
      line += 1;
    }
  }
}
