use crate::markdown::MarkdownSection;
use html_builder::{Buffer, Html5, Node};
use std::{fmt, fmt::Write, fs, io, path::PathBuf};

mod arg;
mod path;

pub use arg::{Arg, Ty};
pub use path::{Path, PathKind};

#[derive(Debug, Clone)]
pub struct Module {
  pub(crate) name: String,
  pub(crate) doc:  MarkdownSection,

  pub(crate) inner:      Vec<Module>,
  pub(crate) structs:    Vec<Struct>,
  pub(crate) builtins:   Vec<Builtin>,
  pub(crate) funcs:      Vec<Func>,
  pub(crate) primitives: Vec<Primitive>,
  pub(crate) predefined: Vec<Predefined>,
  pub(crate) callbacks:  Vec<Callback>,
}

#[derive(Debug, Clone)]
pub struct Struct {
  pub(crate) name:   String,
  doc:               MarkdownSection,
  // In the order of the source file (not alphabetical).
  pub(crate) funcs:  Vec<Func>,
  pub(crate) fields: Vec<Field>,
}

#[derive(Debug, Clone)]
pub struct Builtin {
  pub(crate) name:  String,
  doc:              MarkdownSection,
  // In the order of the source file (not alphabetical).
  pub(crate) funcs: Vec<Func>,
}

#[derive(Debug, Clone)]
pub struct Func {
  pub(crate) name: String,
  pub(crate) doc:  MarkdownSection,
  args:            Vec<Arg>,
  ret:             Option<Ty>,
}

#[derive(Debug, Clone)]
pub struct Field {
  pub(crate) name: String,
  /// TODO: Figure out how to document fields, without making the page look
  /// ugly.
  #[allow(unused)]
  pub(crate) doc:  MarkdownSection,
  ty:              Ty,
}

#[derive(Debug, Clone)]
pub struct Primitive {
  pub(crate) name:  String,
  doc:              MarkdownSection,
  pub(crate) funcs: Vec<Func>,
}

#[derive(Debug, Clone)]
pub struct Predefined {
  pub(crate) name: String,
  doc:             MarkdownSection,
}

#[derive(Debug, Clone)]
pub struct Callback {
  pub(crate) name: String,
  doc:             MarkdownSection,

  pub(crate) args: Vec<Ty>,
}

#[derive(Debug, Clone)]
pub struct Page {
  path: Path,
  kind: PageKind,
}

#[derive(Debug, Clone)]
pub enum PageKind {
  Struct(Struct),
  Builtin(Builtin),
  Module(Module),
  Func(Func),
  Primitive(Primitive),
  Predefined(Predefined),
  Callback(Callback),
}

fn write_collapse(node: &mut Node) -> fmt::Result {
  writeln!(
    node.a().attr("class='collapse-button'").attr("onclick=collapse_code(this)").attr("href=#"),
    "[-]"
  )
}

macro_rules! write_table {
  ( $list:expr, $div:expr, $dir:expr, $name:expr, $class:expr ) => {
    if !$list.is_empty() {
      let mut div = $div.div();
      write_collapse(&mut div)?;
      writeln!(div.h1(), "{}", $name)?;
      let mut list = div.div().attr("class='table'");
      for v in &$list {
        write!(
          list
            .a()
            .attr(&format!("href='{}/{}{}.html'", $dir, v.name, v.file_postfix()))
            .attr(&format!("class='table-left {}'", $class)),
          "{}",
          &v.name()
        )?;
        write!(list.p().attr(&format!("class='table-right'")), "{}", &v.doc.summary())?;
      }
    }
  };
}

impl Page {
  pub fn new(path: Path, kind: PageKind) -> Self {
    Page { path, kind }
  }
  pub fn save(&self, prefix: &str) -> io::Result<()> {
    let page = self.generate();
    let path = PathBuf::new().join(format!(
      "{}/{}{}.html",
      prefix,
      self.path.join("/"),
      self.file_postfix()
    ));
    fs::create_dir_all(path.parent().unwrap()).unwrap();
    fs::write(path, page.finish()).unwrap();
    Ok(())
  }
  pub fn generate(&self) -> Buffer {
    let mut doc = Buffer::new();
    writeln!(doc, "<!DOCTYPE html>").unwrap();
    let mut html = doc.html().attr("lang='en'");
    self.write_head(&mut html.head()).unwrap();
    self.write_body(&mut html.body()).unwrap();
    doc
  }

  fn write_body(&self, body: &mut Node) -> fmt::Result {
    self.write_sidebar(&mut body.nav().attr("class='sidebar'"))?;
    self.write_main(&mut body.nav().attr("class='main'"))?;
    Ok(())
  }

  fn write_sidebar(&self, nav: &mut Node) -> fmt::Result {
    writeln!(
      nav.h1().a().attr(&format!("href='{}index.html'", "../".repeat(self.path_len()))),
      "Panda Docs"
    )?;
    match &self.kind {
      PageKind::Module(m) => {
        writeln!(nav.h1(), "Modules")?;
        let mut modules = nav.div();
        let mut module_list = m.inner.clone();
        module_list.sort_by(|a, b| a.name.cmp(&b.name));
        for m in module_list {
          writeln!(
            modules
              .p()
              .a()
              .attr("class='sidebar-link'")
              .attr(&format!("href='{}/index.html'", m.name)),
            "{}",
            m.name
          )?;
        }
        writeln!(nav.h1(), "Structs")?;
        let mut structs = nav.div();
        let mut struct_list = m.structs.clone();
        struct_list.sort_by(|a, b| a.name.cmp(&b.name));
        for s in struct_list {
          writeln!(
            structs.p().a().attr("class='sidebar-link'").attr(&format!("href='{}.html'", s.name)),
            "{}",
            s.name
          )?;
        }
        writeln!(nav.h1(), "Builtins")?;
        let mut builtins = nav.div();
        let mut builtin_list = m.builtins.clone();
        builtin_list.sort_by(|a, b| a.name.cmp(&b.name));
        for b in builtin_list {
          writeln!(
            builtins.p().a().attr("class='sidebar-link'").attr(&format!("href='{}.html'", b.name)),
            "{}",
            b.name
          )?;
        }
        writeln!(nav.h1(), "Functions")?;
        let mut funcs = nav.div();
        let mut func_list = m.funcs.clone();
        func_list.sort_by(|a, b| a.name.cmp(&b.name));
        for f in func_list {
          writeln!(
            funcs.p().a().attr("class='sidebar-link'").attr(&format!("href='#funcs.{}'", f.name)),
            "{}",
            f.name
          )?;
        }
        writeln!(nav.h1(), "Primitives")?;
        let mut primitives = nav.div();
        for p in &m.primitives {
          writeln!(
            primitives
              .p()
              .a()
              .attr("class='sidebar-link'")
              .attr(&format!("href='{}.html'", p.name)),
            "{}",
            p.name
          )?;
        }
      }
      PageKind::Struct(s) => {
        writeln!(nav.h1(), "Functions")?;
        let mut funcs = nav.div();
        let mut func_list = s.funcs.clone();
        func_list.sort_by(|a, b| a.name.cmp(&b.name));
        for f in func_list {
          writeln!(
            funcs.p().a().attr("class='sidebar-link'").attr(&format!("href='#funcs.{}'", f.name)),
            "{}",
            f.name
          )?;
        }
      }
      PageKind::Builtin(b) => {
        writeln!(nav.h1(), "Functions")?;
        let mut funcs = nav.div();
        let mut func_list = b.funcs.clone();
        func_list.sort_by(|a, b| a.name.cmp(&b.name));
        for f in func_list {
          writeln!(
            funcs.p().a().attr("class='sidebar-link'").attr(&format!("href='#funcs.{}'", f.name)),
            "{}",
            f.name
          )?;
        }
      }
      PageKind::Func(_) => {}
      PageKind::Primitive(p) => {
        writeln!(nav.h1(), "Functions")?;
        let mut funcs = nav.div();
        let mut func_list = p.funcs.clone();
        func_list.sort_by(|a, b| a.name.cmp(&b.name));
        for f in func_list {
          writeln!(
            funcs
              .p()
              .a()
              .attr("class='sidebar-link'")
              .attr(&format!("href='{}/{}.html'", p.name, f.name)),
            "{}",
            f.name
          )?;
        }
      }
      PageKind::Predefined(_) => {}
      PageKind::Callback(_) => {}
    }
    Ok(())
  }

  fn write_main(&self, nav: &mut Node) -> fmt::Result {
    if self.path.is_empty() {
      writeln!(nav.h1(), "Panda Docs")?;
    } else if let PageKind::Predefined(_) = &self.kind {
      writeln!(
        nav.h1(),
        "Predefined <a href='{0}.predefined.html' class='predefined'>@{0}</a>",
        self.path.last()
      )?;
    } else if let PageKind::Callback(_) = &self.kind {
      writeln!(
        nav.h1(),
        "Callback <a href='{0}.callback.html' class='callback'>{0}()</a>",
        self.path.last()
      )?;
    } else {
      writeln!(
        nav.h1(),
        "{} {}",
        self.name(),
        self
          .path
          .iter()
          .enumerate()
          .map(|(idx, elem)| {
            let path = format!(
              "{}{}{}.html",
              "../".repeat(self.path_len() - idx),
              &self.path[idx],
              if idx == self.path_len() {
                self.file_postfix()
              } else if idx == self.path_len().saturating_sub(1) {
                self.parent_postfix()
              } else {
                "/index"
              }
            );
            if idx == self.path.len() - 1 {
              format!("<a href='{path}' class='{}'>{}</a>", self.class(), elem)
            } else {
              format!("<a href='{path}'>{}</a>", elem)
            }
          })
          .collect::<Vec<_>>()
          .join("::")
      )?;
    }
    match &self.kind {
      PageKind::Struct(s) => s.generate_header(&mut nav.div(), &self.path)?,
      PageKind::Callback(s) => s.generate_header(&mut nav.div(), &self.path)?,
      _ => {}
    }
    self.doc().generate(&mut nav.div().attr("class='markdown'"), "main_doc")?;
    self.generate_inner(&mut nav.div())?;
    Ok(())
  }

  fn write_head(&self, head: &mut Node) -> fmt::Result {
    writeln!(head.title(), "Panda Docs")?;
    writeln!(head.link().attr("rel='preconnect'").attr("href='https://fonts.googleapis.com'"))?;
    writeln!(head
      .link()
      .attr("rel='preconnect'")
      .attr("href='https://fonts.gstatic.com'")
      .attr("crossorigin"))?;
    writeln!(head
      .link()
      .attr("rel='stylesheet'")
      .attr("href='https://fonts.googleapis.com/css2?family=Roboto&display=swap'")
      .attr("crossorigin"))?;
    writeln!(head
      .link()
      .attr("rel='stylesheet'")
      .attr("href='https://fonts.googleapis.com/css2?family=Inconsolata&display=swap'")
      .attr("crossorigin"))?;
    writeln!(head
      .link()
      .attr("rel='stylesheet'")
      .attr(&format!("href='{}style.css'", "../".repeat(self.path_len()))))?;
    writeln!(head.script().attr(&format!("src='{}main.js'", "../".repeat(self.path_len()))))?;
    Ok(())
  }

  fn doc(&self) -> &MarkdownSection {
    match &self.kind {
      PageKind::Module(m) => &m.doc,
      PageKind::Struct(s) => &s.doc,
      PageKind::Builtin(b) => &b.doc,
      PageKind::Func(f) => &f.doc,
      PageKind::Primitive(p) => &p.doc,
      PageKind::Predefined(p) => &p.doc,
      PageKind::Callback(c) => &c.doc,
    }
  }
  fn name(&self) -> &'static str {
    match &self.kind {
      PageKind::Module(_) => "Module",
      PageKind::Struct(_) => "Struct",
      PageKind::Builtin(_) => "Builtin",
      PageKind::Func(_) => "Function",
      PageKind::Primitive(_) => "Primitive",
      PageKind::Predefined(_) => "Predefined",
      PageKind::Callback(_) => "Callback",
    }
  }
  fn class(&self) -> &'static str {
    match &self.kind {
      PageKind::Module(_) => "module",
      PageKind::Struct(_) => "struct",
      PageKind::Builtin(_) => "builtin",
      PageKind::Func(_) => "func",
      PageKind::Primitive(_) => "primitive",
      PageKind::Predefined(_) => "predefined",
      PageKind::Callback(_) => "callback",
    }
  }
  fn file_postfix(&self) -> &'static str {
    match &self.kind {
      PageKind::Module(m) => m.file_postfix(),
      PageKind::Struct(s) => s.file_postfix(),
      PageKind::Builtin(b) => b.file_postfix(),
      PageKind::Func(f) => f.file_postfix(),
      PageKind::Primitive(p) => p.file_postfix(),
      PageKind::Predefined(p) => p.file_postfix(),
      PageKind::Callback(c) => c.file_postfix(),
    }
  }
  fn parent_postfix(&self) -> &'static str {
    match &self.kind {
      PageKind::Func(_) => "",
      _ => "/index",
    }
  }

  fn generate_inner(&self, node: &mut Node) -> fmt::Result {
    match &self.kind {
      PageKind::Module(m) => m.generate(node),
      PageKind::Struct(s) => s.generate(node, &self.path),
      PageKind::Builtin(b) => b.generate(node, &self.path),
      PageKind::Func(f) => f.generate(node),
      PageKind::Primitive(p) => p.generate(node),
      PageKind::Predefined(p) => p.generate(node),
      PageKind::Callback(c) => c.generate(node),
    }
  }
  /// Number of times to repeat `../`
  pub fn path_len(&self) -> usize {
    if matches!(self.kind, PageKind::Module(_)) {
      self.path.len()
    } else {
      self.path.len() - 1
    }
  }
}

impl Module {
  pub fn new(name: String, doc: MarkdownSection) -> Self {
    Module {
      name,
      doc,
      inner: vec![],
      structs: vec![],
      builtins: vec![],
      funcs: vec![],
      primitives: vec![],
      predefined: vec![],
      callbacks: vec![],
    }
  }
  pub fn name(&self) -> &String {
    &self.name
  }
  pub fn file_postfix(&self) -> &'static str {
    "/index"
  }

  pub fn generate(&self, div: &mut Node) -> fmt::Result {
    write_table!(self.inner, div, ".", "Modules", "module");
    write_table!(self.structs, div, ".", "Structs", "struct");
    write_table!(self.builtins, div, ".", "Builtins", "builtin");
    write_table!(self.funcs, div, ".", "Functions", "func");
    write_table!(self.primitives, div, ".", "Primitives", "primitive");
    write_table!(self.predefined, div, ".", "Predefined", "predefined");
    write_table!(self.callbacks, div, ".", "Callbacks", "callback");
    Ok(())
  }

  pub fn save_all(&self, prefix: &str) -> io::Result<()> {
    // Main page
    self.save_path(prefix, Path::new(vec![], PathKind::Struct))
  }

  fn save_path(&self, prefix: &str, mut path: Path) -> io::Result<()> {
    path.push(self.name.clone());
    Page::new(path.clone(), PageKind::Module(self.clone())).save(prefix)?;
    for m in &self.inner {
      m.save_path(prefix, path.clone())?;
    }
    for s in &self.structs {
      s.save_path(prefix, path.clone())?;
    }
    for b in &self.builtins {
      b.save_path(prefix, path.clone())?;
    }
    for f in &self.funcs {
      f.save_path(prefix, path.clone())?;
    }
    for p in &self.primitives {
      p.save_path(prefix, path.clone())?;
    }
    for p in &self.predefined {
      p.save_path(prefix, path.clone())?;
    }
    for p in &self.callbacks {
      p.save_path(prefix, path.clone())?;
    }
    Ok(())
  }
}

impl Struct {
  pub fn new(name: String, doc: MarkdownSection) -> Self {
    Struct { name, doc, funcs: vec![], fields: vec![] }
  }
  pub fn name(&self) -> &String {
    &self.name
  }
  pub fn file_postfix(&self) -> &'static str {
    ""
  }

  pub fn generate_header(&self, div: &mut Node, path: &Path) -> fmt::Result {
    let mut code = div.pre();
    writeln!(code, "<span class='keyword'>struct</span> {} {{", self.name)?;
    for field in &self.fields {
      writeln!(code, "  {}: {},", field.name, field.ty.generate(path))?;
    }
    write!(code, "}}")?;
    Ok(())
  }
  pub fn generate(&self, div: &mut Node, path: &Path) -> fmt::Result {
    if !self.fields.is_empty() {
      let mut div = div.div();
      write_collapse(&mut div)?;
      writeln!(div.h1(), "Fields")?;
      let mut fields = div.div();
      for field in &self.fields {
        writeln!(fields, "<code>{}: {}</code>", field.name, field.ty.generate(path))?;
        writeln!(fields, "<br>")?;
        field.doc.generate(
          &mut fields.div().attr("class='markdown'").attr("style=margin:10pt"),
          &format!("field.{}", field.name),
        )?;
      }
    }
    if !self.funcs.is_empty() {
      let mut div = div.div();
      write_collapse(&mut div)?;
      writeln!(div.h1(), "Functions")?;
      let mut functions = div.div();
      for f in &self.funcs {
        f.generate_extended(&mut functions.div(), path)?;
      }
    }
    Ok(())
  }

  fn save_path(&self, prefix: &str, mut path: Path) -> io::Result<()> {
    path.push(self.name.clone());
    Page::new(path.clone(), PageKind::Struct(self.clone())).save(prefix)?;
    // Functions are inlined, so we don't save them here
    Ok(())
  }
}
impl Field {
  pub fn new(name: String, doc: MarkdownSection, ty: Ty) -> Self {
    Field { name, doc, ty }
  }
}

impl Builtin {
  pub fn new(name: String, doc: MarkdownSection, funcs: Vec<Func>) -> Self {
    Builtin { name, doc, funcs }
  }
  pub fn name(&self) -> &String {
    &self.name
  }
  pub fn file_postfix(&self) -> &'static str {
    ""
  }

  pub fn generate(&self, div: &mut Node, path: &Path) -> fmt::Result {
    if !self.funcs.is_empty() {
      writeln!(div.h1(), "Functions")?;
      for f in &self.funcs {
        f.generate_extended(&mut div.div(), path)?;
      }
    }
    Ok(())
  }

  fn save_path(&self, prefix: &str, mut path: Path) -> io::Result<()> {
    path.push(self.name.clone());
    Page::new(path.clone(), PageKind::Builtin(self.clone())).save(prefix)?;
    // Functions are inlined, so we don't save them here
    Ok(())
  }
}

impl Func {
  pub fn new(name: String, doc: MarkdownSection, args: Vec<Arg>, ret: Option<Ty>) -> Func {
    Func { name, doc, args, ret }
  }
  pub fn name(&self) -> &String {
    &self.name
  }
  pub fn file_postfix(&self) -> &'static str {
    ""
  }

  pub fn generate_extended(&self, div: &mut Node, path: &Path) -> fmt::Result {
    write_collapse(div)?;
    self.generate_title(&mut div.div().attr("class='func-def'").code(), path)?;
    self.doc.generate(
      &mut div.div().attr("class='markdown'").attr("style=display:block"),
      &format!("func.{}", self.name),
    )?;
    Ok(())
  }

  pub fn generate_title(&self, div: &mut Node, path: &Path) -> fmt::Result {
    if let Some(ret) = &self.ret {
      writeln!(
        div,
        "fn <a id='funcs.{}' href='#funcs.{}' class='func'>{}</a>({}) -> {}",
        self.name,
        self.name,
        self.name,
        self.args.iter().map(|v| v.generate(path)).collect::<Vec<_>>().join(", "),
        ret.generate(path),
      )?;
    } else {
      writeln!(
        div,
        "fn <a id='funcs.{}' href='#funcs.{}' class='func'>{}</a>({})",
        self.name,
        self.name,
        self.name,
        self.args.iter().map(|v| v.generate(path)).collect::<Vec<_>>().join(", ")
      )?;
    }
    Ok(())
  }

  pub fn generate(&self, _div: &mut Node) -> fmt::Result {
    Ok(())
  }

  /// Should only be called for standalone functions (functions in a module).
  /// Functions in a struct should be inlined.
  fn save_path(&self, prefix: &str, mut path: Path) -> io::Result<()> {
    path.push(self.name.clone());
    Page::new(path.clone(), PageKind::Func(self.clone())).save(prefix)?;
    Ok(())
  }
}

impl Primitive {
  pub fn new(name: String, doc: MarkdownSection) -> Primitive {
    Primitive { name, doc, funcs: vec![] }
  }
  pub fn name(&self) -> &String {
    &self.name
  }
  pub fn file_postfix(&self) -> &'static str {
    ""
  }

  pub fn generate(&self, div: &mut Node) -> fmt::Result {
    write_table!(self.funcs, div, &self.name, "Functions", "func");
    Ok(())
  }

  /// Should only be called for standalone functions (functions in a module).
  /// Functions in a struct should be inlined.
  fn save_path(&self, prefix: &str, mut path: Path) -> io::Result<()> {
    path.push(self.name.clone());
    Page::new(path.clone(), PageKind::Primitive(self.clone())).save(prefix)?;
    for p in &self.funcs {
      p.save_path(prefix, path.clone())?;
    }
    Ok(())
  }
}

impl Predefined {
  pub fn new(name: String, doc: MarkdownSection) -> Self {
    Predefined { name, doc }
  }
  pub fn name(&self) -> String {
    format!("@{}", self.name)
  }
  pub fn file_postfix(&self) -> &'static str {
    ".predefined"
  }

  pub fn generate(&self, _: &mut Node) -> fmt::Result {
    Ok(())
  }

  fn save_path(&self, prefix: &str, mut path: Path) -> io::Result<()> {
    path.push(self.name.clone());
    Page::new(path.clone(), PageKind::Predefined(self.clone())).save(prefix)?;
    // Functions are inlined, so we don't save them here
    Ok(())
  }
}

impl Callback {
  pub fn new(name: String, args: Vec<Ty>, doc: MarkdownSection) -> Self {
    Callback { name, args, doc }
  }
  pub fn name(&self) -> String {
    format!("{}()", self.name)
  }
  pub fn file_postfix(&self) -> &'static str {
    ".callback"
  }

  pub fn generate_header(&self, div: &mut Node, path: &Path) -> fmt::Result {
    let mut code = div.pre();
    writeln!(code, "<span class='keyword'>on</span> {}(", self.name)?;
    for arg in &self.args {
      writeln!(code, "  {},", arg.generate(path))?;
    }
    writeln!(code, ") {{")?;
    writeln!(code, "  <span class='comment'>/* handle callback here */</span>")?;
    write!(code, "}}")?;
    Ok(())
  }
  pub fn generate(&self, _: &mut Node) -> fmt::Result {
    Ok(())
  }

  fn save_path(&self, prefix: &str, mut path: Path) -> io::Result<()> {
    path.push(self.name.clone());
    Page::new(path.clone(), PageKind::Callback(self.clone())).save(prefix)?;
    // Functions are inlined, so we don't save them here
    Ok(())
  }
}
