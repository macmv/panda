use std::fs;

use panda_docs::{
  markdown,
  page::{Arg, Func, Path, PathKind, Ty},
  path, Docs, MarkdownSection,
};

fn main() {
  fs::create_dir_all("target/sl_docs").unwrap();
  let mut docs = Docs::new(MarkdownSection::empty());
  docs.add_module(
    path!(std, Struct),
    markdown!(
      /// The standard library
    ),
  );
  docs.add_module(
    path!(std::extra, Struct),
    markdown!(
      /// Extra library features here!
    ),
  );
  docs.add_struct(
    path!(std::extra::Type, Struct),
    markdown!(
      /// Super useful type here
    ),
    vec![Func::new(
      "my_func".into(),
      markdown!(
        /// Does nice things
      ),
      vec![Arg::new("int_arg".into(), Some(Ty::Int))],
      None,
    )],
    vec![],
  );
  docs.save("target/sl_docs");
  /*
  let module = Module::new(
    "std".into(),
    MarkdownSection::empty(),
    vec![Module::new(
      "my".into(),
      MarkdownSection::empty(),
      vec![],
      vec![
        Struct::new(
          "Type".into(),
          MarkdownSection::empty(),
          vec![
            Func::new(
              "say_hello".into(),
              markdown!(
                /// This function says hello world. The `name` is the person it is saying
                /// hello to, and the amount is the number of times it should say hello.
                ///
                /// # Example
                ///
                /// ```
                /// Type::say_hello("Rust", 5)
                /// ```
              ),
              vec![Arg::new("name".into(), Ty::Str), Arg::new("amount".into(), Ty::Int)],
              None,
            ),
            Func::new("of".into(), MarkdownSection::empty(), vec![], None),
            Func::new(
              "funcs".into(),
              MarkdownSection::empty(),
              vec![
                Arg::new("data".into(), Ty::Struct(path!(std::my::OtherType, Struct))),
                Arg::new("outer".into(), Ty::Builtin(path!(std::OuterBuiltin, Struct))),
              ],
              Some(Ty::Str),
            ),
            Func::new("here".into(), MarkdownSection::empty(), vec![], None),
          ],
        ),
        Struct::new("OtherType".into(), MarkdownSection::empty(), vec![]),
      ],
      vec![Builtin::new("Builtin".into(), MarkdownSection::empty(), vec![])],
      vec![],
    )],
    vec![Struct::new("OuterStruct".into(), MarkdownSection::empty(), vec![])],
    vec![Builtin::new("OuterBuiltin".into(), MarkdownSection::empty(), vec![])],
    vec![Func::new("outer_func".into(), MarkdownSection::empty(), vec![], None)],
  );
  module.save_all("target/sl_docs").unwrap();
  */
}
