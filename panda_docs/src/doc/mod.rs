use crate::{
  markdown::MarkdownSection,
  page::{Builtin, Callback, Field, Func, Module, Path, Predefined, Primitive, Struct, Ty},
};
use std::fs;

mod primitive;

/// A type that will store all docs. You can add as many types as you like to
/// this, and when calling save, it will save all of the needed files into html
/// files on disk.
pub struct Docs {
  inner: Module,
}

impl Docs {
  pub fn new(doc: MarkdownSection) -> Self {
    let mut docs = Docs { inner: Module::new(String::new(), doc) };
    primitive::add_primitives(&mut docs);
    docs
  }

  pub fn save(&self, path: &str) {
    fs::create_dir_all(path).unwrap();
    fs::write(path.to_string() + "/style.css", include_str!("../../static/style.css")).unwrap();
    fs::write(path.to_string() + "/main.js", include_str!("../../static/main.js")).unwrap();
    self.inner.save_all(path).unwrap();
  }

  fn find_module(&mut self, path: &Path) -> &mut Module {
    let mut module = &mut self.inner;
    for (i, elem) in path.iter().enumerate() {
      if i == path.len() - 1 {
        break;
      }
      match module.inner.binary_search_by(|m| m.name.as_str().cmp(elem)) {
        Ok(idx) => module = &mut module.inner[idx],
        Err(_) => panic!("module {:?} doesn't have parent!", path),
      }
    }
    module
  }

  pub fn add_module(&mut self, path: Path, doc: MarkdownSection) {
    // Avoid weird empty page bug. This is caused by the main Panda adding the root
    // module a second time.
    if path.last().is_empty() {
      return;
    }
    if matches!(path.first(), "arr" | "bool" | "float" | "int" | "map" | "str") {
      return;
    }
    let module = self.find_module(&path);
    let name = path.last().to_string();
    match module.inner.binary_search_by(|m| m.name.cmp(&name)) {
      Ok(idx) => module.inner[idx].doc = doc,
      Err(_) => module.inner.push(Module::new(path.last().to_string(), doc)),
    }
  }

  fn add_primitive(&mut self, path: Path, doc: MarkdownSection) {
    let module = self.find_module(&path);
    module.primitives.push(Primitive::new(path.last().to_string(), doc));
  }

  pub fn add_struct(
    &mut self,
    path: Path,
    doc: MarkdownSection,
    funcs: Vec<Func>,
    fields: Vec<Field>,
  ) {
    let module = self.find_module(&path);
    let mut strct = Struct::new(path.last().to_string(), doc);
    strct.funcs = funcs;
    strct.fields = fields;
    module.structs.push(strct);
  }

  pub fn add_builtin(&mut self, path: Path, doc: MarkdownSection, funcs: Vec<Func>) {
    let module = self.find_module(&path);
    module.builtins.push(Builtin::new(path.last().to_string(), doc, funcs));
  }

  /// Only for standalone functions.
  pub fn add_func(&mut self, path: Path, func: Func) {
    let module = self.find_module(&path);
    module.funcs.push(func);
  }

  /// Updates the docs of a standalone function.
  pub fn update_func_docs(&mut self, path: Path, doc: MarkdownSection) {
    let module = self.find_module(&path);
    let name = path.last().to_string();
    match module.funcs.binary_search_by(|m| m.name.cmp(&name)) {
      Ok(idx) => module.funcs[idx].doc = doc,
      Err(_) => panic!("no function found at path `{:?}`", path),
    };
  }

  /// Only for functions on structs.
  pub fn add_struct_func(&mut self, mut path: Path, func: Func) {
    let _name = path.pop();
    let module = self.find_module(&path);
    let struct_name = path.last();
    if let Some(idx) = module.structs.iter().position(|m| m.name.as_str() == struct_name) {
      module.structs[idx].funcs.push(func);
      return;
    }
    if let Some(idx) = module.builtins.iter().position(|m| m.name.as_str() == struct_name) {
      module.builtins[idx].funcs.push(func);
      return;
    }
    if let Some(idx) = module.inner.iter().position(|m| m.name.as_str() == struct_name) {
      module.inner[idx].funcs.push(func);
      return;
    }
    match module.primitives.iter().position(|m| m.name.as_str() == struct_name) {
      Some(idx) => module.primitives[idx].funcs.push(func),
      _ => panic!("no module at {:?}", path),
    }
  }

  /// Adds a predefined.
  pub fn add_predefined(&mut self, path: Path, doc: MarkdownSection) {
    let module = self.find_module(&path);
    let name = path.last().to_string();
    module.predefined.push(Predefined::new(name, doc));
  }
  /// Adds a callback.
  pub fn add_callback(&mut self, path: Path, args: Vec<Ty>, doc: MarkdownSection) {
    let module = self.find_module(&path);
    let name = path.last().to_string();
    module.callbacks.push(Callback::new(name, args, doc));
  }
}
