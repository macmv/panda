use crate::{
  doc::Docs,
  markdown,
  page::{Path, PathKind},
  path,
};

pub fn add_primitives(docs: &mut Docs) {
  docs.add_primitive(
    path!(arr, Struct),
    markdown!(
      /// This is the array primitive type, which is a list of values.
      ///
      /// Arrays can be created using the `[]` literal, and modified using functions
      /// on the list:
      ///
      /// ```
      /// [] // empty array
      /// [1, 2, 3] // array holding 1, 2, and 3 in order
      /// ```
      ///
      /// Elements in an array can be accessed by index, or by iterating over the array:
      /// ```
      /// let my_array = [5, 6, 7]
      ///
      /// // Access by index (starts counting at 0)
      /// my_array[0] // returns 5
      ///
      /// // Access by iterating
      /// for element in my_array {
      ///   println(element) // prints 5, then 6, then 7
      /// }
      /// ```
      ///
      /// Arrays are mutable, and can be modified in a number of ways:
      ///
      /// ```
      /// let my_array = [5, 6, 7]
      ///
      /// // Push 8 onto the end of the array
      /// my_array.push(8)
      ///
      /// // Remove the last element from the array
      /// my_array.pop() // returns 8
      /// ```
    ),
  );
  docs.add_primitive(
    path!(bool, Struct),
    markdown!(
      /// This is the bool primitive type, which can be either `true` or `false`.
      ///
      /// This is by far the simplest primitive type in Panda. `bool` is short for
      /// boolean, which can be either `true` or `false`.
      ///
      /// `bool`s can be created with either the literal text `true` or `false`, or by
      /// using comparison operators:
      ///
      /// ```
      /// true
      /// false
      ///
      /// 5 < 6 // returns true
      /// 2 == 3 // returns false
      /// ```
      ///
      /// `bool`s are used in branching operations in Panda. For example:
      ///
      /// ```
      /// if 5 == 5 {
      ///   println("math works as expected")
      /// } else {
      ///   println("how did we get here???")
      /// }
      ///
      /// // This will print out `v: 0`, then `v: 1`, then `v: 2`
      /// v = 0
      /// while v < 3 {
      ///   println("v:", v)
      ///   v++
      /// }
      /// ```
      ///
      /// The value accepted into an `if` or `while` statement will always be a bool. If
      /// it is anything else, then the program will fail to parse. For example, this
      /// does not work:
      ///
      /// ```
      /// if 5 {
      ///   println("5 is not a bool!")
      /// }
      /// ```
    ),
  );
  docs.add_primitive(
    path!(float, Struct),
    markdown!(
      /// This is the float primitive type.
      ///
      /// See also, the <a href='../int/index.html' class='primitive'>int</a> primitive type.
      ///
      /// Floats are more complex than integers. They allows you to work with decimal numbers,
      /// but they are inconsistent if you need to check for exact values.
      ///
      /// Unlike integers, division doesn't round at all.
      /// Here is how you can perform some simple math with floats:
      ///
      /// ```
      /// 5.0 + 6.0 // returns 11.0
      /// 3.0 * 3.0 // returns 9.0
      /// 3.0 - 5.0 // returns -2.0
      /// 5.0 / 2.0 // returns 2.5
      /// ```
      ///
      /// Division by zero is handled differently from ints. This does not create an error:
      ///
      /// ```
      /// 1.0 / 0.0 // returns infinity
      /// 0.0 / 0.0 // returns NaN
      /// -1.0 / 0.0 // returns -infinity
      ///
      /// -5.0 / 0.0 // returns -infinity
      /// 8.0 / 0.0 // returns infinity
      /// ```
      ///
      /// Unlike integers, floats can sometimes be hard to check equality with. Here is an
      /// example of how rounding errors can produce a bad result:
      ///
      /// ```
      /// 3.3 == 3.3 // returns true, as expected
      /// 1.1 + 2.2 // returns 3.3000000000000003
      /// 1.1 + 2.2 == 3.3 // returns false
      /// ```
      ///
      /// Because if this, you should never check for equality with floats! It can
      /// lead to very difficult bugs to find. If you really need equality checks, you should
      /// use an integer instead. For example, if you needed to track dollar values, instead
      /// of storing their dollars as a float, you would just store the number of pennys they
      /// have as an integer.
      ///
      /// Internally, this float is stored as a 64 bit floating point number (an f64 in Rust).
      /// It has a maximum value of around 1e308. The way floats are stored is with the actual
      /// value of the number in some of the bits, and an exponent in the rest of the bits.
      /// This means that as you get larger and larger numbers, the accuracy decreases. So if
      /// you have a float of 1e100, and you add 1, the accuracy is so low that the number
      /// won't be changed at all.
      ///
      /// This makes it hard to know when to switch to big ints, as there isn't a reasonable max
      /// for floats. Because of this, floats never used big ints, and will always be limited to
      /// a 64 bit value. This should be more than enough for most floating point math.
    ),
  );
  docs.add_primitive(
    path!(int, Struct),
    markdown!(
      /// This is the int primitive type.
      ///
      /// See also, the <a href='../float/index.html' class='primitive'>float</a> primitive type.
      ///
      /// Integers are very easy to work with. They can be signed, but cannot
      /// have a decimal point (that is what `float` is for).
      ///
      /// Here is how you can perform some simple math with integers:
      ///
      /// ```
      /// 5 + 6 // returns 11
      /// 3 * 3 // returns 9
      /// 3 - 5 // returns -2
      /// 6 / 3 // returns 2
      /// ```
      ///
      /// Division is a little weird. If the result is not even, it will always
      /// be rounded down:
      ///
      /// ```
      /// 5 / 2 // returns 2, not 2.5
      /// 5.0 / 2.0 // returns 2.5
      /// 5.0 / 2 // fails to compile, as you cannot mix and match floats and ints
      /// ```
      ///
      /// Another problem is division by zero. This results in an error:
      ///
      /// ```
      /// 8 / 0 // crashes the program
      /// ```
      ///
      /// The last nuace of integers is their maximum size. These are stored as
      /// 64 bit signed integers. So, they can range from 9223372036854775807
      /// to -9223372036854775808. This is obviously a large range, and works
      /// well for almost all operations. However, there are certain cases where
      /// you need numbers larger than that. In the future, this will be handled
      /// internally, by switching to a big int, which allows integers of unlimited
      /// size (at the cost of memory usage). Right now, this is not handled
      /// correctly, and you will crash the program if you try to use any numbers
      /// outside of that range.
    ),
  );
  docs.add_primitive(
    path!(map, Struct),
    markdown!(
      /// This is a map, or a collection of keys and values.
      ///
      /// Here is an example of some of the basic functionality around `map`s:
      ///
      /// ```
      /// let my_map = map { "a": 2, "b": 3, "c": 4 }
      ///
      /// println(my_map["b"]) // prints 3
      /// ```
    ),
  );
  docs.add_primitive(
    path!(str, Struct),
    markdown!(
      /// This is a string, or a portion of human-readable text.
      ///
      /// Here is an example of some of the basic functionality around `str`s:
      ///
      /// ```
      /// s = "Hello"
      /// s += " world!"
      ///
      /// println(s) // prints 'Hello world!'
      /// ```
      ///
      /// This is how you can work with text in Panda. `str`s are always immutable,
      /// so any functions you can call on a string will return new values, and never
      /// modify the variable itself.
      ///
      /// This is useful for `map`s, as you can use a string as a key, and then it will
      /// never be changed while inside the map.
      ///
      /// This is a wrapper around Rust's `String` type. Therefore, it will always be a
      /// valid UTF-8 string.
    ),
  );
}
