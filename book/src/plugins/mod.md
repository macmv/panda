# Plugins

Panda has support for adding libraries through Rust. This is how all of the interop
between Sugarcane and plugins works.

In order to add plugins to an environment, you should depend on `panda_derive`. After
this, you need to generate a binding between a Rust type and a Panda type. Example:

```rust
# extern crate panda;
# extern crate panda_derive;

#[derive(Debug, Clone)]
pub struct Point {
  x: f64,
  y: f64,
}

#[panda_derive::define_ty]
impl Point {
  pub fn new(x: f64, y: f64) -> Self {
    Point { x, y }
  }
  pub fn x(&self) -> f64 {
    self.x
  }
  pub fn y(&self) -> f64 {
    self.y
  }
}
```

The `#[panda_derive::define_ty]` line will generate bindings for all of the functions
defined in the given `impl` block. Once you do this, you can add the type to the panda
env like so:

```rust
# extern crate panda;
# extern crate panda_derive;
# #[derive(Debug, Clone)]
# pub struct Point { x: f64, y: f64 }
# #[panda_derive::define_ty]
# impl Point { pub fn new(x: f64, y: f64) -> Self { Point { x, y } } }

use panda::{Panda, runtime::Var};

let mut sl = Panda::new();

// Adds our new type
sl.env().add_builtin_ty::<Point>();

// Calls our new function.
println!("panda: {}", sl.exec_statement("Point::new(3.0, 4.0)"));
// We would check equality here, but we don't specify `Point: PartialEq`, so
// all builtins are never equal.
println!("rust:      {}", Var::from(Point::new(3.0, 4.0)));
```
