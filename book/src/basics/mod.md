# The Basics

This chapter outlines the basics of writing a program in Panda. This will show you everything
you need to know to write small, portable programs using Panda.
