# Hello World

Things:

```rust,editable
fn main() {
  println!("Hello world!");
}
```

```sug
fn main() {
  println("Hello world!")
}
```
