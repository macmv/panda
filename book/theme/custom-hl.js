hljs.registerLanguage("sug", (hljs) => ({
  name: "Panda",
  keywords: {
    keyword: "struct fn impl if while for use",
    built_in: "int str arr map",
    literal: "false true",
  },
  contains: [
    hljs.QUOTE_STRING_MODE,
    hljs.C_NUMBER_MODE,
    {
      scope: "string",
      begin: '"',
      end: '"',
      contains: [{ begin: "\\\\." }],
    },
    hljs.COMMENT("//", "$"),
  ],
}));

hljs.initHighlightingOnLoad();
